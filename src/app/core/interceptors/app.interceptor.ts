/**
 * App interceptor
 *
 * Used to append Token to header or enable / diable spinner
 *
 *  @author Mustafa Omran <promustafaomran@hotmail.com>
 *
 */

import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpEvent,
    HttpRequest,
    HttpHandler,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()

export class AppInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (localStorage.getItem('jms.token') !== null) {
            const token = JSON.parse(localStorage.getItem('jms.token'));
            if (token && token.accessToken) {
                request = request.clone({ headers: request.headers.set('Authorization', `Bearer ${token.accessToken}`) });
            }
        }

        return next.handle(request).pipe(tap(() => {
            // code runs here
        },
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    // display server errors
                    if (err.status == 400) {
                        // code runs here
                    }

                    if (err.status !== 401) {
                        // code runs here
                    }

                    if (err.status == 401) {
                        // code runs here
                    }
                } else {
                    // code runs here
                }
            }));

    }

}
