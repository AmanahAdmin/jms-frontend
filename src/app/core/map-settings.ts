/**
 * Map interface 
 * 
 * @interface MapSettings
 */

export interface MapSettings {
  apiKey: string;
  libraries?: string[];
}
