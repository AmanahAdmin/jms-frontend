import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  currentMessage = new BehaviorSubject(null);
  constructor(
    private angularFireMessaging: AngularFireMessaging,
    private toastr: ToastrService) {
    this.angularFireMessaging.messaging.subscribe((messaging) => {
      messaging.onMessage = messaging.onMessage.bind(messaging);
      messaging.onTokenRefresh = messaging.onTokenRefresh.bind(messaging);
    });
  }

  /**
   * request permission for angular messaging
   * 
   * 
   */
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe((token) => {
      localStorage.setItem('FIREBASE_TOKEN', JSON.stringify(token));
    });
  }

  /**
   * when recieve message alert toast
   * 
   * 
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe((payload: any) => {
      this.toastr.success(payload.notification.body, payload.notification.title);
      this.currentMessage.next(payload);
    });
  }
}
