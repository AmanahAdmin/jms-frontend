import { Component, OnInit, ViewChild } from "@angular/core";
import { IntiateJourneyService } from "@jms/app/shared/Services/initiate-journey.service";
import { FormGroup, NgForm } from "@angular/forms";
import { IntiateJourney } from "@jms/app/shared/Models/IntialJourneyModel";
import swal from "sweetalert2";
import { Zone } from "@jms/app/shared/Entities/admin/Zone";
import { Level } from "@jms/app/shared/enums/level.enum";
import { JourneyAssessmentModel } from "@jms/app/shared/Models/JourneyAssessmentModel";
import { Router, ActivatedRoute } from "@angular/router";
import { ResponseStatus } from "@jms/app/shared/enums/response-status.enum";
import { JourneyStatus } from "@jms/app/shared/enums/journey-status.enum";
import { JourneyDriverModel } from "@jms/app/shared/Models/JourneyDriverModel";
import { JourneyCargoModel } from "@jms/app/shared/Models/journey/JourneyCargoModel";
import { JourneyCheckPointModel } from "@jms/app/shared/Models/journey/JourneyCheckPointModel";
import { JourneyPassengerModel } from "@jms/app/shared/Models/journey/JourneyPassengerModel";
import { JourneyTruckModel } from "@jms/app/shared/Models/journey/JourneyTruckModel";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { MatRadioChange } from "@angular/material";
import mapPointToFixed from "@jms/app/shared/Helpers/number-to-fxed";
import { Calendar } from "@jms/app/constants/calendar";
declare const google: any;
import { PayloadModel } from "@jms/app/shared/Models/PayloadModel";

interface MapPoint {
  lat: string | number;
  lng: string | number;
  title?: string;
}

@Component({
  selector: "app-initiate-journey",
  templateUrl: "./initiate-journey.component.html",
  styleUrls: ["./initiate-journey.component.scss"],
})
export class InitiateJourneyComponent implements OnInit {
  @ViewChild("initJourneyForm", { static: false }) initJourneyForm: NgForm;
  errorMsg: string = "";
  form: FormGroup;
  isSubmit: boolean = false;
  isValidPeople: boolean = true;
  hasPeople: boolean = false;
  isValidAssessment: boolean = true;
  isValidPayload: boolean = true;
  isValidPayloadType: boolean = true;

  requests: any[] = [];
  zones: Zone[] = [];
  vehicleTypes: any[] = [];
  departments: any[] = [];
  locations: any[] = [];
  payloadTypes: any[] = [];
  payLoads: any[] = [];
  assessmentCategories: any[] = [];
  assessments: any[] = [];
  listAssessments: any[] = [];
  selectedPayloadTypes: number[] = [];
  drivers: JourneyDriverModel[] = [];
  vehicles: any[] = [];
  showHideVehicle = true;
  showHideDriver = true;
  checkPointError: boolean = false;
  defaultLat: number = 29.3117;
  defaultLng: number = 47.4818;

  origin: MapPoint = {
    lat: 0,
    lng: 0,
  };

  destination: MapPoint = {
    lat: 0,
    lng: 0,
  };

  waypoints: any[] = [];
  renderOptions = {
    draggable: false,
  };

  optimizeWaypoints: boolean = false;

  priorityDegrees: Level;
  RecurringEvery = "1";
  EndOptions;
  selectedCheckpoint: JourneyCheckPointModel;
  tab = 1;
  checkpoints: JourneyCheckPointModel[] = [];
  cargos: JourneyCargoModel[] = [];
  isDefaultAddCargoAssessmentsLoaded: boolean = false;
  isJourneyFromToVisible: boolean = false;

  days = Calendar.times;
  months = Calendar.months;
  eta = Calendar.etaList;

  journeyForm: IntiateJourney = new IntiateJourney(0, "", null, null, JourneyStatus.PendingOnDriverAndVehicleSelection, new Date(), null, "", "", null, null, null, null, false, "", 0, 0, "", null, null, null, true, 0, false, false, false, false, false, false, false, null, null, null, null, [], [], [], [], [], null);

  zoneFrom: any;
  zoneTo: any;
  map: any;

  edit: boolean;
  editJourneyId: number;
  travelMode: string = "TRANSIT";

  mapPointToFixed = mapPointToFixed;
  journeyAssessments: JourneyAssessmentModel[] = [];

  /**
   *
   * @param intiateJourneyService
   * @param router
   * @param activatedRoute
   */
  constructor(
    private intiateJourneyService: IntiateJourneyService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.editJourneyId = params["id"];
    });
  }

  ngOnInit() {
    this.getData();
    this.addCheckPoint();
    this.getDefaultPreAssessment();
  }

  /**
   * On Change Zone/Location in from/ to journey
   *
   *
   * @param event
   * @param flag
   */
  onLocationSlotChange(event: any, flag: number) {
    const selectedIndex = event.target.selectedIndex;
    const selectedValue = event.target.value;
    const optGroupLabel = event.target.options[
      selectedIndex
    ].parentNode.getAttribute("label");

    if (optGroupLabel == "Zones") {
      // get selected zone
      const selectedZone = this.zones.filter((zone) => {
        return zone.id == selectedValue;
      })[0];
      this.draw(selectedZone.zoneLocations, flag);

      if (flag == 1) {
        this.journeyForm.fromZone = selectedValue;
        this.journeyForm.fromLocation = null;
      } else {
        this.journeyForm.toZone = selectedValue;
        this.journeyForm.toLocation = null;
      }
    } else {
      // get selected location
      const selectedLocation = this.locations.filter((location) => {
        return location.id == selectedValue;
      })[0];

      if (flag == 1) {
        this.journeyForm.fromZone = null;
        this.journeyForm.fromLocation = selectedValue;

        this.origin = {
          lat: this.mapPointToFixed(selectedLocation["latitude"]),
          lng: this.mapPointToFixed(selectedLocation["longitude"]),
        };

        // zoom to location
        this.defaultLat = +this.origin.lat;
        this.defaultLng = +this.origin.lng;

        if (this.zoneTo) {
          this.zoneFrom.setMap(null);
        }
      } else {
        this.journeyForm.toZone = null;
        this.journeyForm.toLocation = selectedValue;

        this.destination = {
          lat: this.mapPointToFixed(selectedLocation["latitude"]),
          lng: this.mapPointToFixed(selectedLocation["longitude"]),
        };

        if (this.zoneTo) {
          this.zoneTo.setMap(null);
        }
      }
    }
  }

  /**
   * On Change Zone/Location in Checkpoints
   *
   *
   * @param event
   * @param index
   */
  onSlotChange(event: any, pointIndex: number) {
    const selectedIndex = event.target.selectedIndex;
    var point = this.journeyForm.checkPoints[pointIndex];

    const selectedValue = event.target.value;
    const optGroupLabel = event.target.options[
      selectedIndex
    ].parentNode.getAttribute("label");

    if (optGroupLabel == "Zones") {
      // get selected zone
      const selectedZone = this.zones.filter((zone) => {
        return zone.id == selectedValue;
      })[0];

      const zonePoint = {
        location: {
          lat: selectedZone.latitude,
          lng: selectedZone.longitude,
        },
        stopover: true,
      };

      this.waypoints[pointIndex] = zonePoint;
      this.waypoints = [...this.waypoints];

      point.zoneId = selectedValue;
      point.locationId = null;
    } else {
      // get selected location
      const selectedLocation = this.locations.filter((location) => {
        return location.id == selectedValue;
      })[0];

      const locationPoint = {
        location: {
          lat: selectedLocation.latitude,
          lng: selectedLocation.longitude,
        },
        stopover: true,
      };

      this.waypoints[pointIndex] = locationPoint;
      this.waypoints = [...this.waypoints];

      point.zoneId = null;
      point.locationId = selectedValue;
    }

    this.validateCheckPoints();
  }

  /**
   * default pre-assessment
   *
   *
   */
  getDefaultPreAssessment() {
    this.intiateJourneyService.GetDEfaultAssessments(1).subscribe((result) => {
      result.data.forEach((element, index) => {
        this.getAssessmentByCategory(element.assessmentCategoryId);
        this.journeyAssessments.splice(index, 0, element);
      });
    });
  }

  /**
   * new checkpoint
   *
   *
   */
  addCheckPoint() {
    const point = this.journeyForm.checkPoints[
      this.journeyForm.checkPoints.length - 1
    ];

    this.checkPointError = false;
    if (point && (!point.IsMapLocation) && point.zoneId == null && point.locationId == null) {
      return (this.checkPointError = true);
    }

    this.journeyForm.checkPoints.push(new JourneyCheckPointModel(0, 0, false, "0", 0, 0, null, null, false, "", "", "", "", "", "", 0, null, null, true));
    this.isHaveCargo();
  }

  /**
   * have cargo or not
   *
   *
   */
  isHaveCargo() {
    const isHaveList = this.journeyForm.checkPoints.filter(
      (a) => a.haveCargo == true
    );
    if (isHaveList.length != 0) {
      if (!this.isDefaultAddCargoAssessmentsLoaded) {
        this.getAllDefaultAddCargoAssessments();
        this.isDefaultAddCargoAssessmentsLoaded = true;
      }
    } else {
      const toRemoveList = this.journeyAssessments.filter(
        (a) => a.assessmentTiming == 2
      );

      const startIndex = this.journeyAssessments.indexOf(
        toRemoveList[0]
      );

      this.journeyAssessments.splice(startIndex, toRemoveList.length);
    }
  }

  /**
   * all default cargo assessments
   *
   *
   */
  getAllDefaultAddCargoAssessments() {
    this.intiateJourneyService.GetAllDefaultAddCargoAssessments(1).subscribe((result: any) => {
      result.data.forEach((element: any) => {
        const indexAt = +this.journeyAssessments.length + 1;
        this.journeyAssessments.splice(indexAt - 1, 0, element);
        this.getAssessmentByCategory(element.assessmentCategoryId);
      });
    });
  }

  /**
   * init checkpoint
   *
   *
   * @param index
   */
  iniCheckPoint(index: number) {
    let point = this.journeyForm.checkPoints[index];
    point.locationId = null;
    point.zoneId = null;
    point.IsMapLocation = !point.IsMapLocation;
    point.Order = index + 1;

    const autocompleteFormField = document.getElementById(
      `checkpointAddressClass` + index
    ) as HTMLInputElement;
    let checkpointAddress = new google.maps.places.Autocomplete(
      autocompleteFormField,
      {}
    );

    checkpointAddress.addListener("place_changed", () => {
      let place: google.maps.places.PlaceResult = checkpointAddress.getPlace();
      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      point.FromLocation = place.formatted_address;
      point.FromLocationLat = +place.geometry.location.lat().toString();
      point.FromLocationLng = +place.geometry.location.lng().toString();

      point.IsMapLocation = true;
      this.waypoints = [
        ...this.waypoints,
        ...[
          {
            location: {
              lat: point.FromLocationLat,
              lng: point.FromLocationLng,
            },
          },
        ],
      ];
    });
  }

  /**
   * show / hide drivers
   *
   *
   * @param event
   * @param ispajero
   */
  showHide(isCompanyVehicle: boolean, ispajero: boolean) {
    if (isCompanyVehicle === false) {
      this.showHideDriver = false;
      this.showHideVehicle = false;
    } else if (isCompanyVehicle === true) {
      this.showHideDriver = true;
      this.showHideVehicle = true;
    } else {
      this.showDriver(isCompanyVehicle, ispajero);
      this.showVehicle(isCompanyVehicle, ispajero);
    }
  }

  /**
   * select company type
   *
   *
   * @param event
   */
  onChangeCompany(event: MatRadioChange) {
    // reset selected previous values
    this.journeyForm.drivers = [];
    this.journeyForm.vehicles = [];

    const value = event.value;
    if (value) {
      this.showHideDriver = true;
      this.showHideVehicle = true;
      if (this.journeyForm.isPajero) {
        this.showHideVehicle = false;
        this.showHideDriver = false;
      } else {
        this.showHideVehicle = true;
        this.showHideDriver = true;
      }
    } else {
      this.showHideDriver = false;
      this.showHideVehicle = false;
    }
  }

  /**
   *  display drivers list
   *
   *
   * @param e
   * @param ispajero
   */
  showDriver(isCompanyVehicle: boolean, ispajero: boolean): boolean {
    if (isCompanyVehicle === true && ispajero === false) {
      return (this.showHideDriver = true);
    }
  }

  /**
   * display vehicles list
   *
   *
   * @param isCompanyVehicle
   * @param ispajero
   */
  showVehicle(isCompanyVehicle: boolean, ispajero: boolean): boolean {
    if (isCompanyVehicle === true && ispajero === false) {
      return (this.showHideVehicle = true);
    } else if (isCompanyVehicle === true && ispajero === true) {
      return (this.showHideVehicle = false);
    }
  }

  /**
   *
   * @param index
   * @param pay
   */
  checkPayload(index: number, payload: any): boolean {
    const car = this.cargos[index];
    if (
      car.PayloadTypes.some((e) => e.payloadTypeId == payload.payloadTypeId)
    ) {
      return true;
    }
  }

  /**
   * add payload
   *
   *
   * @param type
   * @param index
   * @param isChecked
   */
  onChangePayload(type: PayloadModel, index: number, isChecked: boolean) {
    var car = this.cargos[index];

    switch (type.isPeople) {
      case true:
        {
          if (isChecked) {
            this.hasPeople = true;
            this.isValidPeople = false;
            const people = new JourneyPassengerModel(0, "", "");
            if (car.journeyPassengers.length == 0) {
              car.journeyPassengers.push(people);
            }
            const typeToSave = {
              id: 0,
              payloadTypeId: type.payloadTypeId,
              payloadTypeName: type.name,
            };

            car.PayloadTypes.push(typeToSave);
          } else {
            this.hasPeople = false;
            car.journeyPassengers = [];
            if (car && car.PayloadTypes && car.PayloadTypes.length > 0) {
              const indexType = car.PayloadTypes.indexOf(type);
              car.PayloadTypes.splice(indexType, 1);
            }
          }
        }
        break;

      default:
        if (isChecked) {
          const good = new JourneyTruckModel(0, 0, null, "");
          if (car.journeyTrucks.length == 0) {
            car.journeyTrucks.push(good);
          }
          const typeToSave = {
            id: 0,
            payloadTypeId: type.payloadTypeId,
            payloadTypeName: type.name,
          };

          car.PayloadTypes.push(typeToSave);
        } else {
          if (car && car.PayloadTypes && car["PayloadTypes"].length > 0) {
            const indexType = car.PayloadTypes.indexOf(type);
            car.PayloadTypes.splice(indexType, 1);
          }
        }
        break;
    }
  }

  /**
   * new payload
   *
   *
   * @param type
   * @param index
   */
  addNewPayload(type: string, index: number) {
    switch (type) {
      case "People":
        {
          let car = this.cargos[index];
          const lastPeople =
            car.journeyPassengers[car.journeyPassengers.length - 1];
          if (
            lastPeople &&
            (lastPeople.fullName == "" || lastPeople.contactNumber == "")
          ) {
            return (this.isValidPeople = false);
          }

          this.isValidPeople = true;
          const people = new JourneyPassengerModel(0, "", "");
          car.journeyPassengers.push(people);
        }
        break;

      default:
        let car = this.cargos[index];
        const good = new JourneyTruckModel(0, 0, null, "");
        car.journeyTrucks.push(good);
        break;
    }
  }

  /**
   * add cargo
   *
   *
   */
  addCargo() {
    this.cargos.push(new JourneyCargoModel(0, 0, [], [], []));
  }

  /**
   * add new assesment
   *
   *
   */
  addAssessment() {
    this.isValidAssessment = true;
    const assessment = this.journeyAssessments[this.journeyAssessments.length - 1];
    if (assessment && (assessment.assessmentTiming == null || assessment.assessmentCategoryId == 0 || assessment.assessmentId == 0)
    ) {
      return (this.isValidAssessment = false);
    }

    this.journeyAssessments.push(new JourneyAssessmentModel(0, null, 0, 0, 0, false, "", true));
  }

  /**
   * remove assessment
   *
   *
   * @param index
   */
  removeAssessment(journeyAssessment: JourneyAssessmentModel) {
    this.journeyAssessments.splice(this.journeyAssessments.indexOf(journeyAssessment), 1);
  }

  /**
   * get all lookups
   *
   *
   */
  getData() {
    this.intiateJourneyService.GetLookUps().subscribe((result: any) => {
      const data = result.data;

      this.zones = data.zones;
      this.departments = data.departments;
      this.requests = data.requestTypes;

      result.data.vehicleTypes.forEach((element) => {
        this.vehicleTypes.push({
          id: 0,
          journeyId: 0,
          vehicleId: element.id,
          vehicleName: element.name,
          isPajero: element.IsPajero,
        });
      });

      result.data.payLoadTypes.forEach((element) => {
        this.payloadTypes.push({
          payloadTypeId: element.id,
          name: element.name,
          isPeople: element.isPeople,
        });
      });

      this.locations = data.locations;
      this.payLoads = data.payLoads;
      this.assessmentCategories = data.assessmentCategories;
    });

    //  var departments =  localStorage.getItem('UserDepartments');
    // if(departments){
    //   this.departments = JSON.parse(departments) ;
    // }
  }

  /**
   * assessment by category
   *
   *
   * @param categoryId
   * @param index
   */
  getAssessmentByCategory(categoryId: number, journeyAssessmentIndex?: number) {
    if (journeyAssessmentIndex || journeyAssessmentIndex == 0) {
      this.journeyAssessments[journeyAssessmentIndex].assessmentId = 0;
    }
    this.intiateJourneyService.GetAssessmentByCategory(categoryId, 1).subscribe((result: any) => {
      this.journeyAssessments.forEach(element => {
        if (element.assessmentCategoryId == categoryId) {
          this.assessments[categoryId] = result.data;
        }
      });
    });
  }

  /**
   * list vehicles
   *
   *
   */
  getVehicles() {
    this.intiateJourneyService.Vehicles(this.journeyForm.vehicleTypeId, this.journeyForm.isCompanyVehicle).subscribe((result: any) => {
      this.vehicles = result.data;
    });
  }

  /**
   * is pajero or not
   *
   *
   */
  onSelectVehicle(event: Event) {
    this.intiateJourneyService.CheckISPajero(this.journeyForm.vehicleTypeId).subscribe((result: any) => {
      this.journeyForm.ispajero = result.data;
      this.showHide(
        this.journeyForm.isCompanyVehicle,
        this.journeyForm.ispajero
      );
    });

    this.getDrivers();
  }

  /**
   * fetch drivers
   *
   *
   */
  getDrivers() {
    this.getVehicles();
    this.showHide(this.journeyForm.isCompanyVehicle, this.journeyForm.ispajero);

    if (this.journeyForm.vehicleTypeId == 0) {
      return;
    }

    this.intiateJourneyService
      .Drivers(
        this.journeyForm.vehicleTypeId,
        this.journeyForm.isCompanyVehicle
      )
      .subscribe((result: any) => {
        this.drivers = result.data;
      });
  }

  /**
   *
   * @param form
   */
  onSubmit() {
    this.isValidAssessment = true;
    this.journeyForm.cargoes = this.cargos;
    this.isSubmit = true;

    this.validateCheckPoints();
    this.validateJourneyPassengers();
    const isValidPayloads = this.validateCargos();
    if (
      this.initJourneyForm.invalid ||
      this.checkPointError ||
      !this.isValidPeople ||
      !isValidPayloads
    ) {
      swal.fire("", "Please Fill All Mandatory Data", "info");
      return (this.errorMsg = "Please Fill All Mandatory Data");
    } else {
      this.errorMsg = "";
    }

    const assessment = this.journeyAssessments[
      this.journeyAssessments.length - 1
    ];

    if (assessment && (assessment.assessmentTiming == null || assessment.assessmentCategoryId == 0 || assessment.assessmentId == 0)) {
      this.isValidAssessment = false;
      return;
    }

    this.journeyForm.cargoes = this.cargos;
    this.journeyForm.journeyAssessments = this.journeyAssessments;

    if (this.journeyForm.journeyAssessments.length == 0) {
      return (this.errorMsg = "Please Enter at least one assessment");
    } else {
      this.errorMsg = "";
    }

    if (this.isJourneyFromToVisible) {
      this.journeyForm.fromLocation = 0;
      this.journeyForm.toLocation = 0;

      this.journeyForm.fromZone = 0;
      this.journeyForm.toZone = 0;

      this.journeyForm.isMapLocation = true;
    } else {
      this.journeyForm.isMapLocation = false;
    }

    let formToPost = JSON.parse(JSON.stringify(this.journeyForm));

    // drivers
    let drivers: any[] = Array.of(formToPost.drivers);
    formToPost.drivers = [].concat(...drivers);

    // vehiclese
    let vehicles: object[] = Array.of(formToPost.vehicles);
    formToPost.vehicles = [].concat(...vehicles);

    this.intiateJourneyService.Add(formToPost).subscribe((response: any) => {
      if (response.isSuccessful) {
        swal
          .fire({
            title: "Success",
            icon: "success",
            text: "Journey initiated Sucessfully.",
            allowOutsideClick: false,
          })
          .then((end) => {
            if (end) {
              this.router.navigate(["/"]);
            }
          });
      } else {
        swal.fire({
          icon: "error",
          text: response.message,
        });
      }

      // for server error
      if (response.status == ResponseStatus.ServerError) {
        swal.fire({
          title: "Error",
          icon: "error",
          text: response.message,
        });
      }
    });
  }
  validateCargos() {
    let isValid = true;
    this.isValidPayloadType = true;
    this.isValidPayload = true;
    this.cargos.forEach((cargo) => {
      if (cargo.PayloadTypes.length === 0) {
        isValid = false;
        this.isValidPayloadType = false;
      }
      if (cargo.journeyTrucks.length > 0) {
        cargo.journeyTrucks.forEach((truck) => {
          if (truck.payloadId == null || !truck.weight) {
            isValid = false;
            this.isValidPayload = false;
          }
        });
      }
    });

    return isValid;
  }

  /**
   * add peaple
   *
   *
   * @param icargoIndex
   * @param index
   * @param item
   */
  onAddPeopleRow(icargoIndex: number, index: number, item: any) {
    if (item.contactNumber) {
      this.cargos[icargoIndex].journeyPassengers[index].contactNumber =
        item.contactNumber;
    }
    if (item.fullName) {
      this.cargos[icargoIndex].journeyPassengers[index].fullName =
        item.fullName;
    }
    if (item.fullName && item.contactNumber) {
      this.isValidPeople = true;
    }
  }

  /**
   * get map
   *
   *
   * @param map
   */
  onMapReady(map: any) {
    this.map = map;
  }

  /**
   * draw manager
   *
   *
   * @param map
   */
  draw(zone: { lat: string | number; lng: string | number }[], flag: number) {
    const coords = zone;

    coords.forEach((coord: any) => {
      coord.lat = this.mapPointToFixed(coord.lat);
      coord.lng = this.mapPointToFixed(coord.lng);
    });

    this.defaultLat = +coords[0].lat;
    this.defaultLng = +coords[0].lng;

    const polygon = new google.maps.Polygon({
      paths: coords,
      strokeColor: "#1E90FF",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#1E90FF",
      fillOpacity: 0.35,
    });

    switch (flag) {
      case 1:
        if (this.zoneFrom) {
          this.zoneFrom.setMap(null);
        }

        this.zoneFrom = polygon;
        this.zoneFrom.setMap(this.map);

        this.origin = {
          lat: coords[0]["lat"],
          lng: coords[0]["lng"],
        };
        break;

      default:
        if (this.zoneTo) {
          this.zoneTo.setMap(null);
        }

        this.zoneTo = polygon;
        this.zoneTo.setMap(this.map);

        this.destination = {
          lat: coords[0]["lat"],
          lng: coords[0]["lng"],
        };
        break;
    }
  }

  /**
   * on select address
   *
   *
   * @param address
   * @param type
   */
  handleAddressChange(address: Address, type: string) {
    const lat = address.geometry.location.lat();
    const lng = address.geometry.location.lng();

    switch (type) {
      case "origin":
        this.origin = { lat, lng };

        this.journeyForm.fromLat = +this.origin.lat;
        this.journeyForm.fromLng = +this.origin.lng;
        this.journeyForm.fromDestination = address.formatted_address;

        if (this.zoneFrom) {
          this.zoneFrom.setMap(null);
        }
        break;

      default:
        this.destination = { lat, lng };

        this.journeyForm.toLat = +this.destination.lat;
        this.journeyForm.toLng = +this.destination.lng;
        this.journeyForm.toDestination = address.formatted_address;

        if (this.zoneTo) {
          this.zoneTo.setMap(null);
        }
        break;
    }

    // zoom to new location
    this.defaultLat = lat;
    this.defaultLng = lng;
  }

  /**
   * show / hide map location
   *
   *
   */
  showHideFromTo() {
    this.isJourneyFromToVisible = !this.isJourneyFromToVisible;

    if (this.isJourneyFromToVisible) {
      this.journeyForm.isMapLocation = true;

      // reset location
      this.journeyForm.toLocation = 0;
      this.journeyForm.fromLocation = 0;

      // reset zone
      this.journeyForm.toZone = 0;
      this.journeyForm.fromZone = 0;
    } else {
      this.journeyForm.isMapLocation = false;
    }
  }

  /**
   * validate checkpoints
   *
   *
   */
  validateCheckPoints() {
    if (this.journeyForm.checkPoints.length > 0) {
      for (
        let index = 0;
        index < this.journeyForm.checkPoints.length;
        index++
      ) {
        this.checkPointError = false;
        const checkpoint = this.journeyForm.checkPoints[index];
        if (
          !checkpoint.IsMapLocation &&
          checkpoint.zoneId == null &&
          checkpoint.locationId == null &&
          checkpoint.ETA == "0"
        ) {
          this.checkPointError = true;
        }
      }
    }
  }

  /**
   * valid Journey Passengers or not
   *
   *
   */
  validateJourneyPassengers(): boolean {
    this.isValidPeople = true;
    this.cargos.forEach((cargo) => {
      if (cargo.journeyPassengers.length > 0) {
        cargo.journeyPassengers.forEach((passenger) => {
          if (passenger.fullName == "" || passenger.contactNumber == "") {
            this.isValidPeople = false;
          }
        });
      }
    });

    return this.isValidPeople;
  }

  /**
   * remove waypoint
   * 
   * 
   * @param index 
   */
  removeCheckpoint(index: number) {
    this.journeyForm.checkPoints.splice(index, 1);
    this.waypoints.splice(index, 1);
    this.waypoints = [...this.waypoints];
  }
}
