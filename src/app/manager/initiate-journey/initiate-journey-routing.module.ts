import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InitiateJourneyComponent } from './initiate-journey.component';

const routes: Routes = [{ path: '', component: InitiateJourneyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitiateJourneyRoutingModule { }
