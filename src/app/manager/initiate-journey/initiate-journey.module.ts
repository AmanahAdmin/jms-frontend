import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InitiateJourneyRoutingModule } from './initiate-journey-routing.module';
import { InitiateJourneyComponent } from './initiate-journey.component';
import { MatTableModule, MatIconModule, MatDialogModule, MatPaginatorModule, MatSortModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCheckboxModule, MatCardModule, MatMenuModule, MatSelectModule, MatDividerModule, MatProgressBarModule, MatAutocompleteModule, MatRadioModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { AgmDrawingModule } from '@agm/drawing';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
  declarations: [InitiateJourneyComponent],
  imports: [
    CommonModule,
    InitiateJourneyRoutingModule,

    // material
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatRadioModule,

    AgmCoreModule,
    AgmDirectionModule,
    AgmDrawingModule,
    GooglePlaceModule,

    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,

    MatSelectModule,
    MatDividerModule,

    MatProgressBarModule,
    MatAutocompleteModule,

    FormsModule,
    ReactiveFormsModule,
  ]
})
export class InitiateJourneyModule { }
