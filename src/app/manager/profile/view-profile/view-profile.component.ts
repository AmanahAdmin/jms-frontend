import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { UserService, AuthenticationService } from '@jms/app/shared/Services';
import { ManagerProfile } from '@jms/app/shared/Models';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ProfileChangePasswordModalComponent } from '@jms/app/shared/components/profile-change-password-modal/profile-change-password-modal.component';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit, OnDestroy {

  userId: string;
  subscriptions: Subscription[] = [];
  profile: ManagerProfile;

  constructor(
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private matDialog: MatDialog,
  ) { }

  ngOnInit() {
    this.userId = this.authenticationService.currentUserValue.id;
    this.profile = new ManagerProfile();
    this.getProfileData();
  }
  getProfileData() {
    this.subscriptions.push(
      this.userService.getProfile(this.userId).subscribe(
        res => {
          this.profile = res;
        }
      )
    );
  }

  openChangePasswordModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      userId: this.userId,
      adminId: 0
    }
    const modal = this.matDialog.open(ProfileChangePasswordModalComponent, dialogConfig);

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

}
