import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ManagerProfile } from '@jms/app/shared/Models';
import { UserService, AuthenticationService, SwalService } from '@jms/app/shared/Services';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from '@jms/app/shared/Helpers/MustMatch';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  form: FormGroup;
  userId: string;
  subscriptions: Subscription[] = [];
  profile: ManagerProfile;

  constructor(
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private swalService: SwalService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  ngOnInit() {
    this.form = this.formBuilder.group({
      fullName: ["", Validators.required],
      userName: [{ value: '', disabled: true }],
      email: [{ value: '', disabled: true }],
      emergencyContact: [""],
      address: [""],
      nationality: [""],
      phoneNumber: [""],
      image: [""],
    })
    this.userId = this.authenticationService.currentUserValue.id;
    this.profile = new ManagerProfile();
    this.getProfileData();
  }
  get f() { return this.form.controls; }
  getProfileData() {
    this.subscriptions.push(
      this.userService.getProfile(this.userId).subscribe(
        res => {
          this.profile = res;
          this.form = this.formBuilder.group({
            fullName: [this.profile.data.fullName, Validators.required],
            userName: [{ value: this.profile.data.username, disabled: true }],
            email: [{ value: this.profile.data.email, disabled: true }],
            emergencyContact: [this.profile.data.emergencyContact],
            address: [this.profile.data.address],
            nationality: [this.profile.data.nationality],
            phoneNumber: [this.profile.data.phoneNumber],
            image: [this.profile.data.image],
          })
        }
      )
    );
  }
  cancel() {
    this.form.reset();
    this.router.navigate(['../'], { relativeTo: this.route })
  }
  save() {
    if (this.form.invalid) return;
    const profile = Object.assign({}, this.profile.data, this.form.value);
    this.userService.editProfile(this.userId, profile).subscribe(
      res => {
        this.swalService.showCustomMessages({
          type: 'success',
          messages: ['Profile Edit successfully'],
          title: 'Profile Edit'
        });
        this.router.navigate(['../'], { relativeTo: this.route })
      }, err => {
        this.swalService.showCustomMessages({
          type: 'error',
          messages: ['Profile Edit failed', err],
          title: 'Profile Edit'
        })
      }
    )
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

}
