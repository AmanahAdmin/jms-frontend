import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatTable } from '@angular/material';
import swal from "sweetalert2";
import * as $ from 'jquery'
import { constant } from '@jms/app/shared/const/constant';
import { RequestTypeModel } from '@jms/app/shared/Models/RequestTypeModel';
import { AuthenticationService, SwalService } from '@jms/app/shared/Services';
import { RequestTypesService } from '@jms/app/shared/Services/request-types.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-request-type',
  templateUrl: './request-type.component.html',
  styleUrls: ['./request-type.component.scss']
})
export class RequestTypeComponent implements OnInit {
  searchBy: string = '';
  isSubmitted: boolean;

  public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
  featureConst = constant;
  constructor(public dialog: MatDialog, private requestTypesService: RequestTypesService,
    private authService: AuthenticationService, private swalService: SwalService) { }

  displayedColumns: string[] = ['name', 'isActive', 'action'];
  //dataSource = new MatTableDataSource([]);
  public dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  requestType: RequestTypeModel = new RequestTypeModel(0, "", true);;
  requests: RequestTypeModel[] = [];

  pageIndex: number = 1;
  pageSize: number = 10;
  length: number;
  totalCount: number;
  loading: boolean;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  ngOnInit() {
    this.getRequests();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  /**
  * get all request types
  *
  *
  */
  getRequests() {
    this.requestTypesService.GetAll(this.searchBy)
      .subscribe((result: any) => {
        this.requests = result.data;
        this.dataSource.data = result.data;
        //  this.dataSource = new MatTableDataSource(result.data);
      })
  }

  /**
   * initial Request Type object
   */
  initObject() {
    this.isSubmitted = false;
    this.requestType = new RequestTypeModel(0, "", true);

    return this.requestType;
  }


  /**
   * update Request Type object
   */
  Update(row_obj: any) {
    this.isSubmitted = false;
    this.requestType = row_obj;
  }


  /**
 * edit / create
 *
 *
 */
  onSubmit() {
    this.isSubmitted = true;
    if (this.requestType.name.trim().length == 0) {
      return;
    }

    switch (this.requestType.id) {

      case 0:

        this.requestTypesService.Add(this.requestType).subscribe(res => {
          Swal.fire('Add Request Type', `Your Request Type ${this.requestType.name} added successfully`, 'success');
          this.getRequests();
          this.closeModal();
        });
        break;

      default:
        this.requestTypesService.Update(this.requestType).subscribe(res => {
          Swal.fire('Update Request Type', `Your Request Type ${this.requestType.name} updated successfully`, 'success');
          this.getRequests();
          this.closeModal();
        });

        break;
    }

  }


  /**
  *
  * close Modal
  */
  closeModal() {
    $('.close').click();
    this.requestType = this.initObject();
  }


  /**
   * delete Request Type
   *
   *@param typeID
   */
  Delete(row_obj: any) {
    Swal.fire({
      title: 'Delete Request Type ',
      text: `Are you sure to delete this ${row_obj.name} request type?`,
      showCancelButton: true,
      confirmButtonColor: '#17BC9B',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.requestTypesService.Delete(row_obj.id).subscribe((res) => {
          if (res.status == 1) {
            this.getRequests();
            Swal.fire({
              icon: 'info',
              text: 'Request Type has been deleted successfully'
            })
          } else {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't delete`,
              text: res.message
            })
          }
        }, (err) => {
          Swal.fire({
            icon: 'info',
            title: `Sorry can't delete`,
            text: err
          });
        });
      }
    });
  }


  /**
   * apply Filter
   *
   *
   * @param event
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.searchBy = filterValue;
    this.getRequests();
  }


  /**
     * active or deactive Request Type
     *
     *
     * @param event
     */
    ActivateOrDeactivate(event: Event, row_obj: any) {
      event.preventDefault();
      event.stopPropagation();
      const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
      this.swalService.showConfirmMessage(message, `Request Type ${row_obj.name}`).then(result => {
        if (result.value) {
          this.requestTypesService.Active(row_obj.id, !row_obj.isActive).
          subscribe(
            (res: any) => {
              if (!res.isSuccessful) {
                Swal.fire({
                  icon: 'info',
                  title: `Sorry can't ${message}`,
                  text: res.message
                })
              } else {
                Swal.fire({
                  icon: 'success',
                  title: `${message}d successfully`,
                  text: `Request Type ${message}d Successfully`
                })
                this.getRequests();
              }
            }), ((err) => {
              Swal.fire({
                icon: 'info',
                title: `Sorry can't ${message}`,
                text: err
              });
            });
        }
      })
    }




}







