import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestTypesService } from '@jms/app/shared/Services/request-types.service';

@Component({
  selector: 'app-manage-request-type',
  templateUrl: './manage-request-type.component.html',
  styleUrls: ['./manage-request-type.component.css']
})
export class ManageRequestTypeComponent implements OnInit {
  form: FormGroup;
  formTitle: string;
  formAction: string;
  constructor(
    public dialogRef: MatDialogRef<ManageRequestTypeComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private requestTypesService: RequestTypesService
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      isActive: [''],

    });



    if (this.data.type == 'edit') {
      this.formAction = "Update";
      this.formTitle = "Update";
      this.form.patchValue(this.data.request, { emitEvent: true, onlySelf: true });


    } else {
      this.formTitle = "Add New";
      this.formAction = "Add";
    }
  }

  ngOnInit() {


  }

  cancel(): void {
    this.dialogRef.close();
  }

  /**
 * edit / create
 *
 *
 */
  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    let form = this.form.value;

    switch (this.data.type) {
      case 'create':
        this.requestTypesService.Add(form).subscribe(res => {
          this.dialogRef.close(true);
        });
        break;

      case 'edit':
        form['id'] = this.data.request.id;
        this.requestTypesService.Update(form).subscribe(res => {
          this.dialogRef.close(true);
        });
        break;

      default:
        break;
    }

  }


  /**
   * check if form input has an error
   *
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'name':
        if (this.form.get('name').hasError('required')) {
          return 'Name required';
        }
        break;

      default:
        return '';
    }
  }

}
