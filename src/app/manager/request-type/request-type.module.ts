import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestTypeRoutingModule } from './request-type-routing.module';
import { RequestTypeComponent } from './request-type.component';
import { MatAutocompleteModule, MatFormFieldModule, MatIconModule, MatButtonModule, MatDialogModule, MatInputModule, MatCardModule, MatMenuModule, MatTableModule, MatSelectModule, MatDividerModule, MatPaginatorModule, MatSortModule, MatProgressBarModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageRequestTypeComponent } from './manage-request-type/manage-request-type.component';
import { MatCheckboxModule, MatSlideToggleModule } from '@angular/material';
@NgModule({
  declarations: [RequestTypeComponent, ManageRequestTypeComponent],
  imports: [
    CommonModule,
    RequestTypeRoutingModule,

    // material
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,

    MatCheckboxModule,
    MatSlideToggleModule,
    MatCardModule,
    MatMenuModule,

    MatSelectModule,
    MatDividerModule,

    MatProgressBarModule,
    MatAutocompleteModule,

    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    ManageRequestTypeComponent
  ]
})
export class RequestTypeModule { }
