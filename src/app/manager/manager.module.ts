import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';

import { ManagerRoutingModule } from './manager-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTooltipModule,
    ManagerRoutingModule,
  ]
})
export class ManagerModule { }
