import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThirdPartyService } from '@jms/app/shared/Services/third-party.service';

@Component({
  selector: 'app-manage-third-party',
  templateUrl: './manage-third-party.component.html',
  styleUrls: ['./manage-third-party.component.css']
})
export class ManageThirdPartyComponent implements OnInit {
  form: FormGroup;
  formTitle: string;
  formAction: string;
  constructor(
    public dialogRef: MatDialogRef<ManageThirdPartyComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private thirdPartyService: ThirdPartyService
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      isActive: [false],

    });



    if (this.data.type == 'edit') {
      this.formAction = "Update";
      this.formTitle = "Update";
      this.form.patchValue(this.data.thirdParty, { emitEvent: true, onlySelf: true });


    } else {
      this.formTitle = "Add ";
      this.formAction = "Add";
    }
  }

  ngOnInit() {


  }

  cancel(): void {
    this.dialogRef.close();
  }

  /**
 * edit / create
 *
 *
 */
  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    let form = this.form.value;

    console.log(form)

    switch (this.data.type) {
      case 'create':
        this.thirdPartyService.Add(form).subscribe(res => {
          this.dialogRef.close(true);
        });
        break;

      case 'edit':
        form['id'] = this.data.thirdParty.id;
        this.thirdPartyService.Update(form).subscribe(res => {
          this.dialogRef.close(true);
        });
        break;

      default:
        break;
    }

  }


  /**
   * check if form input has an error
   *
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'name':
        if (this.form.get('name').hasError('required')) {
          return 'Name required';
        }
        break;

      default:
        return '';
    }
  }

}
