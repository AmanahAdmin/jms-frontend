import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThirdPartyRoutingModule } from './third-party-routing.module';
import { ThirdPartyComponent } from './third-party.component';
import { MatSortModule, MatTableModule, MatIconModule, MatDialogModule, MatPaginatorModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCheckboxModule, MatCardModule, MatMenuModule, MatSelectModule, MatDividerModule, MatProgressBarModule, MatAutocompleteModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ManageThirdPartyComponent } from './manage-third-party/manage-third-party.component';

@NgModule({
  declarations: [ThirdPartyComponent, ManageThirdPartyComponent],
  imports: [
    CommonModule,
    ThirdPartyRoutingModule,

    // material
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,

    MatSelectModule,
    MatDividerModule,

    MatProgressBarModule,
    MatAutocompleteModule,

    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [

  ]
})
export class ThirdPartyModule { }
