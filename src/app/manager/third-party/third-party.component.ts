import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatTable } from '@angular/material';
import { ThirdPartyService } from '@jms/app/shared/Services/third-party.service';
import { ThirdPartyModel } from './../../shared/Models/ThirdPartyModel';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { SwalService } from '@jms/app/shared/Services';

@Component({
  selector: 'app-third-party',
  templateUrl: './third-party.component.html',
  styleUrls: ['./third-party.component.scss']
})
export class ThirdPartyComponent implements OnInit {
  searchBy: string = '';
  isSubmitted: boolean;

  public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
  featureConst = constant;
  constructor(public dialog: MatDialog, private thirdPartyService: ThirdPartyService,
    private authService: AuthenticationService, private swalService: SwalService) { }

  displayedColumns: string[] = ['name', 'isActive', 'action'];
  public dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  thirdParty: ThirdPartyModel = new ThirdPartyModel(0, "", true);;
  thirdParties: ThirdPartyModel[] = [];

  pageIndex: number = 1;
  pageSize: number = 10;
  length: number;
  totalCount: number;
  loading: boolean;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  ngOnInit() {
    this.getThirdParty();
  }


  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  /**
  * get all third party
  *
  *
  */
  getThirdParty() {
    this.thirdPartyService.GetAll(this.searchBy)
      .subscribe((result: any) => {
        this.thirdParties = result.data;
        this.dataSource.data = result.data;
      })
  }

  /**
   * initial third party object
   */
  initObject() {
    this.isSubmitted = false;
    this.thirdParty = new ThirdPartyModel(0, "", true);

    return this.thirdParty;
  }


  /**
   * update third party object
   */
  Update(row_obj: any) {
    this.isSubmitted = false;
    this.thirdParty = row_obj;
  }


  /**
 * edit / create
 *
 *
 */
  onSubmit() {
    this.isSubmitted = true;
    if (this.thirdParty.name.trim().length == 0) {
      return;
    }

    switch (this.thirdParty.id) {

      case 0:
        this.thirdPartyService.Add(this.thirdParty).subscribe(res => {
          Swal.fire('Add Third Party', `Your Third Party ${this.thirdParty.name} added successfully`, 'success');
          this.getThirdParty();
          this.closeModal();
        });
        break;

      default:

        this.thirdPartyService.Update(this.thirdParty).subscribe(res => {
          Swal.fire('Update Third Party', `Your Third Party ${this.thirdParty.name} updated successfully`, 'success');
          this.getThirdParty();
          this.closeModal();
        });
        break;
    }

  }


  /**
  *
  * close Modal
  */
  closeModal() {
    $('.close').click();
    this.thirdParty = this.initObject();
  }


  /**
   * delete third Party
   *
   *@param typeID
   */
  Delete(row_obj: any) {
    Swal.fire({
      title: 'Delete Third Party ',
      text: `Are you sure to delete this ${row_obj.name} third party?`,
      showCancelButton: true,
      confirmButtonColor: '#17BC9B',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.thirdPartyService.Delete(row_obj.id).subscribe((deleteResult: any) => {
          if (deleteResult.isSuccessful === false || deleteResult.status === 0) {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't delete`,
              text: deleteResult.message
            })
          } else {
            Swal.fire({
              icon: 'info',
              text: 'Third Party has been deleted successfully'
            })
          }
          this.getThirdParty();
        })
      }
    });
  }


  /**
   * apply Filter
   *
   *
   * @param event
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.searchBy = filterValue;
    this.getThirdParty();
  }


  /**
     * active or deactive third party
     *
     *
     * @param event
     */
  ActivateOrDeactivate(event: Event, row_obj: any) {
    event.preventDefault();
    event.stopPropagation();
    const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
    this.swalService.showConfirmMessage(message, `third party ${row_obj.name}`).then(result => {
      if (result.value) {
        this.thirdPartyService.Active(row_obj.id, !row_obj.isActive).
          subscribe(
            (res: any) => {
              if (!res.isSuccessful) {
                Swal.fire({
                  icon: 'info',
                  title: `Sorry can't ${message}`,
                  text: res.message
                })
              } else {
                Swal.fire({
                  icon: 'success',
                  title: `${message}d successfully`,
                  text: `Third Party ${message}d Successfully`
                })
                this.getThirdParty();
              }
            }), ((err) => {
              Swal.fire({
                icon: 'info',
                title: `Sorry can't ${message}`,
                text: err
              });
            });
      }
    })
  }




}
