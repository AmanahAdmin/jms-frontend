export enum JourneyStatus {
  JourneyRejected = 7,
  PendingOnDriverStartJourney = 8,
  JourneyCompleted = 17,
  // JourneyClosed = 18,
  PendingOnDriverAndVehicleSelection = 19,
  PendingOnVehicleSelection = 24,
  PendingOnDriverPreAssessment = 25,
  PendingOnPreAssessmentApproval = 26,
  PendingForApproval = 27,
  Started = 28,
  //   ChangeDriver = 29,
  //   ChangeVehicle = 30,
  InProgress = 31,
  SuccessPendingPostTripAssessment = 32,
  RescheduleTheJourney = 21,
  RollBackToDriverToResubmitAssessments = 23,
  PendingOnChangingDriver = 33,
  PendingOnChangingVehicle = 34,
  SuccessPendingPostTripAssessmentApproval = 35,
  PendingPreAssessmentApproval = 36,
  PendingCheckpointAssessmentApproval = 37,
  FailedPendingPostTripAssessment = 38,
  FailedPendingPostTripAssessmentApproval = 39,
  PendingCheckpointAssessment = 40,
  JourneyCancelled = 41,
  PendingOnDriverPostTripAssessment = 32,
  PendingPostTripAssessmentApproval = 35,
}
export const JourneyStatusValue = [
  { id: 25, value: 'Pending driver pre-assessment' },
  { id: 36, value: 'Pending pre-assessment approval' },
  { id: 28, value: 'Started' },
  { id: 7, value: 'Rejected' },
  { id: 41, value: 'Cancelled' },
  { id: 17, value: 'Completed' },
  { id: 32, value: 'Pending post-trip assessment' },
  { id: 35, value: 'Pending post-trip approval' },
  { id: 19, value: 'Pending on driver and vehicle selection' },
  { id: 24, value: 'Pending on vehicle selection' },
  { id: 21, value: 'Request To Reschedule' },
  { id: 33, value: 'Request To Change Driver' },
  { id: 34, value: 'Request To Change Vehicle' },
  { id: 23, value: 'Roll Back To Driver To Resubmit Assessment' },
]