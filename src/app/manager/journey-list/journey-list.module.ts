import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { JourneyListRoutingModule } from './journey-list-routing.module';
import { JourneyListComponent } from './journey-list.component';
import { SharedModule } from '@jms/app/shared/shared.module';
import { JourneyDetailsComponent } from './journey-details/journey-details.component';
import {
  MatTableModule,
  MatDatepickerModule,
  MatIconModule,
  MatDialogModule,
  MatPaginatorModule,
  MatSortModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatCardModule,
  MatMenuModule,
  MatSelectModule,
  MatDividerModule,
  MatProgressBarModule,
  MatAutocompleteModule,
  MatRadioModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmDirectionModule } from 'agm-direction';
import { AgmCoreModule } from '@agm/core';
import { AgmDrawingModule } from '@agm/drawing';

import { RejectJourneyModalComponent } from './journey-details/reject-journey-modal/reject-journey-modal.component';
import { JourneyActionsModalComponent } from './journey-details/journey-actions-modal/journey-actions-modal.component';
import { AssessmentAnswersModule } from './journey-details/assessment-answers/assessment-answers.module';
import { DateToTimezoneModule } from '@jms/app/pipes/date-to-timezone/date-to-timezone.module';
import { MergeJourneysModalComponent } from './merge-journeys-modal/merge-journeys-modal.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { UnMergeJourneyModalComponent } from './un-merge-journey-modal/un-merge-journey-modal.component';
import { CommunicationPointsModule } from './journey-details/communication-points/communication-points.module';

@NgModule({
  declarations: [
    JourneyListComponent,
    JourneyDetailsComponent,
    RejectJourneyModalComponent,
    JourneyActionsModalComponent,
    MergeJourneysModalComponent,
    UnMergeJourneyModalComponent,
  ],
  imports: [
    CommonModule,
    JourneyListRoutingModule,
    // material
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatRadioModule,
    AgmDirectionModule,
    AgmCoreModule,
    AgmDrawingModule,

    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,

    MatSelectModule,
    MatDividerModule,

    MatProgressBarModule,
    MatAutocompleteModule,

    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    SharedModule,
    AssessmentAnswersModule,
    DateToTimezoneModule,
    DragDropModule,
    CommunicationPointsModule,
  ],
  entryComponents: [
    RejectJourneyModalComponent,
    JourneyActionsModalComponent,
    MergeJourneysModalComponent,
    UnMergeJourneyModalComponent
  ],
  providers: [
    DatePipe
  ]
})
export class JourneyListModule { }
