import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JourneyListComponent } from './journey-list.component';
import { JourneyDetailsComponent } from './journey-details/journey-details.component';

const routes: Routes = [
  {
    path: '',
    component: JourneyListComponent
  },
  {
    path: 'details/:id',
    component: JourneyDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JourneyListRoutingModule { }
