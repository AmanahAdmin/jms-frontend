import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JourneyFilterParams, JourneyList } from '@jms/app/shared/Models/journeyList.model';
import { AuthenticationService, SwalService } from '@jms/app/shared/Services';
import { JourneyService } from '@jms/app/shared/Services/JourneyService';
import { constant } from '@jms/app/shared/const/constant';
import { Router } from '@angular/router';
import { JourneyStatus, JourneyStatusValue } from './journey-status';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { RejectJourneyModalComponent } from './journey-details/reject-journey-modal/reject-journey-modal.component';
import { MergeJourneysModalComponent } from './merge-journeys-modal/merge-journeys-modal.component';
import { MergedJourney } from '@jms/app/shared/Models/merged-journey.model';
import { IntiateJourneyService } from '@jms/app/shared/Services/initiate-journey.service';
import { UnMergeJourneyModalComponent } from './un-merge-journey-modal/un-merge-journey-modal.component';

interface JourneysListState {
  listItem: JourneyList,
  isChecked: boolean
}
@Component({
  selector: 'app-journey-list',
  templateUrl: './journey-list.component.html',
  styleUrls: ['./journey-list.component.scss']
})
export class JourneyListComponent implements OnInit, OnDestroy {

  //journeysList: JourneyList[] = [];

  journeysListState: JourneysListState[] = [];
  mergedJourneys: MergedJourney[] = [];
  statusList: Object[] = [];
  subscriptions: Subscription[] = [];
  filterParams = new JourneyFilterParams();

  pageIndex: number = 1;
  pageSize: number = 3;
  length: number;
  totalCount = 0;
  featureConst = constant;
  journeyStatus = JourneyStatus;

  /**
   * 
   * @param journeyService 
   * @param swalService 
   * @param authService 
   */
  constructor(
    private journeyService: JourneyService,
    private swalService: SwalService,
    private authService: AuthenticationService,
    private router: Router,
    private dialog: MatDialog,
    private intiateJourneyService: IntiateJourneyService,
  ) {
  }

  ngOnInit() {
    this.statusList = this.buildJourneyStatusArray();
    //this.filterParams.status = -1;
    this.getJourneys(this.pageIndex);
  }

  /**
   * 
   * @param featurename check for feature permission
   */
  public validateFeature(featurename: string) {
    return this.authService.validateFeature(featurename);
  }

  /**
   * 
   * @param pageNumber get all journeys list
   */
  getJourneys(pageNumber: number) {
    this.filterParams.page = pageNumber;
    this.filterParams.pageSize = this.pageSize;
    this.pageIndex = pageNumber;

    this.subscriptions.push(
      this.journeyService.getJourneysList(this.filterParams).subscribe(
        res => {
          //this.journeysList = res.data.journeyList;
          this.journeysListState = res.data.journeyList.map(journey => ({
            isChecked: !!this.mergedJourneys && this.mergedJourneys.some(merged => merged.journeyId === journey.id),
            listItem: journey
          }))

          this.totalCount = res.data.count;
          this.length = res.data.journeyList.length;
        }
      )
    )
  }

  changeFilters() {
    this.getJourneys(this.pageIndex);
  }

  filterByTitle() {
    if (this.filterParams.title === '') {
      this.getJourneys(this.pageIndex);
    }
  }

  /**
   * 
   * @param id delete specific joureny
   */
  deleteJourney(id: number) {
    this.swalService.showConfirmMessage('delete', 'journey')
      .then(data => {
        if (data.value) {
          this.subscriptions.push(
            this.journeyService.deleteJourney(id).subscribe(
              res => {
                this.swalService.showCustomMessages({
                  type: 'success',
                  messages: ['Deleted successfully']
                });
                this.getJourneys(this.pageIndex);
              },
              err => {
                this.swalService.showCustomMessages({
                  type: 'error',
                  messages: ['Delete is failed', err]
                });
              }
            )
          );
        }
      })
  }

  /**
   * change filter status
   * 
   * 
   * @param event 
   */
  changeStatus(event: number) {
    this.filterParams.status = event;
    this.getJourneys(this.pageIndex);
  }

  openCancelModal(id: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '600px';
    dialogConfig.data = {
      title: 'Cancel Journey',
      placeholder: 'Please Enter Cancel Reason'
    }
    const dialogRef = this.dialog.open(RejectJourneyModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.cancelJourney(id, data);
        }
      }
    );
  }

  /**
   * open merged journeys modal to rearrange priorities
   */
  openMergeJourneysModal() {
    if (this.mergedJourneys.length === 0) {
      this.swalService.showCustomMessages({
        type: 'error',
        title: `Can't Merge`,
        messages: ['please select journeys to be merged']
      })
      return;
    }
    if (this.mergedJourneys.length === 1) {
      this.swalService.showCustomMessages({
        type: 'error',
        title: `Can't Merge`,
        messages: [`you can't merge only one journey, please select more than one journey`]
      })
      return;
    }
    if (!this.validateMergedJourneys()) {
      this.swalService.showCustomMessages({
        type: 'error',
        title: `Can't Merge This Journeys`,
        messages: ['Driver and Vehicle plate no must be the same for all selected journeys']
      })
      return;
    }

    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '715px';
    dialogConfig.height = '617px';
    dialogConfig.autoFocus = false;
    const isValidArrange = this.mergedJourneys.every(x => x.status != "PendingOnDriverPreAssessment");
    if (!isValidArrange) {
      this.mergedJourneys.sort((j1, j2) => (j1.status == 'PendingOnDriverPreAssessment') ? 1 : -1);
    }
    dialogConfig.data = {
      mergedJourneys: this.mergedJourneys
    }
    const dialogRef = this.dialog.open(MergeJourneysModalComponent, dialogConfig);
    dialogRef.componentInstance.onDelete.subscribe(() => {
      this.journeysListState.forEach(journyState => journyState.isChecked = !!this.mergedJourneys && this.mergedJourneys.some(merged => merged.journeyId === journyState.listItem.id))
    })
    dialogRef.afterClosed().subscribe(
      data => {
        if (data.isSuccessful) {
          this.getJourneys(this.pageIndex);
        } else {
          this.mergedJourneys = data;
        }
      }
    );
  }

  cancelJourney(id, reason) {
    this.journeyService.cancelJourney(id, reason).subscribe(
      res => {
        this.getJourneys(this.pageIndex);

      }
    )
  }

  /**
   * get all status for dropdown
   * 
   * 
   * @param languageId 
   */
  // getAllStatus() {
  //   this.subscriptions.push(
  //     this.journeyService.getJourneyStatus('en').subscribe(
  //       res => {
  //         this.statusList = res.data;
  //       }
  //     )
  //   );
  // }

  /**
   * navigate to journey details
   * 
   * 
   * @param id 
   */
  journeyDetails(id: number) {
    this.router.navigate(['journey-list/details', id]);
  }

  /**
   * edit journey
   * 
   * 
   * @param id 
   */
  editJourney(id: number) {
    this.router.navigate(['edit-journey', id]);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  /**
   * Journey Status
   * 
   * 
   */
  buildJourneyStatusArray(): Object[] {
    const array = []
    for (let obj of JourneyStatusValue) {
      array.push({ id: obj.id, name: obj.value });
    }
    return array;
  }

  /**
   * 
   * @param event 
   * @param journey 
   * @param index 
   * add journey to merged list and remove it when unselected
   */
  addToMergeList(journey: JourneysListState) {
    // event.preventDefault();
    const mergedJourney: MergedJourney = {
      isHold: false,
      status: journey.listItem.status.statusName,
      title: journey.listItem.title,
      driver: journey.listItem.driverName,
      journeyId: journey.listItem.id,
      vehiclePlateNo: journey.listItem.vehiclePlateNo,
      driverId: journey.listItem.driverId,
      vehicleTypeId: journey.listItem.vehicleTypeId,
      vehicleId: journey.listItem.vehicleId
    }
    const index = this.mergedJourneys.findIndex(merged => merged.journeyId === journey.listItem.id)
    if (index === -1) {
      this.mergedJourneys.push(mergedJourney);
    } else {
      this.mergedJourneys.splice(index, 1)
    }
  }

  /**
   * compare journeys drivers and vehhcles plate no  to be merged
   */
  validateMergedJourneys() {
    if (this.mergedJourneys.length <= 1)
      return false;
    const firstDriverId = this.mergedJourneys[0].driverId;
    const firstVehiclePlateNo = this.mergedJourneys[0].vehiclePlateNo
    let isValid = true;
    this.mergedJourneys.forEach(journey => {
      if (journey.driverId !== firstDriverId || journey.vehiclePlateNo != firstVehiclePlateNo) {
        isValid = false;
      }
    });
    return isValid;
  }

  /**
   * 
   * @param id 
   * open popup to confirm un merge journey
   */
  openUnmergeJourneyModal(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '600px';
    dialogConfig.autoFocus = false;
    const dialogRef = this.dialog.open(UnMergeJourneyModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.intiateJourneyService.unmergeJourney(id).subscribe(
            res => {
              this.swalService.showCustomMessages({
                type: 'success',
                messages: ['Journey Unmerged successfully']
              });
              this.getJourneys(this.pageIndex);
            },
            err => {
              this.swalService.showCustomMessages({
                type: 'error',
                messages: [err]
              });
            }
          );
        }
      }
    );
  }

  /**
   * 
   * @param status 
   * check journey status is applicable to be merged or not 
   */
  validateJourneyStatus(status: JourneyStatus) {
    if (status == JourneyStatus.JourneyCompleted || status == JourneyStatus.JourneyRejected || status == JourneyStatus.JourneyCancelled) {
      return false;
    }
    return true;
  }

}
