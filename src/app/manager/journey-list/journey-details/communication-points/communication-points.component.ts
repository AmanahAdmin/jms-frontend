import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  ViewChild,
} from "@angular/core";
import Swal from "sweetalert2";
import { Calendar, IETA } from "@jms/app/constants/calendar";

import { CommunicationPoint } from "@jms/app/shared/Models/communication-point-model";
import { JourneyLocationsModel } from "@jms/app/shared/Models/journey-locations-model";
import { CommunicationPointService } from "@jms/app/shared/Services/communication-point.service";
import { Subscription } from "rxjs";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-communication-points",
  templateUrl: "./communication-points.component.html",
  styleUrls: ["./communication-points.component.scss"],
})
export class CommunicationPointsComponent implements OnInit, OnDestroy {
  @Input() points: CommunicationPoint[] = [];
  @Input() journeyId;
  @Input() isAllowedToAddUpdateCommunicationPoint: boolean;
  @Input() showSaveButton: boolean = true;
  @Output() onChangePoints: EventEmitter<
    CommunicationPoint[]
  > = new EventEmitter();
  @ViewChild("form", { static: true }) form: NgForm;

  saveSubscription: Subscription;
  locationsSubscription: Subscription;
  locations: JourneyLocationsModel[] = [];
  etaListUnits: (IETA & { id: number })[] = [];
  selectedEtas: number[] = [];
  validPoints: boolean = true;
  isSubmit: boolean = false;
  /**
   *
   * @param communicationPointService
   */
  constructor(private communicationPointService: CommunicationPointService) { }

  ngOnInit() {
    this.getAllLocations();
    this.etaListUnits = Calendar.etaListUnits.map((unit, index) => ({
      ...unit,
      id: index + 1,
    }));

    if (this.points && this.points.length > 0) {
      this.selectedEtas = this.points.map((point) => {
        if (point.duration) {
          return this.etaListUnits.filter(
            (eta) => eta.unit == point.durationUnit && eta.value == point.duration
          )[0].id;
        }
      });

      const hasNotSavedPoints = this.points.some(point => !point.id);
      if (hasNotSavedPoints) {
        this.form.form.markAsDirty();
      }
    }

    this.onChangePoints.emit(this.points);
  }

  /**
   * Save communication point after delete,add or updated existed
   *
   *
   */
  save() {
    this.isSubmit = true;

    if (!this.validateCommunicationPoints()) {
      Swal.fire("", "Fill all required data first", "info");
      return;
    }
    if (!this.validateCommunicationPointslocations()) {
      Swal.fire(
        "",
        `can't add communication point with the same location from and to`,
        "info"
      );
      return;
    }
    if (!this.validateDublicatedCommunicationPoints()) {
      Swal.fire(
        "",
        `can't add duplicated communication points with the same location from and to`,
        "info"
      );
      return;
    }

    if (this.saveSubscription) {
      this.saveSubscription.unsubscribe();
    }

    this.points.forEach((point) => {
      point.journeyId = this.journeyId;
    });
    this.saveSubscription = this.communicationPointService
      .save(this.points, this.journeyId)
      .subscribe(
        (res: any) => {
          if (res.isSuccessful) {
            Swal.fire("", res.message, "success");
            this.form.form.markAsPristine();
            this.isSubmit = false;
          } else {
            Swal.fire("", res.message, "error");
          }
        },
        (err) => {
          Swal.fire("", err, "error");
        }
      );
  }

  /**
   * create new communication point
   *
   *
   * @param point
   */
  create(point: CommunicationPoint) {
    this.communicationPointService.addCommunicationPoint(point).subscribe();
  }

  /**
   * add communication point to array
   *
   *
   */
  add() {
    let point = new CommunicationPoint();
    point.allowUpdateDeleteAction = true;

    point.checkpointFromId = 0;
    point.checkpointToId = 0;
    this.points.push(point);

    const lastIndex = this.points.length - 1;
    this.selectedEtas[lastIndex] = 1;

    if (this.points.length >= 1) {
      this.isValidPoints();
    }

    this.onChangePoints.emit(this.points);
  }

  isValidPoints(): void {
    this.validPoints = true;
    this.points.forEach((point, index) => {
      if (
        point.checkpointToId == 0 ||
        point.checkpointFromId == 0 ||
        this.selectedEtas[index] == 1
      ) {
        this.validPoints = false;
      }
    });
  }

  /**
   * remove communication point
   *
   *
   * @param id
   */
  remove(index: number) {
    if (this.points[index].journeyId) {
      this.form.form.markAsDirty();
    } else {
      this.form.form.markAsPristine();
    }
    this.points.splice(index, 1);
    this.selectedEtas.splice(index, 1);
    this.isValidPoints();
    this.onChangePoints.emit(this.points);
  }

  /**
   * get all locations related to journey
   *
   *
   */
  getAllLocations() {
    this.locationsSubscription = this.communicationPointService
      .getJourneyLocations(this.journeyId)
      .subscribe((res) => {
        this.locations = res.data;
      });
  }

  /**
   * set location to data
   *
   *
   * @param point
   */
  setLocationTo(point: CommunicationPoint) {
    point.checkpointToId = Number(point.checkpointToId);

    const location = this.locations.find(
      (location) => location.locationId == point.checkpointToId
    );
    if (location) {
      point.locationToType = location.locationType;
    }

    this.isValidPoints();
  }

  /**
   * set location from data
   *
   *
   * @param point
   */
  setLocationFrom(point: CommunicationPoint) {
    point.checkpointFromId = Number(point.checkpointFromId);

    const location = this.locations.find(
      (location) => location.locationId == point.checkpointFromId
    );
    if (location) {
      point.locationFromType = location.locationType;
    }

    this.isValidPoints();
  }

  /**
   * fill ETA attributes
   *
   *
   * @param point
   * @param etaId
   */
  setDuration(point: CommunicationPoint, etaId: number) {
    const eta = this.etaListUnits.find((eta) => eta.id == etaId);
    if (eta) {
      point.duration = eta.value;
      point.durationUnit = eta.unit;
    }

    this.isValidPoints();
  }

  /**
   * check and vaildate communications points
   *
   *
   */
  validateCommunicationPoints() {
    let isValid = true;
    this.points.forEach((point) => {
      if (
        point.checkpointFromId == null ||
        point.checkpointToId == null ||
        !point.duration
      ) {
        isValid = false;
      }
    });

    return isValid;
  }

  /**
   * validate adding the same location for from and to communication point
   */
  validateCommunicationPointslocations() {
    let isValid = true;
    this.points.forEach((point) => {
      if (point.checkpointFromId == point.checkpointToId) {
        isValid = false;
      }
    });
    return isValid;
  }

  /**
   * validate adding Dublication for the same communication point
   */
  validateDublicatedCommunicationPoints() {
    let isValid = true;
    let j = 1;
    for (let index = 0; index < this.points.length - 1; index++) {
      const element = this.points[index];
      const secondElement = this.points[j];
      if (
        element.checkpointFromId == secondElement.checkpointFromId &&
        element.checkpointToId == secondElement.checkpointToId
      ) {
        isValid = false;
      }
      j++;
    }
    return isValid;
  }

  /**
   * track points array for adding, modifing and removeing
   *
   *
   * @param index
   * @param item
   */
  trackByFn(index: number, item) {
    return index;
  }

  /**
   * end all open subscriptions when component destroyed
   *
   *
   */
  ngOnDestroy(): void {
    if (this.saveSubscription) {
      this.saveSubscription.unsubscribe();
    }

    if (this.locationsSubscription) {
      this.locationsSubscription.unsubscribe();
    }
  }
}
