import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationPointsComponent } from './communication-points.component';

describe('CommunicationPointsComponent', () => {
  let component: CommunicationPointsComponent;
  let fixture: ComponentFixture<CommunicationPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
