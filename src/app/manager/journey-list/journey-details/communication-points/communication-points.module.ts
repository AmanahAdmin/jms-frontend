import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunicationPointsComponent } from './communication-points.component';
import { FormsModule } from '@angular/forms';
import { MatIconModule, MatButtonModule, MatFormFieldModule } from '@angular/material';

@NgModule({
  declarations: [CommunicationPointsComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule
  ],
  exports: [
    CommunicationPointsComponent
  ]
})
export class CommunicationPointsModule { }
