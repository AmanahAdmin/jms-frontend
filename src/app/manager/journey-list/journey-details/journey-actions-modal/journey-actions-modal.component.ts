import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthenticationService } from '@jms/app/shared/Services';
import { IntiateJourneyService } from '@jms/app/shared/Services/initiate-journey.service';

interface AllowedPermissions {
  isAllowedToRequestToRescheduleTheJourney: boolean;
  isAllowedToRequestToChangeDriver: boolean;
  isAllowedToRequestToChangeVehicle: boolean;
  isAllowedToRollBackToDriverToResubmitAssessments: boolean;
}

@Component({
  selector: 'app-journey-actions-modal',
  templateUrl: './journey-actions-modal.component.html',
  styleUrls: ['./journey-actions-modal.component.scss']
})
export class JourneyActionsModalComponent {

  actionsPermissions = [
    'isAllowedToRequestToRescheduleTheJourney',
    'isAllowedToRequestToChangeDriver',
    'isAllowedToRequestToChangeVehicle',
    'isAllowedToRollBackToDriverToResubmitAssessments'
  ];

  actions: AllowedPermissions;
  journeyId;
  languageId;
  selectedAction: string;
  reason: string;
  isSubmit: boolean = false;

  /**
   * 
   * @param authService 
   * @param intiateJourneyService 
   * @param dialogRef refrence for modal
   * @param data get received data for binding   
   */
  constructor(
    private dialogRef: MatDialogRef<JourneyActionsModalComponent>,
    private authService: AuthenticationService,
    private intiateJourneyService: IntiateJourneyService,
    @Inject(MAT_DIALOG_DATA) data,
  ) {
    dialogRef.disableClose = true;
    this.actions = data.actions;
    this.journeyId = data.journeyId;
  }

  /**
   * 
   * @param featurename validate feature permission
   */
  validateFeature(featurename: string) {
    return this.authService.validateFeature(featurename);
  }

  /**
   * submit button action according to permissions
   * 
   * 
   */
  submit() {
    this.isSubmit = true;

    if (this.isSubmit && !this.reason) {
      return;
    }

    let body = {
      journeyId: this.journeyId,
      reason: this.reason
    };

    switch (this.selectedAction) {
      case this.actionsPermissions[0]:
        body['actionType'] = 0;
        break;

      case this.actionsPermissions[1]:
        body['actionType'] = 1;
        break;

      case this.actionsPermissions[2]:
        body['actionType'] = 2;
        break;

      case this.actionsPermissions[3]:
        body['actionType'] = 3;
        break;
    }

    this.intiateJourneyService.requstReschedule(body).subscribe(res => {
      this.dialogRef.close(true);
    });
  }

  close() {
    this.dialogRef.close();
  }
}
