import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourneyActionsModalComponent } from './journey-actions-modal.component';

describe('JourneyActionsModalComponent', () => {
  let component: JourneyActionsModalComponent;
  let fixture: ComponentFixture<JourneyActionsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourneyActionsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourneyActionsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
