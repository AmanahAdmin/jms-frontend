import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentAnswersComponent } from './assessment-answers.component';

describe('AssessmentAnswersComponent', () => {
  let component: AssessmentAnswersComponent;
  let fixture: ComponentFixture<AssessmentAnswersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentAnswersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentAnswersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
