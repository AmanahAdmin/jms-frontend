import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AssessmentTiming } from '@jms/app/shared/enums/assessment-timing';
import { AssessmentAnswersTiming, JourenyData } from '@jms/app/shared/Models/assessmentAnswer.model';
import { AuthenticationService } from '@jms/app/shared/Services';
import { IntiateJourneyService } from '@jms/app/shared/Services/initiate-journey.service';

@Component({
  selector: 'app-assessment-answers',
  templateUrl: './assessment-answers.component.html',
  styleUrls: ['./assessment-answers.component.scss']
})
export class AssessmentAnswersComponent implements OnInit {

  assessmentAnswerList: AssessmentAnswersTiming[] = [];
  journeyData = new JourenyData();
  languageId;

  @Input() journeyId: number;

  timing: AssessmentTiming;
  answersIds: number[] = [];

  @Output() onGetAnswersIds = new EventEmitter<number[]>();

  constructor(
    private intiateJourneyService: IntiateJourneyService,
    private authService: AuthenticationService,

  ) { }

  ngOnInit() {
    this.authService.currentUser.subscribe(
      user => {
        this.languageId = user.browsingLanguageId;
        this.getAssessmentAnswers();
      }
    )
  }

  /**
   * assessment answers
   * 
   * 
   */
  getAssessmentAnswers() {
    this.intiateJourneyService.getAssessmentAnswers(this.journeyId, this.languageId).subscribe(
      res => {
        this.assessmentAnswerList = res.data.answers;

        if (this.assessmentAnswerList.length > 0) {
          this.assessmentAnswerList.forEach(assessmentAnswerTiming => {
            assessmentAnswerTiming.assessmentWithAnwswers.forEach(assessmentAnswer => {
              assessmentAnswer.answers.forEach(answer => {
                this.answersIds.push(answer.id);
              })
            });
          });
        }

        if (this.answersIds.length > 0) {
          this.onGetAnswersIds.emit(this.answersIds);
        }

        this.journeyData = res.data.jourenyData;
      });
  }
}
