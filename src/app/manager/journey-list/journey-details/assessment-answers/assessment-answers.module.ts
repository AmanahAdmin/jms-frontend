import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssessmentAnswersComponent } from './assessment-answers.component';
import { DateToTimezoneModule } from '@jms/app/pipes/date-to-timezone/date-to-timezone.module';
import { MatButtonModule, MatExpansionModule } from '@angular/material';

@NgModule({
  declarations: [AssessmentAnswersComponent],
  imports: [
    CommonModule,
    DateToTimezoneModule,
    MatExpansionModule,
    MatButtonModule
  ],
  exports: [
    AssessmentAnswersComponent
  ]
})
export class AssessmentAnswersModule { }
