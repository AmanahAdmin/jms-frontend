import { Component, OnInit, OnDestroy } from "@angular/core";
import { IntiateJourneyService } from "@jms/app/shared/Services/initiate-journey.service";
import { IntiateJourney } from "@jms/shared/Models/IntialJourneyModel";
import { Router, ActivatedRoute } from "@angular/router";
import { JourneyStatus } from "@jms/app/shared/enums/journey-status.enum";
import { JourneyDriverModel } from "@jms/app/shared/Models/JourneyDriverModel";
import { Subscription } from "rxjs";
import { AuthenticationService } from "@jms/app/shared/Services";
import { MatDialog, MatSelectChange } from "@angular/material";
import { MatDialogConfig } from "@angular/material";
import { RejectJourneyModalComponent } from "./reject-journey-modal/reject-journey-modal.component";
import { JourneyActionsModalComponent } from "./journey-actions-modal/journey-actions-modal.component";
import {
  Driver,
  Vehicle,
} from "@jms/app/shared/Models/journey/journey-details";
import { JourneyVehicleModel } from "@jms/app/shared/Models/JourneyVehicleModel";
import Swal from "sweetalert2";
import { AssessmentTiming } from "@jms/app/shared/enums/assessment-timing";
import { DatePipe } from "@angular/common";
declare const google: any;

interface MapPoint {
  lat: number | string;
  lng: number | string;
  title?: string | object;
}

@Component({
  selector: "app-journey-details",
  templateUrl: "./journey-details.component.html",
  styleUrls: ["./journey-details.component.scss"],
})
export class JourneyDetailsComponent implements OnInit, OnDestroy {
  actionsPermissions = [
    "isAllowedToRequestToRescheduleTheJourney",
    "isAllowedToRequestToChangeDriver",
    "isAllowedToRequestToChangeVehicle",
    "isAllowedToRollBackToDriverToResubmitAssessments",
  ];

  requests: any[] = [];
  vehicleTypes: any[] = [];
  payLoads: any[] = [];
  assessments: any[] = [];
  drivers: JourneyDriverModel[] = [];
  selectedDrivers: any[] = [];
  selectedVehicles: JourneyVehicleModel[] = [];

  defaultLat: number | string = 27.61;
  defaultLng: number | string = 30.32;

  tab: number = 1;

  journeyForm: IntiateJourney = new IntiateJourney(0, "", null, null, JourneyStatus.PendingOnDriverAndVehicleSelection, new Date(), null, "", "", null, null, null, null, false, "", 0, 0, "", null, null, null, true, 0, false, false, false, false, false, false, false, null, null, null, null, [], [], [], [], [], null);

  journeyId: number;
  origin: MapPoint;
  destination: MapPoint;
  subscriptions: Subscription[] = [];

  rejectReason: string;

  vehicles: any[] = [];
  disableVehicleSelection: boolean = false;
  vehicleTypeId: number;
  timing: AssessmentTiming;
  answersIds: number[] = [];
  JourneyStatus = JourneyStatus;
  map: any;
  waypoints: any[] = [];
  renderOptions = {
    draggable: false,
  };

  optimizeWaypoints: boolean = false;
  showActionsButton: boolean = false;
  valuesChanged: boolean = false;

  /**
   * 
   * @param intiateJourneyService 
   * @param router 
   * @param activatedRoute 
   * @param authService 
   * @param dialog 
   * @param datePipe 
   */
  constructor(
    private intiateJourneyService: IntiateJourneyService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    public dialog: MatDialog,
    private datePipe: DatePipe
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.journeyId = params.id;
    });
  }

  ngOnInit() {
    this.journeyDetails();
    this.getData();
  }

  validateFeature(featurename: string) {
    return this.authService.validateFeature(featurename);
  }

  /**
   * reject journey
   *
   *
   */
  // openRejectModal() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.width = "600px";
  //   dialogConfig.data = {
  //     title: "Reject Journey",
  //     placeholder: "Please Enter Rejection Reason",
  //   };
  //   const dialogRef = this.dialog.open(
  //     RejectJourneyModalComponent,
  //     dialogConfig
  //   );
  //   dialogRef.afterClosed().subscribe((data) => {
  //     if (data) {
  //       this.rejectReason = data;
  //       this.rejectJourney();
  //     }
  //   });
  // }

  /**
   * journey actions
   *
   *
   */
  openJourneyActionsModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "600px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      actions: this.journeyForm["currentUserPermission"],
      journeyId: this.journeyId,
    };

    const dialogRef = this.dialog.open(
      JourneyActionsModalComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.journeyDetails();
      }
    });
  }

  /**
   * reject journey
   *
   *
   */
  // rejectJourney() {
  //   this.intiateJourneyService
  //     .rejectJourney(this.journeyId, this.rejectReason)
  //     .subscribe((res) => {
  //       this.router.navigate(["/journey-list"]);
  //     });
  // }

  /**
   * approve
   *
   *
   */
  approveJourney() {
    this.intiateJourneyService
      .approveJourney(this.journeyId)
      .subscribe((res) => {
        this.router.navigate(["/journey-list"]);
      });
  }

  /**
   * close journey
   *
   *
   */
  closeJourney() {
    this.intiateJourneyService
      .approveJourney(this.journeyId)
      .subscribe((res) => {
        this.router.navigate(["/journey-list"]);
      });
  }

  /**
   * journey details
   *
   *
   */
  journeyDetails() {
    this.subscriptions.push(
      this.intiateJourneyService.getJourenyFullDetails(this.journeyId).subscribe((jourenyDetails) => {
        this.journeyForm = jourenyDetails.data;
        this.vehicleTypeId = this.journeyForm.vehicleType.id;

        this.selectedDrivers = this.journeyForm.drivers;
        this.selectedVehicles = this.journeyForm.vehicles;

        if (this.journeyForm.deliveryDate) {
          this.journeyForm.deliveryDate = this.datePipe.transform(new Date(this.journeyForm.deliveryDate), "dd-MM-yyyy - h:mm a");
        }

        this.vehiclesByType(this.journeyForm.vehicleType.id);
        this.driversByType(this.journeyForm.vehicleType.id);

        // from
        if (this.journeyForm["fromLocationId"] != null) {
          this.origin = {
            lat: this.journeyForm["fromLocation"]["latitude"],
            lng: this.journeyForm["fromLocation"]["longitude"],
            title: this.journeyForm["fromLocation"]["name"],
          };
        } else if (this.journeyForm["fromZoneId"] != null) {
          this.origin = {
            lat: this.journeyForm["fromZoneLocations"][0]["lat"],
            lng: this.journeyForm["fromZoneLocations"][0]["lng"],
            title: this.journeyForm.fromZone["name"],
          };

          // draw zone
          this.draw(this.journeyForm["fromZoneLocations"]);
        } else if (this.journeyForm.isMapLocation) {
          this.origin = {
            lat: this.journeyForm.fromLat,
            lng: this.journeyForm.fromLng,
            title: this.journeyForm.fromDestination,
          };
        }

        // to
        if (this.journeyForm["toLocationId"] != null) {
          this.destination = {
            lat: this.journeyForm.toLocation["latitude"],
            lng: this.journeyForm.toLocation["longitude"],
            title: this.journeyForm.toLocation["name"],
          };
        } else if (this.journeyForm["toZoneId"]) {
          this.destination = {
            lat: this.journeyForm["toZoneLocations"][0]["lat"],
            lng: this.journeyForm["toZoneLocations"][0]["lng"],
            title: this.journeyForm.toZone["name"],
          };

          // draw zone
          this.draw(this.journeyForm["toZoneLocations"]);
        } else if (this.journeyForm.isMapLocation) {
          this.destination = {
            lat: this.journeyForm.toLat,
            lng: this.journeyForm.toLng,
            title: this.journeyForm.toDestination,
          };
        }

        // action button permissions
        if (this.journeyForm["currentUserPermission"]) {
          const allowed: object | any = this.journeyForm[
            "currentUserPermission"
          ];
          this.showActionsButton =
            allowed.isAllowedToRequestToRescheduleTheJourney ||
            allowed.isAllowedToRequestToChangeDriver ||
            allowed.isAllowedToRequestToChangeVehicle ||
            allowed.isAllowedToRollBackToDriverToResubmitAssessments;
        }

        // set selected
        this.journeyForm.drivers = this.journeyForm.drivers[0];
        this.journeyForm.vehicles = this.journeyForm.vehicles[0];

        // add checkpoints on map
        this.journeyForm.checkPoints.forEach((checkpoint) => {
          if (
            checkpoint.location["latitude"] != 0 &&
            location["longitude"] != 0
          ) {
            this.waypoints = [
              ...this.waypoints,
              ...[
                {
                  location: {
                    lat: checkpoint["location"]["latitude"],
                    lng: checkpoint["location"]["longitude"],
                  },
                },
              ],
            ];
          } else if (
            checkpoint.zone["latitude"] != 0 &&
            checkpoint.zone["longitude"] != 0
          ) {
            this.waypoints = [
              ...this.waypoints,
              ...[
                {
                  location: {
                    lat: checkpoint["zone"]["latitude"],
                    lng: checkpoint["zone"]["longitude"],
                  },
                },
              ],
            ];
          }
        });
      })
    );
  }

  /**
   * get all lookups
   *
   *
   */
  getData() {
    this.intiateJourneyService.GetLookUps().subscribe((result: any) => {
      const data = result.data;
      this.payLoads = data.payLoads;

      result.data.vehicleTypes.forEach((element) => {
        this.vehicleTypes.push({
          id: 0,
          journeyId: 0,
          vehicleId: element.id,
          vehicleName: element.name,
        });
      });
    });
  }

  /**
   * send form data
   *
   *
   * @param form
   */
  onSubmit() {
    if (this.selectedDrivers.length == 0 && this.journeyForm["currentUserPermission"].isAllowedToRequestToChangeDriver) {
      Swal.fire("Please select driver(s)");
    } else if (this.selectedVehicles.length == 0 && this.journeyForm["currentUserPermission"].isAllowedToRequestToChangeVehicle) {
      Swal.fire("Please select vehicle(s)");
    } else {
      this.save();
    }
  }

  /**
   * save dirvers & vehicle & new date
   *
   *
   */
  save() {
    // drivers
    let driversToPost: any[] = Array.of(this.selectedDrivers);
    driversToPost = [].concat(...driversToPost);

    // vehiclese
    let vehicles: object[] = Array.of(this.selectedVehicles);
    vehicles = [].concat(...vehicles);

    let body = {
      journeyId: this.journeyId,
      vehicleTypeId: this.vehicleTypeId,
      vehicles: vehicles,
      drivers: driversToPost,
    };

    if (this.journeyForm["currentUserPermission"].isAllowedToRescheduleTheJourney) {
      body["actionType"] = 0;
      body["newDeliveryDate"] = new Date(this.journeyForm.deliveryDate);

      this.intiateJourneyService.performTheRequestToHandleJourneyAction(body).subscribe((res: any) => {
        if (res.isSuccessful) {
          this.valuesChanged = false;
          Swal.fire("Journey updated succssfully!");
          this.journeyDetails();
        } else {
          Swal.fire("", res.message, 'error');
        }

      });
    }

    if (this.journeyForm.journeyStatus.statusId == JourneyStatus.PendingOnVehicleSelection || this.journeyForm.journeyStatus.statusId == JourneyStatus.PendingOnDriverAndVehicleSelection) {
      this.intiateJourneyService.editDriversAndVehicles(body).subscribe((res) => {
        this.valuesChanged = false;
        Swal.fire("Journey updated succssfully!");
        this.journeyDetails();
      });
    } else {
      if (this.journeyForm["currentUserPermission"].isAllowedToRescheduleTheJourney || this.journeyForm["currentUserPermission"].isAllowedToChangeDriver || this.journeyForm["currentUserPermission"].isAllowedToChangeVehicle) {
        if (this.journeyForm["currentUserPermission"].isAllowedToRescheduleTheJourney) {
          body["actionType"] = 0;
        } else if (
          this.journeyForm["currentUserPermission"].isAllowedToChangeDriver
        ) {
          body["actionType"] = 1;
        } else if (
          this.journeyForm["currentUserPermission"].isAllowedToChangeVehicle
        ) {
          body["actionType"] = 2;
        }

        this.intiateJourneyService.performTheRequestToHandleJourneyAction(body).subscribe((res: any) => {
          if (res.isSuccessful) {
            this.valuesChanged = false;
            Swal.fire("Journey updated succssfully!");
            this.journeyDetails();
          } else {
            Swal.fire("", res.message, 'error');
          }
        });
      }
    }
  }

  /**
   * select dirver/drivers
   *
   *
   * @param event
   */
  onSelectDrivers(event: MatSelectChange) {
    this.valuesChanged = true;
    this.selectedDrivers = event.value;
  }

  /**
   * select vehicle
   *
   *
   * @param event
   */
  onSelectVehicles(event: MatSelectChange) {
    this.valuesChanged = true;
    this.selectedVehicles = event.value;
  }

  /**
   * vehicles using type id
   *
   *
   */
  onSelectVehicleType(id: number) {
    this.valuesChanged = true;
    this.vehiclesByType(id);
  }

  /**
   * change date
   *
   *
   * @param event
   */
  onChangeDate(event) {
    this.valuesChanged = true;
  }

  /**
   * vehicles via type
   *
   *
   * @param id
   */
  vehiclesByType(id: number) {
    this.subscriptions.push(
      this.intiateJourneyService.Vehicles(id, this.journeyForm.isCompanyVehicle).subscribe((data) => {
        this.vehicles = data.data;

        // selected vehicles
        if (this.vehicles && this.vehicles.length > 0) {
          this.vehicles = this.vehicles.map((item) => {
            return {
              vehicleId: item.vehicleId,
              vehicleName: item.vehicleName,
            };
          });
        }
      })
    );
  }

  /**
   * drivers
   *
   *
   * @param id
   */
  driversByType(id: number) {
    this.subscriptions.push(
      this.intiateJourneyService
        .Drivers(id, this.journeyForm.isCompanyVehicle)
        .subscribe((data) => {
          this.drivers = data.data;

          // selected drivers
          if (this.drivers.length > 0) {
            this.drivers = this.drivers.map((item) => {
              return {
                driverId: item.driverId,
                driverName: item.driverName,
              };
            });
          }
        })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * compare to select default selected
   *
   *
   * @param c1
   * @param c2
   */
  compareVehiclesFn(c1: Vehicle, c2: Vehicle): boolean {
    return c1 && c2 ? c1.vehicleId === c2.vehicleId : c1 === c2;
  }

  /**
   * compare to select default selected
   *
   *
   * @param c1
   * @param c2
   */
  compareDriversFn(c1: Driver, c2: Driver): boolean {
    return c1 && c2 ? c1.driverId === c2.driverId : c1 === c2;
  }

  /**
   * cancel edits
   *
   *
   */
  cancel() {
    this.router.navigate(["journey-list"]);
  }

  /**
   * get map
   *
   *
   * @param map
   */
  onMapReady(map: any) {
    this.map = map;
  }

  /**
   * draw manager
   *
   *
   * @param map
   */
  draw(zone: { lat: string; lng: string }[]) {
    const coords = zone;

    this.defaultLat = coords[0].lat;
    this.defaultLng = coords[0].lng;

    const polygon = new google.maps.Polygon({
      paths: coords,
      strokeColor: "#1E90FF",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#1E90FF",
      fillOpacity: 0.35,
    });

    polygon.setMap(this.map);
  }

  /**
   * check in point at journey
   *
   *
   * @param checkpointId
   */
  checkInJourney(checkpointId: number) {
    const body = {
      journeyId: this.journeyId,
      checkpointId: checkpointId,
    };

    this.intiateJourneyService.checkIn(body).subscribe((res) => {
      // refersh details
      this.journeyDetails();

      if (!res.isSuccessful) {
        Swal.fire("", res.message, "error");
      } else {
        Swal.fire("", res.message, "success");
      }
    });
  }
}
