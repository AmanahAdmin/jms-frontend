import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectJourneyModalComponent } from './reject-journey-modal.component';

describe('RejectJourneyModalComponent', () => {
  let component: RejectJourneyModalComponent;
  let fixture: ComponentFixture<RejectJourneyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectJourneyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectJourneyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
