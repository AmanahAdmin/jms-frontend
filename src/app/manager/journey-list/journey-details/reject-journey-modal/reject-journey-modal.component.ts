import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-reject-journey-modal',
  templateUrl: './reject-journey-modal.component.html',
  styleUrls: ['./reject-journey-modal.component.scss']
})
export class RejectJourneyModalComponent {

  rejectReason: string;
  @Input() title: string;
  @Input() placeholder: string;
  isSubmit: boolean = false;

  /**
   * 
   * @param dialogRef refrence for modal
   * @param data get received data for binding
   */
  constructor(
    private dialogRef: MatDialogRef<RejectJourneyModalComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {

    dialogRef.disableClose = true;
    this.title = data.title;
    this.placeholder = data.placeholder;
  }

  /**
   * submit action
   */
  submit() {
    this.isSubmit = true;
    if (!this.rejectReason)
      return;
    this.dialogRef.close(this.rejectReason);
  }

  /**
   * close modal dialog
   */
  close() {
    this.dialogRef.close();
  }
}
