import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-un-merge-journey-modal',
  templateUrl: './un-merge-journey-modal.component.html',
  styleUrls: ['./un-merge-journey-modal.component.scss']
})
export class UnMergeJourneyModalComponent implements OnInit {

  /**
   * 
   * @param dialogRef 
   */
  constructor(
    private dialogRef: MatDialogRef<UnMergeJourneyModalComponent>,
  ) { }

  ngOnInit() {
  }

  /**
   * un merge journey
   * 
   * 
   */
  unmerge() {
    this.dialogRef.close(true);
  }

  /**
   * close dialog modal
   * 
   * 
   */
  close() {
    this.dialogRef.close();
  }
}
