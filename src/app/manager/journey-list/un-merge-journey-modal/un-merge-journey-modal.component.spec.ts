import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnMergeJourneyModalComponent } from './un-merge-journey-modal.component';

describe('UnMergeJourneyModalComponent', () => {
  let component: UnMergeJourneyModalComponent;
  let fixture: ComponentFixture<UnMergeJourneyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnMergeJourneyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnMergeJourneyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
