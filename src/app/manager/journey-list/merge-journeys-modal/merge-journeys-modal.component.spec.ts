import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeJourneysModalComponent } from './merge-journeys-modal.component';

describe('MergeJourneysModalComponent', () => {
  let component: MergeJourneysModalComponent;
  let fixture: ComponentFixture<MergeJourneysModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeJourneysModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeJourneysModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
