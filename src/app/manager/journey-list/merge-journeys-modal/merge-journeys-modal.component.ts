import {
  CdkDragDrop,
  CdkDropList,
  moveItemInArray,
} from "@angular/cdk/drag-drop";
import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import {
  MergedJourney,
  mergedJourneysModal,
} from "@jms/app/shared/Models/merged-journey.model";
import { SwalService } from "@jms/app/shared/Services";
import { IntiateJourneyService } from "@jms/app/shared/Services/initiate-journey.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-merge-journeys-modal",
  templateUrl: "./merge-journeys-modal.component.html",
  styleUrls: ["./merge-journeys-modal.component.scss"],
})
export class MergeJourneysModalComponent {
  mergedJourneys: MergedJourney[] = [];
  tempMergedJourneys: MergedJourney[] = [];

  @Output() onDelete = new EventEmitter<void>();
  @ViewChild("dropList", { static: true }) dropList: CdkDropList;

  /**
   *
   * @param dialogRef
   * @param intiateJourneyService
   * @param swalService
   * @param data
   */
  constructor(
    private dialogRef: MatDialogRef<MergeJourneysModalComponent>,
    private intiateJourneyService: IntiateJourneyService,
    private swalService: SwalService,

    @Inject(MAT_DIALOG_DATA) data
  ) {
    dialogRef.disableClose = true;
    this.mergedJourneys = data.mergedJourneys;
    this.tempMergedJourneys = [...this.mergedJourneys];
  }

  /**
   *
   * @param event
   * rearrange journeys with drag and drop
   */
  drop(event: CdkDragDrop<MergedJourney[]>) {
    moveItemInArray(
      this.mergedJourneys,
      event.previousIndex,
      event.currentIndex
    );
    this.mergedJourneys[0].isHold = false;
  }

  setJourneyOnHold(journey: MergedJourney, index: number) {
    if (index == 0) {
      Swal.fire("", `First Journey can't be Hold`, "error");
      return;
    }
    journey.isHold = true;
  }

  /** submit merged journeys to be saved */
  submit() {
    let mergedListModel = new mergedJourneysModal();
    const mergedList = this.mergedJourneys.map((item, index) => {
      return {
        journeyId: item.journeyId,
        isHold: item.isHold,
        order: index + 1,
      };
    });
    mergedListModel.mergedJourneys = mergedList;
    mergedListModel.driverId = this.mergedJourneys[0].driverId;
    mergedListModel.vehicleId = this.mergedJourneys[0].vehicleId;
    mergedListModel.vehicleTypeId = this.mergedJourneys[0].vehicleTypeId;
    this.intiateJourneyService.mergeJourneys(mergedListModel).subscribe(
      (res) => {
        if (res.isSuccessful) {
          this.swalService.showCustomMessages({
            type: "success",
            messages: ["Journeys Merged successfully"],
          });
          this.dialogRef.close(res);
        } else {
          this.swalService.showCustomMessages({
            type: "error",
            messages: [res.message],
          });
        }
      },
      (err) => {
        this.swalService.showCustomMessages({
          type: "error",
          messages: [err],
        });
      }
    );
  }

  /**
   * close dialog modal
   */
  close() {
    this.mergedJourneys = this.tempMergedJourneys;
    this.dialogRef.close(this.tempMergedJourneys);
  }

  /**
   *
   * @param index
   * delete journey before merge with another journeys
   */
  delete(index) {
    this.mergedJourneys.splice(index, 1);
    this.onDelete.emit();
  }
}
