import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditJourneyRoutingModule } from './edit-journey-routing.module';
import { EditJourneyComponent } from './edit-journey.component';
import {
  MatTableModule,
  MatIconModule,
  MatDialogModule,
  MatPaginatorModule,
  MatSortModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatCardModule,
  MatMenuModule,
  MatSelectModule,
  MatDividerModule,
  MatProgressBarModule,
  MatAutocompleteModule,
  MatRadioModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { AgmDrawingModule } from '@agm/drawing';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AssessmentAnswersModule } from '../journey-list/journey-details/assessment-answers/assessment-answers.module';
import { CommunicationPointsModule } from '../journey-list/journey-details/communication-points/communication-points.module';

@NgModule({
  declarations: [EditJourneyComponent],
  imports: [
    CommonModule,
    EditJourneyRoutingModule,
    CommonModule,

    // material
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatRadioModule,

    AgmCoreModule,
    AgmDirectionModule,
    AgmDrawingModule,
    GooglePlaceModule,

    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,

    MatSelectModule,
    MatDividerModule,

    MatProgressBarModule,
    MatAutocompleteModule,

    FormsModule,
    ReactiveFormsModule,
    AssessmentAnswersModule,
    CommunicationPointsModule
  ]
})
export class EditJourneyModule { }
