import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { IntiateJourneyService } from "@jms/app/shared/Services/initiate-journey.service";
import { FormGroup, NgForm } from "@angular/forms";
import swal from "sweetalert2";
import { Zone } from "@jms/app/shared/Entities/admin/Zone";
import { Level } from "@jms/app/shared/enums/level.enum";
import { ResponseStatus } from "@jms/app/shared/enums/response-status.enum";
import { JourneyStatus } from "@jms/app/shared/enums/journey-status.enum";

// models
import { IntiateJourney } from "@jms/app/shared/Models/IntialJourneyModel";
import { JourneyAssessmentModel } from "@jms/app/shared/Models/JourneyAssessmentModel";
import { JourneyDriverModel } from "@jms/app/shared/Models/JourneyDriverModel";
import { JourneyCargoModel } from "@jms/app/shared/Models/journey/JourneyCargoModel";
import { JourneyCheckPointModel } from "@jms/app/shared/Models/journey/JourneyCheckPointModel";
import { JourneyPassengerModel } from "@jms/app/shared/Models/journey/JourneyPassengerModel";
import { JourneyTruckModel } from "@jms/app/shared/Models/journey/JourneyTruckModel";
import { JourneyVehicleModel } from "@jms/app/shared/Models/JourneyVehicleModel";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { MatRadioChange, MatSelectChange } from "@angular/material";
import { Calendar } from "@jms/app/constants/calendar";
import { Subscription } from "rxjs";
import { Driver, Vehicle } from "@jms/app/shared/Models/journey/journey-details";
import mapPointToFixed from "@jms/app/shared/Helpers/number-to-fxed";
declare const google: any;
import Swal from "sweetalert2";
import { element } from 'protractor';

interface MapPoint {
  lat: string | number;
  lng: string | number;
  title?: string;
}

@Component({
  selector: "app-edit-journey",
  templateUrl: "./edit-journey.component.html",
  styleUrls: ["./edit-journey.component.scss"],
})
export class EditJourneyComponent implements OnInit {
  errorMsg: string = "";
  form: FormGroup;
  isSubmit: boolean = false;
  isValidPeople: boolean = false;
  isValidCommunicationsPoints: boolean = true;
  isValidAssessment: boolean = true;
  requests: any[] = [];
  zones: Zone[] = [];
  vehicleTypes: any[] = [];
  departments: any[] = [];
  locations: any[] = [];
  payloadTypes: any[] = [];
  payLoads: any[] = [];
  assessmentCategories: any[] = [];
  assessments: any[] = [];
  listAssessments: any[] = [];
  selectedPayloadTypes: number[] = [];
  drivers: JourneyDriverModel[] = [];
  vehicles: any[] = [];
  showHideVehicle = true;
  showHideDriver = true;
  isValidCheckPoint: boolean = false;
  defaultLat: number = 29.3117;
  defaultLng: number = 47.4818;
  selectedDrivers: any[] = [];
  selectedVehicles: JourneyVehicleModel[] = [];
  vehicleTypeId: number;

  origin: MapPoint = {
    lat: 0,
    lng: 0,
  };

  destination: MapPoint = {
    lat: 0,
    lng: 0,
  };

  waypoints: any[] = [];
  renderOptions = {
    draggable: true,
  };
  optimizeWaypoints: boolean = false;

  priorityDegrees: Level;
  RecurringEvery = "1";
  EndOptions;
  selectedCheckpoint: JourneyCheckPointModel;
  tab = 1;
  checkpoints: JourneyCheckPointModel[] = [];
  isDefaultAddCargoAssessmentsLoaded: boolean = false;
  isJourneyFromToVisible: boolean = false;

  days = Calendar.days;
  months = Calendar.months;
  times = Calendar.times;
  eta = Calendar.etaList;

  journeyForm: IntiateJourney = new IntiateJourney(0, "", null, null, JourneyStatus.PendingOnDriverAndVehicleSelection, new Date(), null, "", "", null, null, null, null, false, "", 0, 0, "", null, null, null, true, 0, false, false, false, false, false, false, false, null, null, null, null, [], [], [], [], [], null);

  zoneFrom: any;
  zoneTo: any;
  map: any;

  edit: boolean;
  editJourneyId: number;
  subscriptions: Subscription[] = [];
  mapPointToFixed = mapPointToFixed;
  journeyStatus = JourneyStatus;
  checkPointError: boolean = false;

  journeyLoaded: boolean = false;

  /**
   *
   * @param intiateJourneyService
   * @param router
   * @param activatedRoute
   */
  constructor(
    private intiateJourneyService: IntiateJourneyService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.editJourneyId = params["id"];
    });
  }

  ngOnInit() {
    this.getData();
    this.journeyDetails();
    this.addCheckPoint();
  }

  /**
   * journey details
   *
   *
   */
  journeyDetails() {
    this.subscriptions.push(
      this.intiateJourneyService.getJourenyFullDetails(this.editJourneyId).subscribe((jourenyDetails) => {
        this.journeyForm = jourenyDetails.data;
        this.vehicleTypeId = this.journeyForm.vehicleType.id;

        this.selectedDrivers = this.journeyForm.drivers;
        this.selectedVehicles = this.journeyForm.vehicles;

        this.vehiclesByType(this.journeyForm.vehicleType.id);
        this.driversByType(this.journeyForm.vehicleType.id);

        this.journeyForm.journeyAssessments.forEach(element => {
          this.getAssessmentByCategory(element.assessmentCategoryId);
        });

        if (this.journeyForm["fromLocation"]["latitude"] != 0 && this.journeyForm["fromLocation"]["longitude"] != 0) {
          this.origin = {
            lat: this.mapPointToFixed(this.journeyForm["fromLocation"]["latitude"]),
            lng: this.mapPointToFixed(this.journeyForm["fromLocation"]["longitude"]),
            title: this.journeyForm["fromLocation"]["name"],
          };
        } else if (this.journeyForm["fromZone"]["latitude"] != 0 && this.journeyForm["fromZone"]["longitude"] != 0) {
          this.origin = {
            lat: this.mapPointToFixed(this.journeyForm["fromZoneLocations"][0]["lat"]),
            lng: this.mapPointToFixed(this.journeyForm["fromZoneLocations"][0]["lng"]),
            title: this.journeyForm.fromZone["name"],
          };

          // draw zone
          this.draw(this.journeyForm["fromZoneLocations"], 1);
        } else if (this.journeyForm.isMapLocation) {
          this.origin = {
            lat: this.mapPointToFixed(this.journeyForm.fromLat),
            lng: this.mapPointToFixed(this.journeyForm.fromLng),
          };
        }

        // to
        if (this.journeyForm.toLocation["latitude"] != 0 && this.journeyForm.toLocation["longitude"] != 0) {
          this.destination = {
            lat: this.mapPointToFixed(this.journeyForm.toLocation["latitude"]),
            lng: this.mapPointToFixed(this.journeyForm.toLocation["longitude"]),
            title: this.journeyForm.toLocation["name"],
          };
        } else if (this.journeyForm.toZone["latitude"] != 0 && this.journeyForm.toZone["longitude"] != 0) {
          this.destination = {
            lat: this.mapPointToFixed(this.journeyForm["toZoneLocations"][0]["lat"]),
            lng: this.mapPointToFixed(this.journeyForm["toZoneLocations"][0]["lng"]),
            title: this.journeyForm.toZone["name"],
          };

          // draw zone
          this.draw(this.journeyForm["toZoneLocations"], 0);
        } else if (this.journeyForm.isMapLocation) {
          this.destination = {
            lat: this.mapPointToFixed(this.journeyForm.toLat),
            lng: this.mapPointToFixed(this.journeyForm.toLng),
          };
        }

        this.journeyLoaded = true;

        // set selected
        this.journeyForm.vehicles = this.journeyForm.vehicles[0];
        this.journeyForm.drivers = this.journeyForm.drivers[0];

        // add checkpoints on map
        this.journeyForm.checkPoints.forEach((checkpoint) => {
          if (!checkpoint['isMapLocation']) {
            if (checkpoint.location["latitude"] != 0 && location["longitude"] != 0) {
              this.waypoints = [
                ...this.waypoints,
                ...[
                  {
                    location: {
                      lat: this.mapPointToFixed(checkpoint["location"]["latitude"]),
                      lng: this.mapPointToFixed(checkpoint["location"]["longitude"]),
                    },
                    stopover: true,
                  },
                ],
              ];
            } else if (checkpoint.zone["latitude"] != 0 && checkpoint.zone["longitude"] != 0) {
              this.waypoints = [
                ...this.waypoints,
                ...[
                  {
                    location: {
                      lat: this.mapPointToFixed(checkpoint["zone"]["latitude"]),
                      lng: this.mapPointToFixed(checkpoint["zone"]["longitude"]),
                    },
                    stopover: true,
                  },
                ],
              ];
            }
          } else {
            this.waypoints = [
              ...this.waypoints,
              ...[
                {
                  location: {
                    lat: this.mapPointToFixed(checkpoint["fromLat"]),
                    lng: this.mapPointToFixed(checkpoint["fromLng"]),
                  },
                  stopover: true,
                },
              ],
            ];
          }
        });
      }));
  }

  /**
   * On Change Zone/Location
   * in from/ to journey
   *
   */
  onLocationSlotChange(event: any, flag: number) {
    const selectedIndex = event.target.selectedIndex;
    const selectedValue = event.target.value;
    const optGroupLabel = event.target.options[
      selectedIndex
    ].parentNode.getAttribute("label");

    if (optGroupLabel == "Zones") {
      // get selected zone
      const selectedZone = this.zones.filter((zone) => {
        return zone.id == selectedValue;
      })[0];

      if (selectedIndex && selectedZone.zoneLocations.length > 0) {
        this.draw(selectedZone.zoneLocations, flag);
      }

      if (flag == 1) {
        this.journeyForm.fromZone = selectedValue;
        this.journeyForm.fromLocation = null;
      } else {
        this.journeyForm.toZone = selectedValue;
        this.journeyForm.toLocation = null;
      }
    } else {
      // get selected location
      const selectedLocation = this.locations.filter((location) => {
        return location.id == selectedValue;
      })[0];

      if (flag == 1) {
        this.journeyForm.fromZone = null;
        this.journeyForm.fromLocation = selectedValue;

        this.origin = {
          lat: selectedLocation["latitude"],
          lng: selectedLocation["longitude"],
        };

        if (this.zoneFrom) {
          this.zoneFrom.setMap(null);
        }
      } else {
        this.journeyForm.toZone = null;
        this.journeyForm.toLocation = selectedValue;

        this.destination = {
          lat: selectedLocation["latitude"],
          lng: selectedLocation["longitude"],
        };

        if (this.zoneTo) {
          this.zoneTo.setMap(null);
        }
      }
    }
  }

  /**
   * On Change Zone/Location in Checkpoints
   *
   *
   * @param event
   * @param index
   */
  onSlotChange(event: any, pointIndex: number) {
    this.isValidCheckPoint = true;

    const selectedIndex = event.target.selectedIndex;
    var point = this.journeyForm.checkPoints[pointIndex];

    const selectedValue = event.target.value;
    const optGroupLabel = event.target.options[
      selectedIndex
    ].parentNode.getAttribute("label");

    if (optGroupLabel == "Zones") {
      // get selected zone
      const selectedZone = this.zones.filter((zone) => {
        return zone.id == selectedValue;
      })[0];

      const zonePoint = {
        location: {
          lat: selectedZone.latitude,
          lng: selectedZone.longitude,
        },
        stopover: true,
      };

      this.waypoints[pointIndex] = zonePoint;
      this.waypoints = [...this.waypoints];

      point.zoneId = selectedValue;
      point.locationId = null;
    } else {
      // get selected location
      const selectedLocation = this.locations.filter((location) => {
        return location.id == selectedValue;
      })[0];

      const locationPoint = {
        location: {
          lat: selectedLocation.latitude,
          lng: selectedLocation.longitude,
        },
        stopover: true,
      };

      this.waypoints[pointIndex] = locationPoint;
      this.waypoints = [...this.waypoints];

      point.zoneId = null;
      point.locationId = selectedValue;
    }

    this.validateCheckPoints();
  }

  /**
   * default assessments
   *
   *
   */
  getDefaultPreAssessment() {
    this.intiateJourneyService.GetDEfaultAssessments(1).subscribe((result) => {
      result.data.forEach((element, index) => {
        this.getAssessmentByCategory(element.assessmentCategoryId);
      });
    });
  }

  /**
   * new checkpoint
   *
   *
   */
  addCheckPoint() {
    const point = this.journeyForm.checkPoints[
      this.journeyForm.checkPoints.length - 1
    ];

    this.checkPointError = false;
    if (point && (!point.IsMapLocation) && point.zoneId == null && point.locationId == null) {
      return (this.checkPointError = true);
    }

    this.journeyForm.checkPoints.push(new JourneyCheckPointModel(0, 0, false, "0", 0, 0, null, null, false, "", "", "", "", "", "", 0, null, null, true));
    this.isHaveCargo();
  }

  /**
   * have cargo
   *
   *
   */
  isHaveCargo(event?: any) {
    const isHaveList = this.journeyForm.checkPoints.filter(
      (a) => a.haveCargo == true
    );
    if (isHaveList.length != 0 && !this.isDefaultAddCargoAssessmentsLoaded) {
      this.getAllDefaultAddCargoAssessments();
      this.isDefaultAddCargoAssessmentsLoaded = true;
    } else {
      // delete cargo
      const toRemoveList = this.journeyForm.journeyAssessments.filter(
        (a) => a.assessmentTiming == 2
      );
      const startIndex = this.journeyForm.journeyAssessments.indexOf(
        toRemoveList[0]
      );
      this.journeyForm.journeyAssessments.splice(
        startIndex,
        toRemoveList.length
      );
    }
  }

  /**
   * assessments
   *
   *
   */
  getAllDefaultAddCargoAssessments() {
    this.intiateJourneyService.GetAllDefaultAddCargoAssessments(1).subscribe((result: any) => {
      result.data.forEach((element: any) => {
        const indexAt = +this.journeyForm.journeyAssessments.length + 1;
        this.journeyForm.journeyAssessments.splice(indexAt - 1, 0, element);
        this.getAssessmentByCategory(element.assessmentCategoryId);
      });
    });
  }

  /**
   *
   * @param index
   */
  iniCheckPoint(index: number) {
    var point = this.journeyForm.checkPoints[index];
    point.locationId = null;
    point.zoneId = null;
    point.IsMapLocation = !point.IsMapLocation;
    point.Order = index + 1;

    const autocompleteFormField = document.getElementById(
      `checkpointAddressClass` + index
    ) as HTMLInputElement;
    let checkpointAddress = new google.maps.places.Autocomplete(
      autocompleteFormField,
      {}
    );

    checkpointAddress.addListener("place_changed", () => {
      let place: google.maps.places.PlaceResult = checkpointAddress.getPlace();
      if (place.geometry === undefined || place.geometry === null) {
        return;
      } else {
        point.FromLocation = place.formatted_address;
        point.FromLocationLat = +place.geometry.location.lat().toString();
        point.FromLocationLng = +place.geometry.location.lng().toString();

        point.IsMapLocation = true;
        this.waypoints = [
          ...this.waypoints,
          ...[
            {
              location: {
                lat: point.FromLocationLat,
                lng: point.FromLocationLng,
              },
            },
          ],
        ];
      }
    });
  }

  /**
   * show / hide drivers or vehicles
   *
   *
   * @param e
   * @param ispajero
   */
  showHide(e: any, ispajero) {
    if (e.value === false) {
      this.showHideDriver = false;
      this.showHideVehicle = false;
    } else if (e.value === true) {
      this.showHideDriver = true;
      this.showHideVehicle = true;
    } else {
      this.showDriver(e, ispajero);
      this.showVehicle(e, ispajero);
    }
  }

  /**
   * select company type
   *
   *
   * @param event
   */
  onChangeCompany(event: MatRadioChange) {
    // reset selected previous values
    this.journeyForm.drivers = [];
    this.journeyForm.vehicles = [];

    const value = event.value;

    if (value) {
      this.showHideDriver = true;
      this.showHideVehicle = true;

      if (this.journeyForm.isPajero) {
        this.showHideVehicle = false;
        this.showHideDriver = false;
      } else {
        this.showHideVehicle = true;
        this.showHideDriver = true;
      }
    } else {
      this.showHideDriver = false;
      this.showHideVehicle = false;
    }
  }

  /**
   * show drivers
   *
   *
   * @param e
   * @param ispajero
   */
  showDriver(e, ispajero) {
    if (e === true && ispajero === false) {
      this.showHideDriver = true;
    } else if (e === true && ispajero === true) {
      this.showHideDriver = true;
    }

    return this.showHideDriver;
  }

  /**
   * show vehicles
   *
   *
   * @param e
   * @param ispajero
   */
  showVehicle(e, ispajero) {
    if (e === true && ispajero === false) {
      this.showHideVehicle = true;
    } else if (e === true && ispajero === true) {
      this.showHideVehicle = false;
    }

    return this.showHideVehicle;
  }

  /**
   * check payload
   *
   *
   * @param index
   * @param pay
   */
  checkPayload(index: number, pay): boolean {
    const car = this.journeyForm.cargoes[index];
    if (
      car["payloadTypes"].length > 0 &&
      car["payloadTypes"].some(
        (type) => type.payloadTypeId == pay.PayloadTypeId
      )
    ) {
      return true;
    }
  }

  /**
   * add payload
   *
   *
   * @param type
   * @param index
   * @param isChecked
   */
  addPayload(type: any, index: number, isChecked: boolean) {
    switch (type.name) {
      case "People":
        {
          var car = this.journeyForm.cargoes[index];
          if (isChecked) {
            this.isValidPeople = false;
            const people = new JourneyPassengerModel(0, "", "");
            if (car.journeyPassengers.length == 0) {
              car.journeyPassengers.push(people);
            } else {
              car.PayloadTypes.push(type);
            }
          } else {
            car.journeyPassengers = [];

            const indexType = car.PayloadTypes.indexOf(type);
            car.PayloadTypes.splice(indexType, 1);
          }
        }
        break;

      default:
        if (isChecked) {
          var car = this.journeyForm.cargoes[index];
          const good = new JourneyTruckModel(0, 0, null, "");
          if (car.journeyTrucks.length == 0) {
            car.journeyTrucks.push(good);
          } else {
            car.PayloadTypes.push(type);
          }
        } else {
          const indexType = car.PayloadTypes.indexOf(type);
          car.PayloadTypes.splice(indexType, 1);
        }
        break;
    }
  }

  /**
   * new payload
   *
   *
   * @param type
   * @param index
   */
  addNewPayload(type, index) {
    switch (type) {
      case "People":
        {
          var car = this.journeyForm.cargoes[index];
          var lastPeople =
            car.journeyPassengers[car.journeyPassengers.length - 1];
          console.log(lastPeople);
          if (lastPeople) {
            if (lastPeople.fullName == "" || lastPeople.contactNumber == "") {
              this.isValidPeople = false;
              return;
            }
          }
          this.isValidPeople = false;
          const people = new JourneyPassengerModel(0, "", "");
          car.journeyPassengers.push(people);
        }
        break;

      default:
        var car = this.journeyForm.cargoes[index];
        const good = new JourneyTruckModel(0, 0, null, "");
        car.journeyTrucks.push(good);
        break;
    }
  }

  /**
   * new cargo
   *
   *
   */
  addCargo() {
    this.journeyForm.cargoes.push(new JourneyCargoModel(0, 0, [], [], []));
  }

  /**
   * add new assesment
   *
   *
   */
  addAssessment() {
    this.isValidAssessment = true;
    const assessment = this.journeyForm.journeyAssessments[
      this.journeyForm.journeyAssessments.length - 1
    ];

    if (
      assessment &&
      (assessment.assessmentTiming == null ||
        assessment.assessmentCategoryId == 0 ||
        assessment.assessmentId == 0)
    ) {
      return (this.isValidAssessment = false);
    } else {
      this.journeyForm.journeyAssessments.push(
        new JourneyAssessmentModel(0, null, 0, 0, 0, false, "", true)
      );
    }
  }

  /**
   * remove assessment
   *
   *
   * @param index
   * @param assessment
   */
  removeAssessment(index: number) {
    this.journeyForm.journeyAssessments.splice(index, 1);
  }

  /**
   * get all lookups
   *
   *
   */
  getData() {
    this.intiateJourneyService.GetLookUps().subscribe((result: any) => {
      const data = result.data;

      this.departments = data.departments;
      this.requests = data.requestTypes;

      result.data.vehicleTypes.forEach((element) => {
        this.vehicleTypes.push({
          id: 0,
          journeyId: 0,
          vehicleId: element.id,
          vehicleName: element.name,
          isPajero: element.IsPajero,
        });
      });

      this.zones = data.zones;
      this.locations = data.locations;

      result.data.payLoadTypes.forEach((element: any) => {
        this.payloadTypes.push({
          PayloadTypeId: element.id,
          name: element.name,
        });
      });

      this.payLoads = data.payLoads;
      this.assessmentCategories = data.assessmentCategories;
    });
  }

  /**
   * assessment by category
   *
   *
   * @param categoryId
   * @param index
   */
  getAssessmentByCategory(categoryId: number) {
    this.intiateJourneyService.GetAssessmentByCategory(categoryId, 1).subscribe((result: any) => {
      this.assessments[categoryId] = result.data;
    });
  }

  /**
   * list vehicles
   *
   *
   */
  getVehicles() {
    this.intiateJourneyService
      .Vehicles(
        this.journeyForm.vehicleTypeId,
        this.journeyForm.isCompanyVehicle
      )
      .subscribe((result: any) => {
        this.vehicles = result.data;
      });
  }

  /**
   * fetch drivers
   *
   *
   */
  getDrivers() {
    this.getVehicles();
    this.showHide(this.journeyForm.isCompanyVehicle, this.journeyForm.ispajero);

    if (this.journeyForm.vehicleTypeId == 0) {
      return;
    }

    this.intiateJourneyService
      .Drivers(
        this.journeyForm.vehicleTypeId,
        this.journeyForm.isCompanyVehicle
      )
      .subscribe((result: any) => {
        this.drivers = result.data;
      });
  }

  /**
   * update values
   *
   *
   *
   * @param form
   */
  onSubmit(form: NgForm) {
    this.isValidCheckPoint = true;
    this.isValidAssessment = true;
    this.isSubmit = true;

    if (form.invalid && this.checkPointError) {
      return (this.errorMsg = "Please Fill All Mandatory Data");
    } else {
      this.errorMsg = "";
    }

    if (this.journeyForm.journeyAssessments.length == 0) {
      return (this.errorMsg = "Please Enter at least one assessment");
    } else {
      this.errorMsg = "";
    }

    // validate communications points
    if (
      !this.isValidCommunicationsPoints &&
      this.journeyForm["journeyCommunicationPoints"].length > 0
    ) {
      return;
    }

    if (this.isJourneyFromToVisible) {
      this.journeyForm.fromLocation = null;
      this.journeyForm.toLocation = null;

      this.journeyForm.fromZone = null;
      this.journeyForm.toZone = null;

      this.journeyForm.isMapLocation = true;
    } else {
      this.journeyForm.isMapLocation = false;

      if (
        this.journeyForm.fromLocation != null &&
        this.journeyForm.fromLocation["id"] >= 0
      ) {
        this.journeyForm.fromLocation = this.journeyForm.fromLocation["id"];
        this.journeyForm.fromZone = null;
      }

      if (
        this.journeyForm.toLocation != null &&
        this.journeyForm.toLocation["id"] >= 0
      ) {
        this.journeyForm.toLocation = this.journeyForm.toLocation["id"];
        this.journeyForm.toZone = null;
      }

      if (
        this.journeyForm.fromZone != null &&
        this.journeyForm.fromZone["id"] >= 0
      ) {
        this.journeyForm.fromLocation = null;
        this.journeyForm.fromZone = this.journeyForm.fromZone["id"];
      }

      if (
        this.journeyForm.toZone != null &&
        this.journeyForm.toZone["id"] >= 0
      ) {
        this.journeyForm.toLocation = null;
        this.journeyForm.toZone = this.journeyForm.toZone["id"];
      }
    }

    let formToPost = JSON.parse(JSON.stringify(this.journeyForm));

    // drivers
    let drivers: any[] = Array.of(formToPost.drivers);
    formToPost.drivers = [].concat(...drivers);

    // vehiclese
    let vehicles: object[] = Array.of(formToPost.vehicles);
    formToPost.vehicles = [].concat(...vehicles);

    formToPost['id'] = this.editJourneyId;
    formToPost['journeyStatus'] = formToPost['journeyStatus']['statusId'];
    this.intiateJourneyService.update(formToPost).subscribe((response: any) => {
      if (response.isSuccessful) {
        swal
          .fire({
            title: "Success",
            icon: "success",
            text: "Journey updated Sucessfully.",
            allowOutsideClick: false,
          })
          .then((end) => {
            if (end) {
              this.router.navigate(["/"]);
            }
          });
      } else {
        swal.fire({
          icon: "error",
          text: response.message,
        });

        this.journeyForm.drivers = [];
        this.journeyForm.vehicles = [];
      }

      // for server error
      if (response.status == ResponseStatus.ServerError) {
        swal.fire({
          title: "Error",
          icon: "error",
          text: response.message,
        });
      }
    });
  }

  /**
   * new people row for passengers
   *
   *
   * @param icargoIndex
   * @param index
   * @param item
   */
  onAddPeopleRow(icargoIndex, index, item) {
    if (item.contactNumber) {
      this.journeyForm.cargoes[icargoIndex].journeyPassengers[
        index
      ].contactNumber = item.contactNumber;
    }

    if (item.fullName) {
      this.journeyForm.cargoes[icargoIndex].journeyPassengers[index].fullName =
        item.fullName;
    }

    if (item.fullName && item.contactNumber) {
      this.isValidPeople = true;
    }
  }

  /**
   * get map
   *
   *
   * @param map
   */
  onMapReady(map: any) {
    this.map = map;
  }

  /**
   * draw manager
   *
   *
   * @param zone
   * @param flag
   */
  draw(zone: { lat: string | number; lng: string | number }[], flag: number) {
    const coords = zone;
    coords.forEach((coord: any) => {
      coord.lat = this.mapPointToFixed(coord.lat);
      coord.lng = this.mapPointToFixed(coord.lng);
    });

    this.defaultLat = this.mapPointToFixed(+coords[0].lat);
    this.defaultLng = this.mapPointToFixed(+coords[0].lng);

    const polygon = new google.maps.Polygon({
      paths: coords,
      strokeColor: "#1E90FF",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#1E90FF",
      fillOpacity: 0.35,
    });

    switch (flag) {
      case 1:
        if (this.zoneFrom) {
          this.zoneFrom.setMap(null);
        }

        this.zoneFrom = polygon;
        this.zoneFrom.setMap(this.map);

        this.origin = {
          lat: this.mapPointToFixed(coords[0]["lat"]),
          lng: this.mapPointToFixed(coords[0]["lng"]),
        };
        break;

      default:
        if (this.zoneTo) {
          this.zoneTo.setMap(null);
        }

        this.zoneTo = polygon;
        this.zoneTo.setMap(this.map);

        this.destination = {
          lat: this.mapPointToFixed(coords[0]["lat"]),
          lng: this.mapPointToFixed(coords[0]["lng"]),
        };
        break;
    }
  }

  /**
   * on select address
   *
   *
   * @param address
   * @param type
   */
  handleAddressChange(address: Address, type: string) {
    switch (type) {
      case "origin":
        this.origin = {
          lat: address.geometry.location.lat(),
          lng: address.geometry.location.lng(),
        };

        this.journeyForm.fromLat = +this.origin.lat;
        this.journeyForm.fromLng = +this.origin.lng;
        this.journeyForm.fromDestination = address.formatted_address;
        break;

      default:
        this.destination = {
          lat: address.geometry.location.lat(),
          lng: address.geometry.location.lng(),
        };

        this.journeyForm.toLat = +this.destination.lat;
        this.journeyForm.toLng = +this.destination.lng;
        this.journeyForm.toDestination = address.formatted_address;
        break;
    }

    // zoom to new location
    this.defaultLat = address.geometry.location.lat();
    this.defaultLng = address.geometry.location.lng();
  }

  /**
   * show / hide map location
   *
   *
   */
  showHideFromTo() {
    this.isJourneyFromToVisible = !this.isJourneyFromToVisible;

    if (this.isJourneyFromToVisible) {
      this.journeyForm.isMapLocation = true;

      // reset location
      this.journeyForm.toLocation = 0;
      this.journeyForm.fromLocation = 0;

      // reset zone
      this.journeyForm.toZone = 0;
      this.journeyForm.fromZone = 0;
    } else {
      this.journeyForm.isMapLocation = false;
    }
  }

  /**
   * select dirver/drivers
   *
   *
   * @param event
   */
  onSelectDrivers(event: MatSelectChange) {
    this.selectedDrivers = event.value;
  }

  /**
   * vehicles via type
   *
   *
   * @param id
   */
  vehiclesByType(id: number) {
    this.intiateJourneyService.CheckISPajero(id).subscribe((result: any) => {
      this.journeyForm.ispajero = result.data;
      this.showHide(
        this.journeyForm.isCompanyVehicle,
        this.journeyForm.ispajero
      );
    });

    this.getDrivers();

    this.subscriptions.push(
      this.intiateJourneyService
        .Vehicles(id, this.journeyForm.isCompanyVehicle)
        .subscribe((data) => {
          this.vehicles = data.data;

          // selected vehicles
          if (this.vehicles.length > 0) {
            this.vehicles = this.vehicles.map((item) => {
              return {
                vehicleId: item.vehicleId,
                vehicleName: item.vehicleName,
              };
            });
          }
        })
    );
  }


  /**
   * is pajero or not
   *
   *
   */
  onSelectVehicle(event: Event) {
    this.intiateJourneyService.CheckISPajero(this.journeyForm.vehicleTypeId).subscribe((result: any) => {
      this.journeyForm.ispajero = result.data;
      this.showHide(
        this.journeyForm.isCompanyVehicle,
        this.journeyForm.ispajero
      );
    });

    this.getDrivers();
  }
  /**
   * on select vehicle plate no
   *
   *
   * @param event
   */
  onChangeVehiclePlateNo(event: MatSelectChange) {
    this.selectedVehicles = event.value;
  }

  /**
   * drivers
   *
   *
   * @param id
   */
  driversByType(id: number) {
    this.subscriptions.push(
      this.intiateJourneyService
        .Drivers(id, this.journeyForm.isCompanyVehicle)
        .subscribe((data) => {
          this.drivers = data.data;

          // selected drivers
          if (this.drivers.length > 0) {
            this.drivers = this.drivers.map((item) => {
              return {
                driverId: item.driverId,
                driverName: item.driverName,
              };
            });
          }
        })
    );
  }

  /**
   * compare to select default selected
   *
   *
   * @param c1
   * @param c2
   */
  compareVehiclesFn(c1: Vehicle, c2: Vehicle): boolean {
    return c1 && c2 ? c1.vehicleId === c2.vehicleId : c1 === c2;
  }

  /**
   * compare to select default selected
   *
   *
   * @param c1
   * @param c2
   */
  compareDriversFn(c1: Driver, c2: Driver): boolean {
    return c1 && c2 ? c1.driverId === c2.driverId : c1 === c2;
  }
  /**
   * validate checkpoints
   *
   *
   */
  validateCheckPoints() {
    this.checkPointError = false;
    for (let index = 0; index < this.journeyForm.checkPoints.length; index++) {
      const checkpoint = this.journeyForm.checkPoints[index];
      if (
        !checkpoint.IsMapLocation &&
        checkpoint.zoneId == null &&
        checkpoint.locationId == null &&
        checkpoint.ETA == ""
      ) {
        this.checkPointError = true;
      }
    }
  }

  /**
   * check in point at journey
   *
   *
   * @param checkpointId
   */
  checkInJourney(checkpointId: number) {
    const body = {
      journeyId: this.editJourneyId,
      checkpointId: checkpointId,
    };

    this.intiateJourneyService.checkIn(body).subscribe((res) => {
      if (!res.isSuccessful) {
        Swal.fire("", res.message, "error");
      } else {
        Swal.fire("", res.message, "success");
        this.journeyDetails();
      }
    });
  }

  /**
   * Directions
   *
   *
   * @param event
   */
  onResponse(event) {
    if (this.journeyLoaded && event.status == "ZERO_RESULTS") {
      // code runs here
    }
  }

  /**
   *
   * @param array
   */
  changePoints(array: object[]): void {
    if (array.length > 0) {
      array.forEach((element: any) => {
        if (element.checkpointFromId != 0 && element.checkpointFromId != 0) {
          this.isValidCommunicationsPoints = true;
        } else {
          this.isValidCommunicationsPoints = false;
        }
      });
    }
  }

  /**
   * remove waypoint
   * 
   * 
   * @param index 
   */
  removeCheckpoint(index: number) {
    this.journeyForm.checkPoints.splice(index, 1);
    this.waypoints.splice(index, 1);
    this.waypoints = [...this.waypoints];
  }
}
