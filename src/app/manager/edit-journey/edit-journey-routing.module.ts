import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditJourneyComponent } from './edit-journey.component';

const routes: Routes = [
  {
    path: '',
    component: EditJourneyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditJourneyRoutingModule { }
