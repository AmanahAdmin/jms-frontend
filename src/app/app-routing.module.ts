import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/Helpers/auth.guard';

//Common
import { LoginComponent } from './common/login/login.component';
import { ResetPasswordComponent } from './common/reset-password/reset-password.component';
import { NotificationsComponent } from './common/notifications/notifications.component';
import { HomeComponent } from './home/home.component';

//Managers
import { DriverSelectionComponent } from './manager/driver-selection/driver-selection.component';
import { CurrentJourneysComponent } from './manager/current-journeys/current-journeys.component';
import { JourneyCalendarComponent } from './manager/journey-calendar/journey-calendar.component';
import { JourneyDetailsComponent } from './common/journey-details/journey-details.component';

//Admin
import { AdminDashboardComponent } from './admin/dashboard/admin-dashboard.component';
import { WorkforceManagementComponent } from './admin/workforce/workforce-management.component';
import { TeamManagementComponent } from './admin/team/team-management.component';
import { ManagerManagementComponent } from './admin/manager/manager-management.component';
import { DriverManagementComponent } from './admin/driver/driver-management.component';
import { AssessmentCategoryManagementComponent } from './admin/assessmentCategory/assessment-category-management.component';
import { RoleManagementComponent } from './admin/role/role-management.component';
import { AssessmentManagementComponent } from './admin/assessment/assessment-management.component';
import { ForgetPasswordComponent } from './common/forget-password/forget-password.component';
import { ForgetchangepasswordComponent } from './common/forgetchangepassword/forgetchangepassword.component';
import { JourneyMontroing } from './driver/journey-montoring/journey-montoring.component';
import { JourneyApprovalSettingComponent } from './admin/journey-approval-setting/journey-approval-setting.component';
import { VehicleTypeManagementComponent } from './admin/vehicleType/vehicle-type-management.component';
import { VehicleManagementComponent } from './admin/vehicle/vehicle-management.component';
import { ShiftManagementComponent } from './admin/shift/shift-management.component';
import { PayloadTypeManagementComponent } from './admin/payloadType/payload-type-management.component';
import { PayloadManagementComponent } from './admin/payload/payload-management.component';
import { AnswerTypeManagementComponent } from './admin/answerType/answer-type-management.component';
import { WorkflowManagementComponent } from './admin/workflow/workflow-management.component';

import { SubGroupsPageComponent } from './admin/sub-groups-page/sub-groups-page.component';
import { AssessmentsManagementComponent } from './admin/assessment-withTabs/assessments-management/assessments-management.component';
import { PayloadsComponent } from './admin/payloads/payloads/payloads.component';
import { VehiclesComponent } from './admin/vehicles/vehicles/vehicles.component';
import { PagesPermissionsComponent } from './common/pages-permissions/pages-permissions.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'sub-groups', component: SubGroupsPageComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'forgetpassword', component: ForgetPasswordComponent },
    { path: 'reset-password', component: ResetPasswordComponent },
    { path: 'forgetchangepassword', component: ForgetchangepasswordComponent },
    {
        path: 'initiate-journey', loadChildren: () => import('./manager/initiate-journey/initiate-journey.module').then(m => m.InitiateJourneyModule), canActivate: [AuthGuard]
    },
    {
        path: 'edit-journey/:id', loadChildren: () => import('./manager/edit-journey/edit-journey.module').then(m => m.EditJourneyModule), canActivate: [AuthGuard]
    },
    {
        path: 'reports',
        loadChildren: () => import('@jms/app/common/reports/reports.module').then(m => m.ReportsModule),
        canActivate: [AuthGuard]
    },
    // { path: 'validate-journey/:id', component: InitiateJourneyComponent, canActivate: [AuthGuard] },
    // { path: 'journey/:id', component: InitiateJourneyComponent, canActivate: [AuthGuard] },
    { path: 'journeys', component: CurrentJourneysComponent, canActivate: [AuthGuard] },
    { path: 'journey-calendar', component: JourneyCalendarComponent, canActivate: [AuthGuard] },

    { path: 'driver-selection', component: DriverSelectionComponent, canActivate: [AuthGuard] },
    { path: 'journeymontoring', component: JourneyMontroing },
    { path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard] },
    { path: 'journeydetails', component: JourneyDetailsComponent, canActivate: [AuthGuard] },
    { path: 'request-type', loadChildren: () => import('./manager/request-type/request-type.module').then(m => m.RequestTypeModule), canActivate: [AuthGuard] },
    { path: 'third-party', loadChildren: () => import('./manager/third-party/third-party.module').then(m => m.ThirdPartyModule), canActivate: [AuthGuard] },
    //Admin Routes
    { path: 'admin/dashboard', component: AdminDashboardComponent, canActivate: [AuthGuard] },
    { path: 'admin/driver', component: DriverManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/role', component: RoleManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/manager', component: ManagerManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/assessmentCategory', component: AssessmentCategoryManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/assessment', component: AssessmentManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/teams', component: TeamManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/workforces', component: WorkforceManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/journeyApprovalSetting', component: JourneyApprovalSettingComponent, canActivate: [AuthGuard] },
    { path: 'admin/vehicleType', component: VehicleTypeManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/vehicle', component: VehicleManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/shift', component: ShiftManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/payloadType', component: PayloadTypeManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/payload', component: PayloadManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/answerType', component: AnswerTypeManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/workflow', component: WorkflowManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/pagePermission', component: PagesPermissionsComponent, canActivate: [AuthGuard] },
    { path: 'admin/Assessments', component: AssessmentsManagementComponent, canActivate: [AuthGuard] },
    { path: 'admin/Payloads', component: PayloadsComponent, canActivate: [AuthGuard] },
    { path: 'admin/Vehicles', component: VehiclesComponent, canActivate: [AuthGuard] },
    { path: 'admin/alert', loadChildren: () => import('./admin/alerts/alert.module').then(m => m.AlertModule), },
    { path: 'admin/profile', loadChildren: () => import('./common/users.module').then(m => m.UsersModule), },
    {
        path: 'admin',
        canActivate: [AuthGuard],
        children: [
            {
                path: 'general-settings',
                loadChildren: () => import('@jms/app/admin/general-settings/general-settings.module').then(m => m.GeneralSettingsModule)
            },
            {
                path: 'geofencing',
                loadChildren: () => import('@jms/app/admin/geofencing/geofencing-management.module').then(m => m.GeofencingManagementModule)
            },
            {
                path: 'location',
                loadChildren: () => import('@jms/app/admin/location/location-management.module').then(m => m.LocationManagementModule)
            },
            {
                path: 'language',
                loadChildren: () => import('@jms/app/admin/language/language-management.module').then(m => m.LanguageManagementModule)
            }
        ]
    },
    {
        path: 'journey-list',
        loadChildren: () => import('@jms/app/manager/journey-list/journey-list.module').then(m => m.JourneyListModule),
        canActivate: [AuthGuard]
    },

    // manager routes
    { path: 'manager', loadChildren: () => import('./manager/manager.module').then(m => m.ManagerModule) },
    { path: '**', redirectTo: '' },

];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
