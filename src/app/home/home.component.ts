import { map } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable, timer } from 'rxjs';
import { User } from '../shared/models/UserModel';
import { AuthenticationService } from '../shared/Services/AuthenticationService';
import { JourneyService } from '../shared/Services/JourneyService';
import Swal from 'sweetalert2';
import { JourneyStatus } from '../shared/enums/journey-status.enum';
import { constant } from '../shared/const/constant'
import { Router } from '@angular/router';
import { CommonService } from '../shared/Services/CommonService';
import { Permission } from '../shared/Entities/User/Permission';
import { MenuItem, MenuItems } from '../shared/Models/menuItems';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  currentUser: User;
  data: any;
  currentJourney: any = null;
  featureConst = constant;
  subs: Subscription = new Subscription();
  homePermissions: Permission[] = [];
  subGroupPermissions: Permission[] = []; validateFeatureGroups
  selectGroupAsMain = false;
  menu: MenuItem[];
  constructor(private authenticationService: AuthenticationService, private journeyService: JourneyService, private router: Router
    , private commonService: CommonService,
    private menuItems: MenuItems) {
    this.currentUser = this.authenticationService.currentUserValue;
  }
  ngOnInit() {
    this.getHomeFeatures();
    // var userRole = this.currentUser.role.name;
    // if (userRole == 'Driver') {
    //     this.journeyService.GetCurrentJourney().toPromise().then((data) => {
    //         this.currentJourney = data.data;
    //         console.log(this.currentJourney);
    //     });
    // }
    this.menu = this.menuItems.getMenuitem();
  }

  validateFeatures(permissions: string[]) {
    let isValid = false;
    permissions.forEach(featureName => {
      if (this.authenticationService.validateFeature(featureName)) {
        isValid = true;
      } else if (featureName == '*') {
        isValid = true;
      }
    });

    return isValid;
  }

  getHomeFeatures() {
    this.subs.add(this.commonService.GetMainPagesPermission().pipe(res => res).subscribe(per => {
      per.data.map(permisission => permisission).map(features => {
        this.homePermissions = features.features;
      });
    }));
  }

  getsubGroups(groupId) {
    this.subs.add(this.commonService.GetSubPagesPermission(groupId).pipe(res => res).subscribe(per => {
      this.subGroupPermissions = per.data;
    }));
  }

  getSubGroup(groupId) {
    this.router.navigate(['/sub-groups', { queryParams: { id: 37 } }]);
  }

  ngOnDestroy() { this.subs.unsubscribe(); }

}
