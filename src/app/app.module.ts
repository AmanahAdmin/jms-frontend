import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { AppRoutingModule } from './app-routing.module';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { AgmDrawingModule } from '@agm/drawing'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './shared/Helpers/jwt.interceptor';
import { ErrorInterceptor } from './shared/Helpers/error.interceptor'
import { AuthGuard } from './shared/Helpers/auth.guard';
import { AgGridModule } from '@ag-grid-community/angular';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BlockUIModule } from 'ng-block-ui';
import { BlockUIHttpModule } from 'ng-block-ui/http';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { MatSidenavModule, MatRadioModule, MatSortModule, MatPaginatorModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MatNativeDateModule } from '@angular/material/core';

//Common
import { AppComponent } from './app.component';
import { LoginComponent } from './common/login/login.component';
import { ResetPasswordComponent } from './common/reset-password/reset-password.component';
import { MainHeaderComponent } from './common/main-header/main-header.component';
import { NotificationsComponent } from './common/notifications/notifications.component';
import { HomeComponent } from './home/home.component';

//Managers
import { DriverSelectionComponent } from './manager/driver-selection/driver-selection.component';
import { CurrentJourneysComponent } from './manager/current-journeys/current-journeys.component';
import { JourneyCalendarComponent } from './manager/journey-calendar/journey-calendar.component';
import { JourneyInfoComponent } from './common/journey-info/journey-info.component';
import { JourneyDetailsComponent } from './common/journey-details/journey-details.component';

//Driver
import { JourneyMontroing } from './driver/journey-montoring/journey-montoring.component';

//Admin
import { AdminDashboardComponent } from './admin/dashboard/admin-dashboard.component';
import { ManagerManagementComponent } from './admin/manager/manager-management.component';
import { DriverManagementComponent } from './admin/driver/driver-management.component';
import { AssessmentCategoryManagementComponent } from './admin/assessmentCategory/assessment-category-management.component';
import { RoleManagementComponent } from './admin/role/role-management.component';
import { AssessmentManagementComponent } from './admin/assessment/assessment-management.component';
import { TeamManagementComponent } from './admin/team/team-management.component';
import { WorkforceManagementComponent } from './admin/workforce/workforce-management.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ForgetPasswordComponent } from './common/forget-password/forget-password.component';
import { ForgetchangepasswordComponent } from './common/forgetchangepassword/forgetchangepassword.component';
import { CompleteJourneyInfoComponent } from './manager/complete-journey-info/complete-journey-info.component';
import { JourneyApprovalSettingComponent } from './admin/journey-approval-setting/journey-approval-setting.component';

//angular materail
import {
  MatTableModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatAutocompleteModule,
  MatTooltipModule,

} from '@angular/material';
import { UploadComponent } from './upload/upload.component';
import { VehicleTypeManagementComponent } from './admin/vehicleType/vehicle-type-management.component';
import { VehicleManagementComponent } from './admin/vehicle/vehicle-management.component';
import { ShiftManagementComponent } from './admin/shift/shift-management.component';
import { PayloadManagementComponent } from './admin/payload/payload-management.component';
import { PayloadTypeManagementComponent } from './admin/payloadType/payload-type-management.component';
import { AnswerTypeManagementComponent } from './admin/answerType/answer-type-management.component';
import { WorkflowManagementComponent } from './admin/workflow/workflow-management.component';
import { SubGroupsPageComponent } from './admin/sub-groups-page/sub-groups-page.component';
import { AssessmentsManagementComponent } from './admin/assessment-withTabs/assessments-management/assessments-management.component';

import { PayloadsComponent } from './admin/payloads/payloads/payloads.component';
import { VehiclesComponent } from './admin/vehicles/vehicles/vehicles.component';

import { UsersModule } from './common/users.module';
import { PagesPermissionsComponent } from './common/pages-permissions/pages-permissions.component';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CoreModule } from './core/core.module';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { RealtimeNotificationModule } from './realtime-notification/realtime-notification.module';
import { DateToTimezoneModule } from './pipes/date-to-timezone/date-to-timezone.module';
import { AssessmentLocalizationQuestionsComponent } from './admin/assessment/assessment-localization-questions/assessment-localization-questions.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ResetPasswordComponent,
    DriverSelectionComponent,
    JourneyMontroing,
    AdminDashboardComponent,
    ManagerManagementComponent,
    DriverManagementComponent,
    AssessmentCategoryManagementComponent,
    RoleManagementComponent,
    TeamManagementComponent,
    WorkforceManagementComponent,
    AssessmentManagementComponent,
    JourneyApprovalSettingComponent,
    VehicleTypeManagementComponent,
    VehicleManagementComponent,
    HomeComponent,
    ShiftManagementComponent,
    PayloadManagementComponent,
    PayloadTypeManagementComponent,
    AnswerTypeManagementComponent,
    WorkflowManagementComponent,
    NotificationsComponent,
    JourneyInfoComponent,
    JourneyDetailsComponent,
    MainHeaderComponent,
    CurrentJourneysComponent,
    JourneyCalendarComponent,
    ForgetPasswordComponent,
    ForgetchangepasswordComponent,
    CompleteJourneyInfoComponent,
    UploadComponent,
    SubGroupsPageComponent,
    AssessmentsManagementComponent,
    PayloadsComponent,
    VehiclesComponent,
    PagesPermissionsComponent,
    AssessmentLocalizationQuestionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([]),
    BlockUIModule.forRoot(),
    BlockUIHttpModule.forRoot(),
    SweetAlert2Module.forRoot(),
    DragDropModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatRadioModule,
    NgMultiSelectDropDownModule.forRoot(),
    AgmDirectionModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyD1mqYsV0ShwvvIaKU3MOr9CJelaCdAb7I",
      libraries: ["places", "drawing", "geometry"],
      language: "ar",
      region: "EG",
    }),
    AgmDrawingModule,
    GooglePlaceModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.cubeGrid,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '4px',
      primaryColour: '#F5A622',
      secondaryColour: '#F5A622',
      tertiaryColour: '#F5A622'
    }),
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSlideToggleModule,
    NgbModule,
    TabsModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCheckboxModule,
    // DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    UsersModule,
    MatAutocompleteModule,
    MatTooltipModule,
    CoreModule,
    RealtimeNotificationModule,
    DateToTimezoneModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: BsDatepickerConfig, useFactory: getDatepickerConfig },
    AuthGuard,
    GoogleMapsAPIWrapper,
    //MessagingService
  ],
  bootstrap: [AppComponent],
  exports: [MatSortModule, MatPaginatorModule]
})
export class AppModule { }


export function getDatepickerConfig(): BsDatepickerConfig {
  return Object.assign(new BsDatepickerConfig(), {
    containerClass: "theme-dark-blue"
  });
}
