import { Component, ViewChild, OnInit, AfterViewInit } from "@angular/core";
import {
  MatDialog,
  MatTable,
  MatSort,
  MatTableDataSource,
  MatPaginator,
} from "@angular/material";
import Swal from "sweetalert2";
import * as $ from "jquery";
import { AssessmentCategory } from "@jms/app/shared/Entities/admin/AssessmentCategory";
import { AssessmentCategoryService } from "@jms/app/shared/Services/AssessmentCategoryService";
import { AuthenticationService } from "@jms/app/shared/Services/AuthenticationService";
import { constant } from "@jms/app/shared/const/constant";
import { SwalService } from "@jms/app/shared/Services";

@Component({
  selector: "app-assessment-category-management",
  templateUrl: "./assessment-category-management.component.html",
  styleUrls: ["./assessment-category-management.component.css"],
})
export class AssessmentCategoryManagementComponent implements OnInit {
  displayedColumns: string[] = [
    "name",
    "isAllowAutoRiskManagement",
    "isActive",
    "action",
  ];
  featureConst = constant;
  isSubmitted = false;
  assessmentCategory: AssessmentCategory = this.initObject();
  public dataSource = new MatTableDataSource<any>();
  constructor(
    public dialog: MatDialog,
    private authService: AuthenticationService,
    private assessmentCategoryService: AssessmentCategoryService,
    private swalService: SwalService
  ) {}
  ngOnInit() {
    this.initList();
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };
  public validateFeature(featurename: string) {
    return this.authService.validateFeature(featurename);
  }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  initModal() {
    this.assessmentCategory = this.initObject();
  }
  Update(row_obj: any) {
    this.isSubmitted = false;
    this.assessmentCategory = row_obj;
  }

  onSubmit() {
    if (this.validateObject()) {
      if (this.assessmentCategory.id > 0) this.UpdateRowData();
      else this.AddRowData();
    }
  }
  AddRowData() {
    this.assessmentCategoryService
      .Add(this.assessmentCategory)
      .then((result: any) => {
        Swal.fire(
          "Add AssessmentCategory",
          `Your assessmentCategory ${this.assessmentCategory.name} added successfully`,
          "success"
        );
        this.initList();
        this.closeModal();
      });
  }
  UpdateRowData() {
    this.assessmentCategoryService
      .Update(this.assessmentCategory)
      .then((data) => {
        Swal.fire(
          "Update AssessmentCategory",
          `Your assessmentCategory ${this.assessmentCategory.name} updated successfully`,
          "success"
        );
        this.initList();
        this.closeModal();
      });
  }
  ActivateOrDeactivate(event: Event, row_obj: any) {
    event.preventDefault();
    event.stopPropagation();
    const message = !row_obj.isActive ? "Activate" : "Deactivate";
    const messageRes = !row_obj.isActive ? "unblocked" : "blocked";
    this.swalService
      .showConfirmMessage(message, `assessment category  ${row_obj.name}`)
      .then((result) => {
        if (result.value) {
          this.assessmentCategoryService
            .ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive)
            .then((res: any) => {
              if (res.isSuccessful) {
                Swal.fire({
                  icon: "success",
                  title: `${message}d successfully`,
                  text: `Assessment Category ${message}d Successfully`,
                });
                this.initList();
              } else {
                Swal.fire({
                  icon: "info",
                  title: `Sorry can't ${message}`,
                  text: res.message,
                });
              }
            })
            .catch((err) => {
              Swal.fire({
                icon: "info",
                title: `Sorry can't ${message}`,
                text: err,
              });
            });
        }
      });
  }
  Delete(row_obj: any) {
    Swal.fire({
      title: "Delete Assessment Category ",
      text: `Are you sure to delete this ${row_obj.name} assessment Category?`,
      showCancelButton: true,
      confirmButtonColor: "#17BC9B",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then((result) => {
      if (result.value) {
        this.assessmentCategoryService
          .Delete(row_obj.rowId)
          .then((deleteResult: any) => {
            if (!deleteResult.isSuccessful) {
              Swal.fire({
                icon: "info",
                title: `Sorry can't delete`,
                text: deleteResult.message,
              });
            } else {
              Swal.fire({
                icon: "success",
                title: `Assessment Category`,
                text: deleteResult.message,
              });
              this.initList();
            }
          });
      }
    });
  }
  initList() {
    this.assessmentCategoryService.GetAll().then((data: any) => {
      this.dataSource.data = data.data;
      this.table.renderRows();
    });
  }

  initObject() {
    this.isSubmitted = false;
    var newObject: AssessmentCategory = {
      id: 0,
      rowId: "",
      name: "",
      isActive: true,
      isAllowAutoRiskManagement: false,
    };
    return newObject;
  }
  validateObject() {
    this.isSubmitted = true;
    if (this.assessmentCategory.name.trim() == "") return false;
    else return true;
  }
  closeModal() {
    $(".close").click();
    this.assessmentCategory = this.initObject();
  }
}
