import { Component, OnInit, Input, SimpleChanges, HostBinding } from '@angular/core';
import { User } from '@jms/app/shared/models/UserModel';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';

@Component({
  selector: 'app-payloads',
  templateUrl: './payloads.component.html',
  styleUrls: ['./payloads.component.css']
})
export class PayloadsComponent implements OnInit {
  isVisibleThird = false;
  currentUser: User;

  constructor(private authenticationService: AuthenticationService) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
  }

}
