import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Language } from '../../shared/Entities/admin/Language';
import { LanguageService } from '../../shared/Services/LanguageService';
import { AuthenticationService } from '../../shared/Services/AuthenticationService';
import { constant } from '../../shared/const/constant';
import { LanguageDirection } from '../../shared/enums/language-direction-enum';
import { environment } from '../../../environments/environment';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-language-management',
    templateUrl: './language-management.component.html',
    styleUrls: ['./language-management.component.css']
})
export class LanguageManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'isActive', 'action'];
    featureConst = constant;
    LanguageDirections: LanguageDirection;
    keys = [LanguageDirection.ltr, LanguageDirection.rtl];
    environmentUrl: string = environment.JMSApiURL;
    language: Language = this.initObject();;
    isSubmitted = false;
    isAlreadyExsit = false;
    public dataSource = new MatTableDataSource<any>();
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private languageService: LanguageService,
        private swalService: SwalService
    ) { }
    ngOnInit() {
        this.initList();
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.language = this.initObject(); }
    Update(row_obj: any) { this.isSubmitted = false; this.language = row_obj; }

    public response: { dbPath: '' };
    public uploadFinished = (event: any) => {
        this.response = event;
        this.language.image = this.response.dbPath;
    }
    public createImgPath = (serverPath: string) => {
        return `${environment.JMSURL}/${serverPath}`;
    }

    onSubmit() {
        if (this.validateObject()) {
            if (this.language.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.languageService.Add(this.language).then((result: any) => {
            if (result.isSuccessful) {
                Swal.fire('Add Language', `Your language ${this.language.name} added successfully`, 'success');
                this.initList();
                this.closeModal();
            }
            else {
                Swal.fire(`Can't Add Language`, `${result.message}`, 'error');
            }
        });
    }
    UpdateRowData() {
        this.languageService.Update(this.language).then((data) => {
            Swal.fire('Update Language', `Your language ${this.language.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }

    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `language ${row_obj.name}`).then(result => {
            if (result.value) {
                this.languageService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then(() => { this.initList(); });
            }
        })
    }


    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Language ',
            text: `Are you sure to delete this ${row_obj.name} language?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.languageService.Delete(row_obj.rowId).then(() => {
                    this.dataSource.data = this.dataSource.data.filter((value, key) => {
                        return value.id != row_obj.id;
                    });
                    window.location.reload();
                });


            }
        });
    }
    initList() {
        this.languageService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });

    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Language = { id: 0, rowId: '', name: '', isDefault: false, isActive: true, image: '', isForDriver: false, isForManager: false, direction: 0 };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.language.name == '' || this.language.name.trim() == '' || this.language.direction == null) return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.language = this.initObject();
    }
}
