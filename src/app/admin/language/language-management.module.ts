import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguageManagementRoutingModule } from './language-management-routing.module';
import { LanguageManagementComponent } from './language-management.component';
import { MatFormFieldModule, MatTableModule, MatSlideToggleModule, MatPaginatorModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LanguageManagementComponent
  ],
  imports: [
    CommonModule,
    LanguageManagementRoutingModule,
    MatFormFieldModule,
    MatTableModule,
    MatSlideToggleModule,
    FormsModule,
    MatPaginatorModule,
    MatInputModule
  ]
})
export class LanguageManagementModule { }
