import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguageManagementComponent } from './language-management.component';

const routes: Routes = [
  {
    path: '',
    component: LanguageManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguageManagementRoutingModule { }
