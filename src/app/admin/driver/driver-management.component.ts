import { Component, ViewChild, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { DriverService } from '@jms/app/shared/Services/DriverService';
import Swal from 'sweetalert2';
import { Driver } from '@jms/app/shared/Entities/User/Driver';
import { DriveFor } from '@jms/app/shared/enums/drive-for.enum';
import * as $ from 'jquery'
import { constant } from '@jms/app/shared/const/constant';
import { environment } from '../../../environments/environment';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { VehicleTypeService } from '@jms/app/shared/Services/VehicleTypeService';
import { Subscription } from 'rxjs';
import { subscribeOn, map } from 'rxjs/operators';
import { UserService } from '@jms/app/shared/Services/UserService';
import { SwalService } from '@jms/app/shared/Services';
import { ThirdPartyService } from '@jms/app/shared/Services/third-party.service';

@Component({
    selector: 'app-driver-management',
    templateUrl: './driver-management.component.html',
    styleUrls: ['./driver-management.component.css']
})
export class DriverManagementComponent implements OnInit, OnDestroy {
    displayedColumns: string[] = ['fullName', 'username', 'userDepartmentsNames', 'isActive', 'action'];
    isSubmitted = false;
    featureConst = constant;
    dropdownSettings: IDropdownSettings;
    driver: Driver = this.initObject();
    Teams = [];
    vechilesTypes: any[] = [];
    WorkForces = [];
    Managers: any[] = [];
    thirdParties: any[] = [];
    ManagersList: any[] = [];
    DriveFors = DriveFor;
    keys = [DriveFor.Light, DriveFor.Heavy];
    subs: Subscription = new Subscription();
    public dataSource = new MatTableDataSource<any>();
    constructor(public dialog: MatDialog, private authService: AuthenticationService, private driverService: DriverService
        , private vehicleTypeService: VehicleTypeService, private userService: UserService,
        private swalService: SwalService,
        private thirdPartyService: ThirdPartyService
    ) { }

    ngOnInit() {
        this.initMultiDropList();
        this.initList();
        this.authService.GetAllTeam().then((data: any) => { this.Teams = data.data; });
        this.authService.GetAllWorkFoce().then((data: any) => { this.WorkForces = data.data; });
        // this.subs.add(this.userService.getMangersOutCompany().pipe(map(res => res)).subscribe(managers => {

        //     this.ManagersList = this.Managers = managers.data;
        //     console.log(this.Managers)
        // }));
        this.vehicleTypeService.GetAll().then(v => { this.vechilesTypes = v.data; });
        this.getThirdParty();
    }

    /**
* get all third Party
*
*/
    getThirdParty() {
        this.thirdPartyService.GetAll('')
            .subscribe((result: any) => {
                this.thirdParties = result.data;
                this.thirdParties = this.thirdParties.filter(a => a.isActive == true);
                this.Managers = this.thirdParties;
            })
    }

    onSlotChange(event) {
        // console.log(event)
        // var isChecked = event.checked;

        // if (!isChecked) {
        //     this.Managers = this.thirdParties;
        // } else {
        //     this.Managers = this.ManagersList;
        // }

    }

    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    validateEmail() {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(this.driver.email);
    }
    validatePhone() {
        var re = /^(^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$)$/;
        return re.test(this.driver.phoneNumber);
    }
    initModal() { this.driver = this.initObject(); }
    Update(row_obj: any) {
        this.isSubmitted = false;
        this.driver = row_obj;
        this.driver.teams = this.driver.userTeams != null ? this.Teams.filter(x => this.driver.userTeams.map(y => y).includes(x.id)) : [];
        this.driver.uiDriverVehicleTypes = this.driver.driverVehicleTypes != null ? this.vechilesTypes.filter(x => this.driver.driverVehicleTypes.map(y => y).includes(x.id)) : [];
        this.driver.uiEmployeeFor = this.driver.employeeFor != null ? this.Managers.filter(x => this.driver.employeeFor.map(y => y).includes(x.id)) : [];
    }

    public response: { dbPath: '' };
    public uploadFinished = (event: any) => {
        this.response = event;
        this.driver.image = this.response.dbPath;
    }
    public createImgPath = (serverPath: string) => { return `${environment.JMSURL}/${serverPath}`; }

    onSubmit() {
        this.driver.userTeams = this.driver.teams != null ? this.driver.teams.map(function (e) { return e.id }) : [];
        this.driver.driverVehicleTypes = this.driver.uiDriverVehicleTypes != null ? this.driver.uiDriverVehicleTypes.map(function (e) { return e.id }) : [];
        if (this.driver.isCompanyEmployee) this.driver.employeeFor = [];
        else this.driver.employeeFor = this.driver.uiEmployeeFor != null ? this.driver.uiEmployeeFor.map(function (e) { return e.id }) : [];

        if (this.validateObject()) {
            if (this.driver.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.driverService.Add(this.driver).then((data: any) => {
            if (data.isSuccessful) {
                Swal.fire('Add Driver', `Your Driver ${this.driver.fullName} added successfully`, 'success');
                this.initList();
                this.closeModal();
            }
            else {
                Swal.fire(`Can't Add Driver`, `${data.message}`, 'error');
            }
        }).catch((err) => {
            Swal.fire(`Can't Add Driver`, `${err}`, 'error');
        });
    }
    UpdateRowData() {
        this.driverService.Update(this.driver).then((data) => {
            Swal.fire('Update Driver', `Your Driver ${this.driver.fullName} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Driver',
            text: `Are you sure to delete this ${row_obj.fullName} driver?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.driverService.Delete(row_obj.rowId).then((deleteResult: any) => {
                    if (deleteResult) {
                        if (deleteResult.isSuccessful === false) {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't delete`,
                                text: deleteResult.message
                            })
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'Driver has been deleted successfully'
                            })
                        }
                    }

                    this.initList();

                });
            }
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `driver ${row_obj.fullName}`).then(result => {
            if (result.value) {
                this.driverService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res: any) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `Driver  ${message}d Successfully`
                            })
                            this.initList();
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: `Sorry can't ${message}`,
                                text: `${res.message}`
                            })
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        })
                    });
            }
        })
    }
    initList() {
        this.subs.add(this.driverService.GetAll().pipe(map(res => res)).subscribe((data) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        }));
    }

    initObject() {
        this.isSubmitted = false;
        return new Driver();
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.driver.fullName.trim() == '' || this.driver.username.trim() == '' ||
            this.driver.email && !this.validateEmail() ||
            this.driver.userTeams == null ||
            this.driver.licenseNo == '' || this.driver.licenseExpiryDate == null ||
            (this.driver.id <= 0 && this.driver.password == '' || this.driver.confrimPassword == '' || this.driver.password != this.driver.confrimPassword) ||
            (this.driver.id > 0 && this.driver.password != '' && this.driver.password != this.driver.confrimPassword) ||
            this.driver.isCompanyEmployee && !this.driver.employeeId ||
            this.driver.driverVehicleTypes.length === 0 ||
            !this.validatePhone()) return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.driver = this.initObject();
    }

    initMultiDropList() {
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            clearSearchFilter: true,
            closeDropDownOnSelection: true,
        };
    }
    removeImage() {
        this.driver.image = null;
    }
    ngOnDestroy() { this.subs.unsubscribe(); }

}
