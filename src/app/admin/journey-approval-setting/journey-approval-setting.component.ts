import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { JourneyApprovalSetting, JourneyApprovalNotifyRole } from '@jms/app/shared/Entities/Journey/journey-approval-setting';
import { JourneyApprovalSettingService } from '@jms/app/shared/Services/JourneyApprovalSettingService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
    selector: 'app-journey-approval-setting',
    templateUrl: './journey-approval-setting.component.html',
    styleUrls: ['./journey-approval-setting.component.css']
})
export class JourneyApprovalSettingComponent implements OnInit {
    displayedColumns: string[] = ['riskType', 'drivingType', 'approvalRole', 'notifyRole', 'action'];
    featureConst = constant;
    journeyApprovalSettings: JourneyApprovalSetting[] = [];
    dropdownSettings: IDropdownSettings;
    Roles = [];
    dropdownList = [];
    selectedItems = [];
    public dataSource = new MatTableDataSource<any>();
    constructor(public dialog: MatDialog, private authService: AuthenticationService, private journeyApprovalSettingService: JourneyApprovalSettingService) { }
    ngOnInit() {
        this.initMultiDropList();
        this.authService.GetLookupRoles().then((data: any) => { this.Roles = data.data; this.initList(); });
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    Add() {
        if (this.journeyApprovalSettings == null) this.journeyApprovalSettings = [];
        this.journeyApprovalSettings.push(this.initObject());
        this.dataSource.data = this.journeyApprovalSettings;
        this.table.renderRows();
    }
    Update(row_obj: any) { row_obj.isSubmitted = false; row_obj.isProcessing = true; }

    onSubmit(element: any) {
        if (this.validateObject(element)) {
            if (element.id > 0) this.UpdateRowData(element);
            else this.AddRowData(element);
        }
    }
    AddRowData(journeyApprovalSetting: any) {
        this.journeyApprovalSettingService.Add(journeyApprovalSetting).then((result: any) => {
            Swal.fire('Add Journey Approval Setting', `Your journey approval setting added successfully`, 'success');
            this.initList();
        }).catch((err) => {
            Swal.fire(`Can't Add Journey Approval Setting`, err, 'error');
        })
    }
    UpdateRowData(journeyApprovalSetting: any) {
        this.journeyApprovalSettingService.Update(journeyApprovalSetting).then((data) => {
            Swal.fire('Update Journey Approval Setting', `Your journey approval setting updated successfully`, 'success');
            this.initList();
        }).catch((err) => {
            Swal.fire(`Can't Update Journey Approval Setting`, err, 'error');
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Journey Approval Setting ',
            text: `Are you sure to delete this journey approval setting?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.journeyApprovalSettingService.Delete(row_obj.rowId).then(() => {
                    Swal.fire('Delete Journey Approval Setting', `Your journey approval setting deleted successfully`, 'success');
                    this.initList();
                }).catch((err) => {
                    Swal.fire(`Can't Delete Journey Approval Setting`, err, 'error');
                })
            }
        });
    }
    initList() {
        this.journeyApprovalSettingService.GetAll().then((data: any) => {
            this.journeyApprovalSettings = data.data;
            for (var index = 0; index < this.journeyApprovalSettings.length; index++) {
                this.journeyApprovalSettings[index].isSubmitted = false;
                this.journeyApprovalSettings[index].approvalRoles = this.Roles.filter(x => this.journeyApprovalSettings[index].journeyApprovalRoles.map(y => y.roleId).includes(x.id));
                this.journeyApprovalSettings[index].notifyRoles = this.Roles.filter(x => this.journeyApprovalSettings[index].journeyNotifyRoles.map(y => y.roleId).includes(x.id));
            }
            this.dataSource.data = this.journeyApprovalSettings;
            this.table.renderRows();
        });
    }

    initObject() {
        var newObject: JourneyApprovalSetting = { id: 0, rowId: '', riskType: null, drivingType: null, isProcessing: true, journeyApprovalRoles: [], approvalRoles: [], journeyNotifyRoles: [], notifyRoles: [], isSubmitted: false };
        return newObject;
    }
    validateObject(journeyApprovalSetting: JourneyApprovalSetting) {
        journeyApprovalSetting.isSubmitted = true;
        var duplicatedRiskTypeAndDrivingType = this.journeyApprovalSettings.filter(function (e) { if (e.riskType == journeyApprovalSetting.riskType && e.drivingType == journeyApprovalSetting.drivingType && e.id !== journeyApprovalSetting.id) return e; })
        if (journeyApprovalSetting.riskType == null || journeyApprovalSetting.drivingType == null || journeyApprovalSetting.journeyApprovalRoles.length <= 0 || journeyApprovalSetting.journeyNotifyRoles.length <= 0 || duplicatedRiskTypeAndDrivingType.length > 0) {
            if (duplicatedRiskTypeAndDrivingType.length > 0)
                Swal.fire('Duplicated Journey Approval Setting', `Your 'Risk Type' and  'Driving Type' already inserted before`, 'error');
            return false;
        }
        else return true;
    }

    initMultiDropList() {
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            clearSearchFilter: true,
            closeDropDownOnSelection: true,
        };
    }
    onItemDeSelect(item: any, element: any, type: any) {
        switch (type) {
            case 'approval': {
                element.journeyApprovalRoles = element.journeyApprovalRoles.filter(function (journeyApprovalRole) {
                    return journeyApprovalRole.roleId != item.id;
                });
            } break;
            case 'notify': {
                element.journeyNotifyRoles = element.journeyNotifyRoles.filter(function (journeyNotifyRole) {
                    return journeyNotifyRole.roleId != item.id;
                });
            } break;
        }
    }
    onItemDeSelectAll(items: any, element: any, type: any) {
        switch (type) {
            case 'approval': element.journeyApprovalRoles = []; break;
            case 'notify': element.journeyNotifyRoles = []; break;
        }
        console.log(element);
    }
    onItemSelect(item: any, element: any, type: any) {
        switch (type) {
            case 'approval': {
                let maxOrder = element.journeyApprovalRoles.length > 0 ? Math.max.apply(Math, element.journeyApprovalRoles.map(function (o) { return o.order; })) : 0;
                element.journeyApprovalRoles.push({ roleId: item.id, order: maxOrder + 1 });
            } break;
            case 'notify': {
                let maxOrder = element.journeyNotifyRoles.length > 0 ? Math.max.apply(Math, element.journeyNotifyRoles.map(function (o) { return o.order; })) : 0;
                element.journeyNotifyRoles.push({ roleId: item.id, order: maxOrder + 1 });
            } break;
        }
    }
    onSelectAll(items: any, element: any, type: any) {
        switch (type) {
            case 'approval': {
                element.journeyApprovalRoles = [];
                for (var index = 0; index < items.length; index++) {
                    element.journeyApprovalRoles.push({ roleId: items[index].id, order: index + 1 });
                }
            } break;
            case 'notify': {
                element.journeyNotifyRoles = [];
                for (index = 0; index < items.length; index++) {
                    element.journeyNotifyRoles.push({ roleId: items[index].id, order: index + 1 });
                }
            } break;
        }
    }

    cancel(element) {

    }


}