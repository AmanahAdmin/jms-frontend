import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { PayloadType } from '@jms/app/shared/Entities/admin/PayloadType';
import { PayloadTypeService } from '@jms/app/shared/Services/PayloadTypeService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-payload-type-management',
    templateUrl: './payload-type-management.component.html',
    styleUrls: ['./payload-type-management.component.css']
})
export class PayloadTypeManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'isActive', 'isPeople', 'isHazardous', 'action'];
    featureConst = constant;
    isSubmitted = false;
    payloadType: PayloadType = this.initObject();
    public dataSource = new MatTableDataSource<any>();
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private payloadTypeService: PayloadTypeService,
        private swalService: SwalService
    ) { }
    ngOnInit() { this.initList(); }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.payloadType = this.initObject(); }
    Update(row_obj: any) { this.isSubmitted = false; this.payloadType = row_obj; }

    onSubmit() {
        if (this.validateObject()) {
            if (this.payloadType.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.payloadTypeService.Add(this.payloadType).then((result: any) => {
            Swal.fire('Add Payload Type', `Your payload type ${this.payloadType.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        this.payloadTypeService.Update(this.payloadType).then((data) => {
            Swal.fire('Update Payload Type', `Your payload type ${this.payloadType.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `payload type ${row_obj.name}`).then(result => {
            if (result.value) {
                this.payloadTypeService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `payload type ${message}d Successfully`
                            })
                            this.initList();
                        } else {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't ${message}`,
                                text: res.message
                            });
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        });
                    });
            }
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Payload Type ',
            text: `Are you sure to delete this ${row_obj.name} payload type?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.payloadTypeService.Delete(row_obj.rowId).then((res) => {
                    if (res.isSuccessful) {
                        Swal.fire({
                            icon: 'info',
                            text: 'Payload Type has been deleted successfully'
                        });
                        this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
                    } else {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't delete`,
                            text: res.message
                        })
                    }
                });
            }
        }).catch((err) => {
            Swal.fire({
                icon: 'info',
                title: `Sorry can't delete`,
                text: err
            })
        });
    }
    initList() {
        this.payloadTypeService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: PayloadType = { id: 0, rowId: '', name: '', isActive: true, isHazardous: false, isPeople: false };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.payloadType.name.trim() == '') return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.payloadType = this.initObject();
    }
}