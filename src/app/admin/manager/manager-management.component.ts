import { Component, ViewChild, OnInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { ManagerService } from '@jms/app/shared/Services/ManagerService';
import Swal from 'sweetalert2';
import { Manager } from '@jms/app/shared/Entities/User/Manager';
import * as $ from 'jquery'
import { constant } from '@jms/app/shared/const/constant';
import { environment } from '../../../environments/environment';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-manager-management',
    templateUrl: './manager-management.component.html',
    styleUrls: ['./manager-management.component.css']
})
export class ManagerManagementComponent implements OnInit {
    displayedColumns: string[] = ['fullName', 'email', 'rolesNames', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted = false;
    Teams = [];
    WorkForces = [];
    Roles = [];
    dataSource = new MatTableDataSource<any>();
    dropdownSettings: IDropdownSettings;
    manager: Manager = this.initObject();

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    /**
     * 
     * @param dialog 
     * @param authService 
     * @param managerService 
     * @param swalService 
     */
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private managerService: ManagerService,
        private swalService: SwalService
    ) { }

    ngOnInit() {
        this.initMultiDropList();
        this.initList();
        this.authService.GetAllTeam().then((data: any) => { this.Teams = data.data; });
        this.authService.GetAllWorkFoce().then((data: any) => { this.WorkForces = data.data; });
        this.authService.GetLookupRoles().then((data: any) => { this.Roles = data.data; });
    }

    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    /**
     * filter table
     * 
     * 
     * @param value 
     */
    doFilter(value: string) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }

    /**
     * 
     * @param featurename 
     */
    validateFeature(featurename: string) {
        return this.authService.validateFeature(featurename);
    }

    /**
     * email 
     * 
     * 
     */
    validateEmail() {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(this.manager.email);
    }

    /**
     * phone
     * 
     * 
     */
    validatePhone() {
        var re = /^(^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$)$/;
        return re.test(this.manager.phoneNumber);
    }

    /**
     * init model
     * 
     * 
     */
    initModal() {
        this.manager = this.initObject();
    }

    /**
     * 
     * @param row_obj 
     */
    Update(row_obj: any) {
        this.isSubmitted = false;
        this.manager = row_obj;
        this.manager.roles = this.manager.userRoles != null ? this.Roles.filter(x => this.manager.userRoles.map(y => y).includes(x.id)) : [];
        this.manager.teams = this.manager.userTeams != null ? this.Teams.filter(x => this.manager.userTeams.map(y => y).includes(x.id)) : [];
    }

    response: { dbPath: '' };
    uploadFinished = (event: any) => {
        this.response = event;
        this.manager.image = this.response.dbPath;
    }

    createImgPath = (serverPath: string) => {
        return `${environment.JMSURL}/${serverPath}`;
    }

    onSubmit() {
        this.manager.userTeams = this.manager.teams != null ? this.manager.teams.map(function (e) { return e.id }) : [];
        this.manager.userRoles = this.manager.roles != null ? this.manager.roles.map(function (e) { return e.id }) : [];
        console.log(this.manager);
        if (this.validateObject()) {
            if (this.manager.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }

    AddRowData() {
        this.managerService.Add(this.manager).subscribe(
            (data: any) => {
                if (data.isSuccessful) {
                    Swal.fire('Add Manager', `Your Manager ${this.manager.fullName} added successfully`, 'success');
                    this.initList();
                    this.closeModal();
                } else {
                    Swal.fire(`Can't Add Manager`, `${data.message}`, 'error');
                }

            }, err => {
                Swal.fire(`Can't Add Manager`, `${err}`, 'error');
            }
        );
    }

    UpdateRowData() {
        this.managerService.Update(this.manager).subscribe((data) => {
            Swal.fire('Update Manager', `Your Manager ${this.manager.fullName} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        }, err => {
            Swal.fire(`Can't Update Manager`, `${err}`, 'error');
        });
    }

    /**
     * delete manager
     * 
     * 
     * @param row 
     */
    delete(row: any) {
        Swal.fire({
            title: 'Delete Manager',
            text: `Are you sure to delete this ${row.fullName} manager?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.managerService.Delete(row.rowId).then((deleteResult: any) => {

                    if (deleteResult) {
                        if (deleteResult.isSuccessful === false) {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't delete`,
                                text: deleteResult.message
                            })
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'Manager has been deleted successfully'
                            });
                        }
                    }

                    this.initList();
                });
            }
        });
    }

    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();

        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `manager ${row_obj.fullName}`).then(result => {
            if (result.value) {
                this.managerService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res: any) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `Manager  ${message}d Successfully`
                            })
                            this.initList();
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: `Sorry can't ${message}`,
                                text: `${res.message}`
                            })
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        })
                    });
            }
        })
    }

    initList() {
        this.managerService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Manager = { id: 0, rowId: '', comments: '', confrimPassword: '', email: '', emergencyContact: '', fullName: '', image: '', isActive: true, password: '', phoneNumber: '', username: '', userTeams: null, workForceId: null, roles: null, position: '', accessNumberOnTheSystem: 0, userStatus: 0, browsingLanguageId: null };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.manager.username.trim() == '' || this.manager.fullName.trim() == '' ||
            this.manager.email.trim() == '' || !this.validateEmail() ||
            this.manager.userTeams == null || this.manager.userRoles == null ||
            (this.manager.id <= 0 && this.manager.password == '' || this.manager.confrimPassword == '' || this.manager.password != this.manager.confrimPassword) ||
            (this.manager.id > 0 && this.manager.password != '' && this.manager.password != this.manager.confrimPassword) ||
            !this.validatePhone()) return false;
        return true;
    }
    closeModal() {
        $('.close').click();
        this.manager = this.initObject();
    }
    initMultiDropList() {
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            clearSearchFilter: true,
            closeDropDownOnSelection: true,
        };
    }
    removeImage() {
        this.manager.image = null;
    }
}
