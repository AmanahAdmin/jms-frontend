import { element } from 'protractor';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentRef, ComponentFactoryResolver } from '@angular/core';
import { User } from '@jms/app/shared/models/UserModel';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {
  DistributorTitle: string;
  activeTab: number = 0
  tabIndex = {
    "VehiclTypes": 0,
    "Vehicls": 1,

  }
  private componentRef: ComponentRef<any>;
  constructor(private authenticationService: AuthenticationService, private router: Router,
    private route: ActivatedRoute,) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }
  ngOnInit() {
  }

}

