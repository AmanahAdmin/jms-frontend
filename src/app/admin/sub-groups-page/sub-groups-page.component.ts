
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable, timer } from 'rxjs';
import { AuthenticationService } from '@jms/app//shared/Services/AuthenticationService';
import { constant } from '@jms/app//shared/const/constant'
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { CommonService } from '@jms/app//shared/Services/CommonService';
import { Permission } from '@jms/app//shared/Entities/User/Permission';
import { User } from '@jms/app/shared/models/UserModel';
import { MenuItem, MenuItems } from '@jms/app/shared/Models/menuItems';

@Component({
  selector: 'app-sub-groups-page',
  templateUrl: './sub-groups-page.component.html',
  styleUrls: ['./sub-groups-page.component.css']
})
export class SubGroupsPageComponent implements OnInit, OnDestroy {
  groupId: number;
  currentUser: User;
  subs: Subscription = new Subscription();
  currentJourney: any = null;
  featureConst = constant;
  subGroupName: string;
  subGroupPermissions: Permission[] = [];
  menu: MenuItem[];

  public hrefVechiles: string = "/sub-groups";

  constructor(private authenticationService: AuthenticationService,
    private menuItems: MenuItems,
    private router: Router, private route: ActivatedRoute,
    private commonService: CommonService) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.groupId = params['id'];
        this.subGroupName = params['Name'];
        this.subGroupName = this.subGroupName.replace(/-/g, ' ');
      });
    this.getsubGroups(this.groupId);
    this.menu = this.menuItems.getMenuitem();

  }

  public validateFeature(featurename: string, groupId) {
    let isvalid = false;
    if (this.subGroupPermissions.length > 0) {
      this.subGroupPermissions.map((feature: any, i) => {
        if (feature.mainGroupId == groupId && feature.value == featurename) {
          isvalid = this.authenticationService.validateFeature(featurename)
          return isvalid;
        }
      });
    }
    return isvalid;
  }
  getsubGroups(groupId: number) {
    this.subs.add(this.commonService.GetSubPagesPermission(groupId).pipe(res => res).subscribe(per => {
      per.data.map(permisission => permisission).map(features => {
        this.subGroupPermissions = features.features;
      });
    }));
  }

  ngOnDestroy() { this.subs.unsubscribe(); }

}




