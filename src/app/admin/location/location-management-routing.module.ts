import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocationManagementComponent } from './location-management.component';

const routes: Routes = [
  {
    path: '',
    component: LocationManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationManagementRoutingModule { }
