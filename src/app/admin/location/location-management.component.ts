import { Component, ViewChild, OnInit, AfterViewInit, ElementRef, ChangeDetectorRef, NgZone } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Location, LocationLocalization } from '@jms/app/shared/Entities/admin/Location';
import { LocationService } from '@jms/app/shared/Services/LocationService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { LanguageService } from '@jms/app/shared/Services/LanguageService';
import { Language } from '@jms/app/shared/Entities/admin/Language';
import { SwalService } from '@jms/app/shared/Services';

@Component({
  selector: 'app-location-management',
  templateUrl: './location-management.component.html',
  styleUrls: ['./location-management.component.css']
})
export class LocationManagementComponent implements OnInit {
  displayedColumns: string[] = ['name', 'isActive', 'action'];
  featureConst = constant;
  isSubmitted = false;
  private geoCoder: google.maps.Geocoder;
  locationLocalizations: LocationLocalization[] = [];
  insertLanguages: Language[] = [];
  location: Location = this.initObject();;
  locationAddress: string = '';

  public dataSource = new MatTableDataSource<any>();
  showForm: boolean = false;

  constructor(public dialog: MatDialog,
    private authService: AuthenticationService,
    private locationService: LocationService,
    private mapsAPI: MapsAPILoader,
    private languageService: LanguageService,
    private swalService: SwalService,
    private ngZone: NgZone,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.initList();
    this.languageService.GetBy(true).then((data: any) => {
      this.insertLanguages = data.data;
      this.location = this.initObject();
      this.initMap();
    });

    this.initAutocomplete();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
  public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  @ViewChild(AgmMap, { static: true }) map: AgmMap;
  @ViewChild('search', { read: ElementRef, static: false }) searchInput: ElementRef;

  initModal() {
    this.showForm = true;

    this.location = this.initObject();
    this.initMap();
  }

  Update(row_obj: any) {
    this.isSubmitted = false;
    this.locationLocalizations = [];
    this.location = row_obj;
    this.showForm = true;

    if (this.insertLanguages != null && this.insertLanguages.length > 0) {
      for (var i = 0; i < this.insertLanguages.length; i++) {
        var locationLocalization = this.getLanguageByFind(row_obj.locationLocalizations, this.insertLanguages[i].id);
        if (locationLocalization != null) this.locationLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, name: locationLocalization.name, description: locationLocalization.description });
        else this.locationLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, name: '', description: '' });
      }
      this.location.locationLocalizations = this.locationLocalizations;
    }
  }
  getLanguageByFind(list: any, id: any) {
    if (list != null) return list.find((x: { languageId: any; }) => x.languageId === id);
    return null;
  }

  onSubmit() {
    if (this.validateObject()) {
      if (this.location.id > 0) this.UpdateRowData();
      else this.AddRowData();
    }
  }

  AddRowData() {
    this.locationService.Add(this.location).then((result: any) => {
      Swal.fire('Add Location', `Your location ${this.location.name} added successfully`, 'success');
      this.initList();
      this.closeModal();
    });
  }

  UpdateRowData() {
    this.locationService.Update(this.location).then((data) => {
      Swal.fire('Update Location', `Your location ${this.location.name} updated successfully`, 'success');
      this.initList();
      this.closeModal();
    });
  }
  ActivateOrDeactivate(event: Event, row_obj: any) {
    event.preventDefault();
    event.stopPropagation();
    const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
    this.swalService.showConfirmMessage(message, `locaion ${row_obj.name}`).then(result => {
      if (result.value) {
        this.locationService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
          then((res: any) => {
            if (res.isSuccessful) {
              Swal.fire({
                icon: 'success',
                title: `${message}d successfully`,
                text: `Location ${message}d Successfully`
              })
              this.initList();
            } else {
              Swal.fire({
                icon: 'info',
                title: `Sorry can't ${message}`,
                text: res.message
              });
            }
          }).catch((err) => {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't ${message}`,
              text: err
            })
          });

      }
    })
  }
  Delete(row_obj: any) {
    Swal.fire({
      title: 'Delete Location ',
      text: `Are you sure to delete this ${row_obj.name} location?`,
      showCancelButton: true,
      confirmButtonColor: '#17BC9B',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.locationService.Delete(row_obj.rowId).then((res) => {
          if (res.status == 1) {
            this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
            Swal.fire({
              icon: 'info',
              text: 'Location has been deleted successfully'
            })
          } else {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't delete`,
              text: res.message
            })
          }
        }).catch((err) => {
          Swal.fire({
            icon: 'info',
            title: `Sorry can't delete`,
            text: err
          });
        });
      }
    });
  }
  initList() {
    this.locationService.GetAll().then((data: any) => {
      this.dataSource.data = data.data;
      this.table.renderRows();
    });
  }

  initObject() {
    this.isSubmitted = false;
    this.locationLocalizations = [];
    if (this.insertLanguages != null && this.insertLanguages.length > 0) {
      for (var i = 0; i < this.insertLanguages.length; i++) {
        this.locationLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, name: '', description: '' });
      }
    }
    var newObject: Location = { id: 0, rowId: '', name: '', description: '', isActive: true, latitude: 29.3117, longitude: 47.4818, zoom: 8, locationAddress: '', locationLocalizations: this.locationLocalizations };
    this.setCurrentLocation();
    return newObject;
  }
  validateObject() {
    this.isSubmitted = true;
    let isLocationLocalizationValid = true;
    for (var index = 0; index < this.location.locationLocalizations.length; index++) {
      if (this.location.locationLocalizations[index].name.trim() == '') {
        isLocationLocalizationValid = false;
        break;
      }
    }
    if (this.location.locationAddress.trim() == '' || this.location.latitude == null || this.location.longitude == null || isLocationLocalizationValid == false) return false;
    return true;
  }

  closeModal() {
    this.close();
    this.location = this.initObject();
  }

  initMap() {
    this.mapsAPI.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
    });
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.location.latitude = position.coords.latitude;
        this.location.longitude = position.coords.longitude;
        this.location.zoom = 10;
        this.getAddress(this.location.latitude, this.location.longitude);
      });
    }
  }

  getAddress(latitude: any, longitude: any) {
    if (latitude != null && longitude != null && this.geoCoder != null) {
      this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.location.zoom = 12;
            this.location.locationAddress = results[0].formatted_address;
          } else { window.alert('No results found'); }
        } else { window.alert('Geocoder failed due to: ' + status); }
      });
    }
  }

  findLocation() {
    let address = this.location.locationAddress;
    if (!this.geoCoder) { this.geoCoder = new google.maps.Geocoder(); }
    this.geoCoder.geocode({ 'address': address }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        for (var i = 0; i < results[0].address_components.length; i++) {
          let types = results[0].address_components[i].types;
          if (types.indexOf('locality') !== -1) this.location.locationAddress = results[0].address_components[i].long_name;
          if (types.indexOf('country') !== -1) this.location.locationAddress += ", " + results[0].address_components[i].long_name;
        }
        if (results[0].geometry.location) {
          this.location.latitude = results[0].geometry.location.lat();
          this.location.longitude = results[0].geometry.location.lng();
        }
        this.map.triggerResize();
      } else {
        alert("Sorry, this search produced no results.");
      }
    });
  }

  initAutocomplete() {
    this.mapsAPI.load().then(() => {
      let fromDestination = new google.maps.places.Autocomplete(this.searchInput.nativeElement, {});
      this.ngZone.run(() => {
        fromDestination.addListener("place_changed", () => {
          let place: google.maps.places.PlaceResult = fromDestination.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            this.location.locationAddress = "";
            return;
          } else {
            this.location.latitude = place.geometry.location.lat();
            this.location.longitude = place.geometry.location.lng();
            this.location.locationAddress = place.formatted_address;
          }
        });

        this.cd.detectChanges();
      });
    });
  }

  findAddressByCoordinates() {
    this.geoCoder.geocode({
      'location': {
        lat: this.location.latitude,
        lng: this.location.longitude
      }
    }, (results, status) => { this.decomposeAddressComponents(results); });
  }

  decomposeAddressComponents(addressArray) {
    if (addressArray.length == 0) { return false; }
    let address = addressArray[0].address_components;
    for (let element of address) {
      if (element.length == 0 && !element['types']) { continue; }
      if (element['types'].indexOf('street_number') > -1) {
        this.location.locationAddress = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('route') > -1) {
        this.location.locationAddress += ', ' + element['long_name'];
        continue;
      }
      if (element['types'].indexOf('country') > -1) {
        this.location.locationAddress += element['long_name'];
        continue;
      }
      if (element['types'].indexOf('postal_code') > -1) {
        this.location.locationAddress += element['long_name'];
        continue;
      }
    }
  }

  close() {
    this.showForm = false;
  }
}
