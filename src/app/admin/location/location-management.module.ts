import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationManagementRoutingModule } from './location-management-routing.module';
import { LocationManagementComponent } from './location-management.component';
import {
  MatFormFieldModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatTableModule,
  MatInputModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    LocationManagementComponent
  ],
  imports: [
    CommonModule,
    LocationManagementRoutingModule,
    MatFormFieldModule,
    MatTableModule,
    FormsModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    AgmCoreModule,
    NgbModule,
    MatInputModule
  ]
})
export class LocationManagementModule { }
