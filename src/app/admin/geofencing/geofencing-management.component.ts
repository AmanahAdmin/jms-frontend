import { Component, ViewChild, OnInit, AfterViewInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Zone } from '@jms/app/shared/Entities/admin/Zone';
import { ZoneService } from '@jms/app/shared/Services/ZoneService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { AgmMap, MapsAPILoader } from '@agm/core';
import { SwalService } from '@jms/app/shared/Services';
import { PlatformLocation } from '@angular/common';

declare const google: any;

@Component({
  selector: 'app-geofencing-management',
  templateUrl: './geofencing-management.component.html',
  styleUrls: ['./geofencing-management.component.css']
})
export class GeofencingManagementComponent implements OnInit {
  displayedColumns: string[] = ['name', 'isActive', 'action'];
  featureConst = constant;
  isSubmitted: boolean = false;
  zone: Zone;
  search: string;
  private geoCoder: google.maps.Geocoder;

  dataSource = new MatTableDataSource<any>();
  showManageForm: boolean = false;

  map: any;
  selectedArea = 0;
  selectedShape: any;
  drawingManager: google.maps.drawing.DrawingManager;

  constructor(
    public dialog: MatDialog,
    private authService: AuthenticationService,
    private zoneService: ZoneService,
    private swalService: SwalService,
    platformLocation: PlatformLocation,
    private mapsAPI: MapsAPILoader,
    private ngZone: NgZone,
    private cd: ChangeDetectorRef
  ) {
    platformLocation.onPopState(() => this.closeModal());
  }

  ngOnInit() {
    this.zone = this.initObject();
    this.setCurrentPosition();
    this.initList();
    this.initAutocomplete();
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }


  public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
  public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  @ViewChild('search', { read: ElementRef, static: false }) searchInput: ElementRef;

  initModal() {
    this.showManageForm = true;

    this.zone = this.initObject();
    this.setCurrentPosition();
    if (this.polygon != null) this.polygon.setMap(null);
    this.drawingManager.setOptions({
      drawingControl: true,
    });

    this.selectedArea = 0;
    this.deleteSelectedShape();
  }

  initAutocomplete() {
    this.mapsAPI.load().then(() => {
      let fromDestination = new google.maps.places.Autocomplete(this.searchInput.nativeElement, {});
      this.ngZone.run(() => {
        fromDestination.addListener("place_changed", () => {
          let place: google.maps.places.PlaceResult = fromDestination.getPlace();
          console.log(place.geometry)
          if (place.geometry === undefined || place.geometry === null) {
            this.zone.search = "";
            return;
          }
          else {
            this.zone.latitude = place.geometry.location.lat();
            this.zone.longitude = place.geometry.location.lng();
          }
        });
        this.cd.detectChanges();
      });
    });
  }

  Update(row_obj: any) {
    this.showManageForm = true;
    this.isSubmitted = false;

    this.zone = row_obj;
    if (this.polygon != null) this.polygon.setMap(null);
    this.drawPolygon(this.map);
  }

  onSubmit() {
    if (this.validateObject()) {
      if (this.zone.id > 0) this.UpdateRowData();
      else this.AddRowData();
    }
  }

  /**
   * add new zone  
   * 
   */
  AddRowData() {
    this.zoneService.Add(this.zone).then((result: any) => {
      Swal.fire('Add Zone', `Your zone ${this.zone.name} added successfully`, 'success');
      this.initList();
      this.closeModal();
      this.deleteSelectedShape();
    });
  }

  /**
   * update zone 
   * 
   * 
   */
  UpdateRowData() {
    this.zoneService.Update(this.zone).then((data) => {
      Swal.fire('Update Zone', `Your zone ${this.zone.name} updated successfully`, 'success');
      this.initList();
      this.closeModal();
    });
  }

  /**
   * activate / deactivate 
   * 
   * 
   * @param event 
   * @param row_obj 
   */
  ActivateOrDeactivate(event: Event, row_obj: any) {
    event.preventDefault();
    event.stopPropagation();
    const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
    this.swalService.showConfirmMessage(message, `zone ${row_obj.name}`).then(result => {
      if (result.value) {
        this.zoneService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
          then((res: any) => {
            if (res.isSuccessful) {
              Swal.fire({
                icon: 'success',
                title: `${message}d successfully`,
                text: `Zone ${message}d Successfully`
              })
              this.initList();
            } else {
              Swal.fire({
                icon: 'info',
                title: `Sorry can't ${message}`,
                text: res.message
              });
            }
          }).catch((err) => {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't ${message}`,
              text: err
            })
          });
      }
    })
  }

  Delete(row_obj: any) {
    Swal.fire({
      title: 'Delete Zone ',
      text: `Are you sure to delete this ${row_obj.name} zone?`,
      showCancelButton: true,
      confirmButtonColor: '#17BC9B',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.zoneService.Delete(row_obj.rowId).then((res) => {
          if (res.status == 1) {
            this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
            Swal.fire({
              icon: 'info',
              text: 'Zone has been deleted successfully'
            })
          } else {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't delete`,
              text: res.message
            })
          }
        }).catch((err) => {
          Swal.fire({
            icon: 'info',
            title: `Sorry can't delete`,
            text: err
          });
        });
      }
    });
  }

  initList() {
    this.showManageForm = false;

    this.zoneService.GetAll().then((data: any) => {
      this.dataSource.data = data.data;
      this.table.renderRows();
    });
  }

  initObject() {
    this.isSubmitted = false;
    var newObject: Zone = { id: 0, rowId: '', name: '', description: '', isActive: true, latitude: 29.3117, longitude: 47.4818, zoom: 10, zoneLocations: [], search: '' };
    return newObject;
  }

  validateObject() {
    this.isSubmitted = true;
    if (this.zone.name == null || this.zone.name.trim() == '' || this.zone.zoneLocations.length <= 0 || this.zone.zoneLocations == null) return false;
    else return true;
  }

  closeModal() {
    this.showManageForm = false;
    this.zone = this.initObject();
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.zone.latitude = position.coords.latitude;
        this.zone.longitude = position.coords.longitude;
      });
    } else {
      // set default kuwait location
      this.zone.latitude = 29.378586;
      this.zone.longitude = 47.990341;
    }

    this.zone.zoom = 10;
  }

  onMapReady(map: any) {
    this.map = map;
    this.initDrawingManager(map);
  }

  /**
   * draw manager
   * 
   * 
   * @param map 
   */
  initDrawingManager = (map: any) => {
    this.drawingManager = null;
    const self = this;
    const options = {
      drawingControl: true,
      drawingControlOptions: {
        drawingModes: ['polygon'],
      },
      polygonOptions: {
        // paths: this.zone.zoneLocations,
        draggable: true,
        editable: true,
        fillColor: 'red',
        strokeColor: 'red'
      },
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
    };

    if (map != null) {
      this.geoCoder = new google.maps.Geocoder;
      this.drawingManager = new google.maps.drawing.DrawingManager(options);
      this.drawingManager.setMap(map);

      google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {
        console.log("event.type = ", event.type)
        if (event.type === google.maps.drawing.OverlayType.POLYGON) {
          const paths = event.overlay.getPaths();
          for (let p = 0; p < paths.getLength(); p++) {
            google.maps.event.addListener(paths.getAt(p), 'set_at', () => {
              if (!event.overlay.drag) self.updatePointList(event.overlay.getPath());
            });
            google.maps.event.addListener(paths.getAt(p), 'insert_at', () => {
              self.updatePointList(event.overlay.getPath());
            });
            google.maps.event.addListener(paths.getAt(p), 'remove_at', () => {
              self.updatePointList(event.overlay.getPath());
            });
          }
          self.updatePointList(event.overlay.getPath());
        }

        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
          self.drawingManager.setDrawingMode(null);
          self.drawingManager.setOptions({ drawingControl: true, });
        }

        const newShape = event.overlay;
        newShape.type = event.type;
        this.setSelection(newShape);
      });
    }
  }

  /**
   * 
   * 
   * @param path 
   */
  updatePointList(path: any) {
    this.zone.zoneLocations = [];
    const len = path.getLength();
    for (let i = 0; i < len; i++) {
      this.zone.zoneLocations.push(path.getAt(i).toJSON());
    }
    this.selectedArea = google.maps.geometry.spherical.computeArea(path);
  }

  polygon: google.maps.Polygon = null;

  drawPolygon(map) {
    this.polygon = new google.maps.Polygon({
      paths: this.zone.zoneLocations,
      draggable: true,
      editable: true,
      strokeColor: 'red',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: 'red',
      fillOpacity: 0.35
    });
    this.selectedShape = this.polygon;

    this.drawingManager.setOptions({
      drawingControl: false,
    });

    this.selectedArea = 1;
    this.polygon.setMap(map);
  }

  clearSelection() {
    if (this.selectedShape) {
      this.selectedShape.setEditable(false);
      this.selectedShape = null;
      this.zone.zoneLocations = [];
    }
  }

  setSelection(shape) {
    this.clearSelection();
    this.selectedShape = shape;
    shape.setEditable(true);
  }

  deleteSelectedShape() {
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
      this.selectedArea = 0;
      this.zone.zoneLocations = [];
      this.drawingManager.setOptions({
        drawingControl: true,
      });
    }
  }

  /**
   * 
   * @param latitude 
   * @param longitude 
   */
  getAddress(latitude: any, longitude: any) {
    if (latitude != null && longitude != null && this.geoCoder != null) {
      this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.zone.zoom = 12;
            this.search = results[0].formatted_address;
          } else { window.alert('No results found'); }
        } else { window.alert('Geocoder failed due to: ' + status); }
      });
    }
  }

  /**
   * get location
   * 
   * 
   */
  findLocation() {
    let address = this.search;
    if (!this.geoCoder) { this.geoCoder = new google.maps.Geocoder(); }
    this.geoCoder.geocode({
      'address': address
    }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        for (var i = 0; i < results[0].address_components.length; i++) {
          let types = results[0].address_components[i].types;

          if (types.indexOf('locality') !== -1) {
            this.search = results[0].address_components[i].long_name;
          }
          if (types.indexOf('country') !== -1) {
            this.search += ", " + results[0].address_components[i].long_name;
          }
        }

        if (results[0].geometry.location) {
          this.zone.latitude = results[0].geometry.location.lat();
          this.zone.longitude = results[0].geometry.location.lng();
        }

        this.map.triggerResize();
      } else {
        alert("Sorry, this search produced no results.");
      }
    });
  }

  /**
   * address using coordinates
   * 
   * 
   */
  findAddressByCoordinates() {
    this.geoCoder.geocode({
      'location': {
        lat: this.zone.latitude,
        lng: this.zone.longitude
      }
    }, (results, status) => { this.decomposeAddressComponents(results); });
  }

  /**
   * 
   * @param addressArray 
   */
  decomposeAddressComponents(addressArray) {
    if (addressArray.length == 0) { return false; }
    let address = addressArray[0].address_components;

    for (let element of address) {
      if (element.length == 0 && !element['types']) { continue; }

      if (element['types'].indexOf('street_number') > -1) {
        this.search = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('route') > -1) {
        this.search += ', ' + element['long_name'];
        continue;
      }
      if (element['types'].indexOf('country') > -1) {
        this.search += element['long_name'];
        continue;
      }
      if (element['types'].indexOf('postal_code') > -1) {
        this.search += element['long_name'];
        continue;
      }
    }
  }
}
