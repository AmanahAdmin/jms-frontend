import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeofencingManagementComponent } from './geofencing-management.component';

const routes: Routes = [
  {
    path: '',
    component: GeofencingManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeofencingManagementRoutingModule { }
