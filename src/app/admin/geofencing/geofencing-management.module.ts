import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeofencingManagementRoutingModule } from './geofencing-management-routing.module';
import { GeofencingManagementComponent } from './geofencing-management.component';
import { MatAutocompleteModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { AgmDrawingModule } from '@agm/drawing';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    GeofencingManagementComponent,
  ],
  imports: [
    CommonModule,
    GeofencingManagementRoutingModule,
    FormsModule,

    // angulat material
    MatFormFieldModule,
    MatTableModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatAutocompleteModule,

    // google maps
    AgmCoreModule,
    AgmDrawingModule,
  ]
})
export class GeofencingManagementModule { }
