import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Payload } from '@jms/app/shared/Entities/admin/Payload';
import { PayloadService } from '@jms/app/shared/Services/PayloadService';
import { PayloadTypeService } from '@jms/app/shared/Services/PayloadTypeService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-payload-management',
    templateUrl: './payload-management.component.html',
    styleUrls: ['./payload-management.component.css']
})
export class PayloadManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'type', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted = false;
    payload: Payload = this.initObject();
    PayloadTypes: any = [];
    public dataSource = new MatTableDataSource<any>();
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private payloadService: PayloadService,
        private payloadTypeService: PayloadTypeService,
        private swalService: SwalService
    ) { }
    ngOnInit() {
        this.initList();
        this.payloadTypeService.GetLookup().then((data: any) => { this.PayloadTypes = data.data; });
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.payload = this.initObject(); }
    Update(row_obj: any) { this.isSubmitted = false; this.payload = row_obj; }

    onSubmit() {
        if (this.validateObject()) {
            if (this.payload.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.payloadService.Add(this.payload).then((result: any) => {
            Swal.fire('Add Payload', `Your payload ${this.payload.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        this.payloadService.Update(this.payload).then((data) => {
            Swal.fire('Update Payload', `Your payload ${this.payload.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `payload ${row_obj.name}`).then(result => {
            if (result.value) {
                this.payloadService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `payload ${message}d Successfully`
                            })
                            this.initList();
                        } else {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't ${message}`,
                                text: res.message
                            });
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        });
                    });
            }
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Payload',
            text: `Are you sure to delete this ${row_obj.name} payload?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.payloadService.Delete(row_obj.rowId).then((res) => {
                    if (res.isSuccessful) {
                        Swal.fire({
                            icon: 'info',
                            text: 'Payload has been deleted successfully'
                        });
                        this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
                    } else {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't delete`,
                            text: res.message
                        })
                    }
                });
            }
        }).catch((err) => {
            Swal.fire({
                icon: 'info',
                title: `Sorry can't delete`,
                text: err
            })
        });
    }
    initList() {
        this.payloadService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            console.log(data.data)
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Payload = { id: 0, rowId: '', name: '', isActive: true, details: '', payloadTypeId: null };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.payload.payloadTypeId == null) return false;
        if (this.payload.name.trim() == '') return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.payload = this.initObject();
    }
}
