import { Component, ViewChild, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import { ManagerRoles } from '@jms/app/shared/enums/user-roles.enum';
import { Permission } from '@jms/app/shared/Entities/User/Permission';
import { CommonService } from '@jms/app/shared/Services/CommonService';
import * as $ from 'jquery'
import { Role } from '@jms/app/shared/Entities/User/Role';
import { RoleService } from '@jms/app/shared/Services/RoleService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-role-management',
    templateUrl: './role-management.component.html',
    styleUrls: ['./role-management.component.css']
})
export class RoleManagementComponent implements OnInit, OnDestroy {
    displayedColumns: string[] = ['name', 'userRoleType', 'action'];
    featureConst = constant;
    isSubmitted: boolean = false;
    name: string = '';
    role: any = {};
    rolesFeatures: any[] = [];
    UserRoles = ManagerRoles;
    keys = [ManagerRoles.Driver, ManagerRoles.ProductLine, ManagerRoles.Dispatcher, ManagerRoles.JMC, ManagerRoles.OperationManager, ManagerRoles.GBM, ManagerRoles.QHSE] //Object.keys(ManagerRoles) ;
    Permissions: Permission[];
    SelectedPermission = [];
    showDefaultPermissions = true;
    subs: Subscription = new Subscription();
    public dataSource = new MatTableDataSource<any>();
    constructor(public dialog: MatDialog, private authService: AuthenticationService, private roleService: RoleService, private commonService: CommonService) { }

    ngOnInit() {
        this.getManagerPermissions();
        this.initList();
    }
    getRolesFeatures() {
        this.subs.add(this.commonService.GetFeatures().pipe(map(res => res)).subscribe(features => {
            this.rolesFeatures = features.data;

            this.rolesFeatures = this.Permissions.filter(
                feature => feature.isAppearInRoles);
        }));
    }
    getManagerPermissions() {
        this.commonService.GetManagerRolePermission().then((data: any) => {
            this.Permissions = data.data;
        });

    }
    getDriverPermisssions() {
        this.subs.add(
            this.commonService.GetDriverRolePermission().pipe(map(res => res)).subscribe(permissions => {
                this.Permissions = permissions.data;
                console.log('driver permissions', permissions);
            })
        );
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() {
        this.role = new Role();
        this.SelectedPermission = [];
    }
    Update(row_obj: any) {
        this.isSubmitted = false;
        this.role = row_obj;
        this.SelectedPermission = row_obj.permissions;
    }
    checkValue(event: any, value: string) {
        let isChecked = $(`#${value}`)[0]['checked'];
        if (isChecked) {
            if (!(this.SelectedPermission != null && this.SelectedPermission.indexOf(value) !== -1) || this.SelectedPermission == null) {
                this.SelectedPermission.push(value);
            }
        }
        else {
            if (this.SelectedPermission != null) {
                const index: number = this.SelectedPermission.indexOf(value);
                if (index !== -1) {
                    this.SelectedPermission.splice(index, 1);
                }
            }
        }
    }


    onSubmit() {
        if (this.validateObject()) {
            this.role.permissions = this.SelectedPermission;
            this.isSubmitted = true;
            if (this.role.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.roleService.Add(this.role).then((result: any) => {
            Swal.fire('Add Role', `Your role ${this.role.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
        }).catch((err) => {
            Swal.fire(`Can't Add Role`, err, 'error');
        });
    }
    UpdateRowData() {
        this.roleService.Update(this.role).then((data) => {
            Swal.fire('Update Role', `Your role ${this.role.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        }).catch((err) => {
            Swal.fire(`Can't Update Role`, err, 'error');
        });
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Role ',
            text: `Are you sure to delete this ${row_obj.name} role?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.roleService.Delete(row_obj.rowId).then((deleteResult: any) => {

                    if (deleteResult) {
                        if (deleteResult.isSuccessful === false || deleteResult.status === 0) {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't delete`,
                                text: deleteResult.message
                            })
                        }
                    }

                    this.initList();
                });
            }
        });
    }
    initList() {
        this.roleService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
            console.log()
        });
    }
    getSelectedRole(e) {
        if (e == 3) {
            this.showDefaultPermissions = false;
            this.getDriverPermisssions();
        }
        else {
            this.showDefaultPermissions = true;
            this.getManagerPermissions();
        }
    }
    initObject() {
        this.isSubmitted = false;
        var newObject: Role = { id: 0, rowId: '', name: '', userRoleType: 0, permissions: [], isCompanyRole: true };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.role.name == null || this.role.name.trim() == '' || this.role.userRoleType == null) return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.role = this.initObject();
    }
    getCompanyRole(e) {
        if (e.checked == false) {
            this.role.isCompanyRole = false;
        }
        else {
            this.role.isCompanyRole = true;
        }


    }
    ngOnDestroy() {
        this.subs.unsubscribe();
    }

}
