import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Vehicle, GatePassDetails } from '@jms/app/shared/Entities/admin/Vehicle';
import { VehicleService } from '@jms/app/shared/Services/VehicleService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { VehicleType } from '@jms/app/shared/Entities/admin/VehicleType';
import { VehicleTypeService } from '@jms/app/shared/Services/VehicleTypeService';
import { ZoneService } from '@jms/app/shared/Services/ZoneService';
import { ThirdPartyService } from '@jms/app/shared/Services/third-party.service';
import { ThirdPartyModel } from '@jms/app/shared/Models/ThirdPartyModel';
import { SwalService } from '@jms/app/shared/Services';
import { ZoneLocationRoutes } from '@jms/app/shared/Models/zone-location.model';


@Component({
    selector: 'app-vehicle-management',
    templateUrl: './vehicle-management.component.html',
    styleUrls: ['./vehicle-management.component.css']
})
export class VehicleManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'typeName', 'isActive', 'action'];
    thirdParties: ThirdPartyModel[] = [];
    featureConst = constant;
    isSubmitted = false;
    vehicle: Vehicle = this.initObject();
    VehicleTypes: VehicleType[] = [];
    Zones: ZoneLocationRoutes;
    isError: boolean = false;
    public dataSource = new MatTableDataSource<Vehicle>();
    languageId: number;
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private vehicleService: VehicleService,
        private vehicleTypeService: VehicleTypeService,
        private zoneService: ZoneService,
        private swalService: SwalService,
        private thirdPartyService: ThirdPartyService) { }

    ngOnInit() {
        this.initList();
        this.vehicleTypeService.GetLookup().then((data: any) => {
            this.VehicleTypes = data.data;
        });
        this.authService.currentUser.subscribe(
            user => {
                this.languageId = user.browsingLanguageId;
                this.vehicleService.getLookUps(this.languageId).then((data: any) => {
                    this.Zones = data.data;
                });
            }
        )

        this.getThirdParty();
    }
    /**
     * custom filter for nested objects
     */
    setupFilter() {
        this.dataSource.filterPredicate = (d: Vehicle, text: string) => {
            return this.searchInObject(d, text);
        };
    }
    searchInObject(searchIn: any, searchText: string): boolean {
        if (!searchIn) {
            return false;
        }
        if (typeof searchIn === 'object') {
            for (let column of Object.keys(searchIn)) {
                const isExist = this.searchInObject(searchIn[column], searchText);
                if (isExist) {
                    return true;
                }
            }
            return false;
        } else if (typeof searchIn === 'string' || typeof searchIn === 'number') {
            const textToSearch = searchIn.toString().toLowerCase();
            return textToSearch.indexOf(searchText) !== -1;
        }
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.vehicle = this.initObject(); }
    Update(row_obj: any) { this.isSubmitted = false; this.vehicle = row_obj; }

    onSubmit() {

        this.isError = false;
        if (this.validateObject()) {
            if (this.vehicle.id > 0) this.UpdateRowData();
            else this.AddRowData();
        } else {
            this.isError = true;
        }
    }
    AddRowData() {
        this.vehicleService.Add(this.vehicle).then((result: any) => {
            Swal.fire('Add Vehicle', `Your vehicle ${this.vehicle.plateNumber} added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        console.log(this.vehicle);
        this.vehicleService.Update(this.vehicle).then((data) => {
            Swal.fire('Update Vehicle', `Your vehicle ${this.vehicle.plateNumber} updated successfully`, 'success');
            this.initList();
            this.closeModal();
            console.log("Done")
        }, (err) => {
            console.log("Error")
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `vehicle ${row_obj.plateNumber}`).then(result => {
            if (result.value) {
                this.vehicleService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res: any) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `vehicle  ${message}d Successfully`
                            })
                            this.initList();
                        } else {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't ${message}`,
                                text: res.message
                            });
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        })
                    });
            }
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Vehicle  ',
            text: `Are you sure to delete this ${row_obj.plateNumber} vehicle ?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.vehicleService.Delete(row_obj.rowId).then((res) => {
                    if (res.status == 1) {
                        Swal.fire({
                            icon: 'info',
                            text: 'Vehicle has been deleted successfully'
                        });
                        this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
                    } else {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't delete`,
                            text: res.message
                        })
                    }
                }).catch((err) => {
                    Swal.fire({
                        icon: 'info',
                        title: `Sorry can't delete`,
                        text: err
                    });
                });
            }
        });
    }
    initList() {
        this.vehicleService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
            console.log(data.data)
        });
    }

    addGatePass() {
        this.vehicle.gatePassDetails.push({ id: 0, contractNumber: '', zoneId: null, vehicleId: this.vehicle.id, expiredDate: null, checkpointId: null });
    }
    deleteGatePass(index: number) {
        if (index !== -1) {
            this.vehicle.gatePassDetails.splice(index, 1);
        }
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Vehicle = { id: 0, rowId: '', plateNumber: '', typeName: '', isActive: true, color: '', licenseExpiryDateFrom: null, licenseExpiryDateTo: null, vehicleTypeId: null, isCompanyVehicle: true, vehicleTypeName: '', supplierContractNo: '', thirdPartyManagementId: null, gatePassDetails: [] };
        return newObject;
    }

    validateObject() {
        this.isSubmitted = true;
        var isGatePassDetailsValid = true;
        if (this.vehicle.gatePassDetails.length > 0) {
            for (var index = 0; index < this.vehicle.gatePassDetails.length; index++) {
                if ((this.vehicle.gatePassDetails[index].zoneId == null && this.vehicle.gatePassDetails[index].checkpointId == null) || !this.vehicle.gatePassDetails[index].expiredDate || !this.vehicle.gatePassDetails[index].contractNumber) {
                    isGatePassDetailsValid = false;
                    break;
                }
            }
        }
        //if(this.vehicle.thirdPartyManagementId == null) return false;
        if (this.vehicle.plateNumber.trim() == '' ||
            this.vehicle.licenseExpiryDateTo == null ||
            this.vehicle.licenseExpiryDateFrom == null ||
            this.vehicle.color.trim() == '' || this.vehicle.vehicleTypeId == null || !isGatePassDetailsValid) return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.vehicle = this.initObject();
        this.initList();
    }


    /**
    * get all third Party
    *
    */
    getThirdParty() {
        this.thirdPartyService.GetAll('')
            .subscribe((result: any) => {
                this.thirdParties = result.data;
                this.thirdParties = this.thirdParties.filter(a => a.isActive == true);

            })
    }

    setGatePassRoute(event, index: number) {
        const selectedIndex = event.target.selectedIndex;
        const selectedValue = event.target.value;
        const optGroupLabel = event.target.options[
            selectedIndex
        ].parentNode.getAttribute("label");
        if (optGroupLabel == "Zones") {
            this.vehicle.gatePassDetails[index].zoneId = Number(selectedValue);
        } else {
            this.vehicle.gatePassDetails[index].checkpointId = Number(selectedValue);
        }
    }
}
