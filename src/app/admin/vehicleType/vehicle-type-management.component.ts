import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { VehicleType } from '@jms/app/shared/Entities/admin/VehicleType';
import { VehicleTypeService } from '@jms/app/shared/Services/VehicleTypeService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { ZoneService } from '@jms/app/shared/Services/ZoneService';
import { Zone } from '@jms/app/shared/Entities/admin/Zone';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-vehicle-type-management',
    templateUrl: './vehicle-type-management.component.html',
    styleUrls: ['./vehicle-type-management.component.css']
})
export class VehicleTypeManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted = false;
    isHiddenOne = false;
    isHiddenTwo = true;
    isHiddenThree = true;
    isDisplayed = false;
    selectedRadio: number = 1;
    Zones: Zone[] = [];
    vehicleType: VehicleType = this.initObject();
    public times: any[] = [{
    }];

    public locations: any[] = [{
    }];

    public TimeAndlocations: any[] = [{
        //time:null,
        //location:null
    }]

    public dataSource = new MatTableDataSource<any>();
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private vehicleTypeService: VehicleTypeService,
        private zoneService: ZoneService,
        private swalService: SwalService
    ) { }
    ngOnInit() {
        this.initList();
        this.zoneService.GetLookup().then((data: any) => {
            this.Zones = data.data;

        });
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() {

        this.vehicleType = this.initObject();
        this.selectedRadio = 1;
        this.TimeAndlocations = [{}];
        this.times = [{}];
        this.locations = [{}];
        this.isSelected(1);
        this.setradio(1);
    }
    Update(row_obj: any) {
        this.isSubmitted = false; this.vehicleType = row_obj;
        console.log('returned', row_obj);
    }

    onSubmit() {
        if (this.validateObject()) {
            if (this.vehicleType.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }

    AddRowData() {
        if (this.selectedRadio == 1) {
            this.vehicleType.times = this.times;
            this.vehicleType.IsInSpecificTime = true;
            this.vehicleType.IsInSpecificRoute = false;
            this.vehicleType.IsInBothTimeAndRoue = false;
            this.TimeAndlocations = [];
            this.locations = [];
        }
        if (this.selectedRadio == 2) {
            this.vehicleType.locations = this.locations;
            this.vehicleType.IsInSpecificRoute = true;
            this.vehicleType.IsInSpecificTime = false;
            this.vehicleType.IsInBothTimeAndRoue = false;
            this.TimeAndlocations = [];
            this.times = [];
        }
        if (this.selectedRadio == 3) {
            this.vehicleType.vehicleTypeTimeAndLocation = this.TimeAndlocations;
            this.vehicleType.IsInBothTimeAndRoue = true;
            this.vehicleType.IsInSpecificTime = false;
            this.vehicleType.IsInSpecificRoute = false;
            this.locations = [];
            this.times = [];
        }
        this.vehicleTypeService.Add(this.vehicleType).then((result: any) => {
            Swal.fire('Add VehicleType', `Your vehicleType ${this.vehicleType.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
            this.vehicleType = this.initObject();
        });
    }

    UpdateRowData() {

        this.vehicleTypeService.Update(this.vehicleType).then((data) => {

            Swal.fire('Update VehicleType', `Your vehicleType ${this.vehicleType.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }


    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `vehicle type ${row_obj.name}`).then(result => {
            if (result.value) {
                this.vehicleTypeService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res: any) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `vehicle type ${message}d Successfully`
                            })
                            this.initList();
                        } else {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't ${message}`,
                                text: res.message
                            });
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        })
                    });
            }
        })
    }

    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Vehicle Type ',
            text: `Are you sure to delete this ${row_obj.name} vehicle type?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.vehicleTypeService.Delete(row_obj.rowId).then((res) => {
                    if (res.status == 1) {
                        Swal.fire({
                            icon: 'info',
                            text: 'Vehicle Type has been deleted successfully'
                        });
                        this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
                    } else {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't delete`,
                            text: res.message
                        })
                    }
                }).catch((err) => {
                    Swal.fire({
                        icon: 'info',
                        title: `Sorry can't delete`,
                        text: err
                    });
                });
            }
        });
    }

    initList() {
        this.vehicleTypeService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: VehicleType = { id: 0, rowId: '', name: '', isActive: true, isNeedingPreRequestForJourneyInitiation: false, locations: null, times: null, vehicleTypeTimeAndLocation: [], IsInSpecificTime: false, IsInSpecificRoute: false, IsInBothTimeAndRoue: false };
        return newObject;
    }

    validateObject() {
        this.isSubmitted = true;
        if (this.vehicleType.name.trim() == '') return false;
        else return true;
    }

    closeModal() {
        $('.close').click();
        this.vehicleType = this.initObject();
    }
    // RadioClick(val){
    //     debugger
    // }
    // radioChange(val){
    //     debugger
    //     if(val==2)
    //     this.isHiddenTwo=false;
    // }
    setradio(e: number): void {
        this.selectedRadio = e;
    }

    isSelected(num: number): boolean {
        if (!this.selectedRadio) { // if no radio button is selected, always return false so every nothing is shown
            return false;
        }

        return (this.selectedRadio === num); // if current radio button is selected, return true, else return false
    }


    addNewTime() {
        this.times.push({

        });
    }
    deleteTime(index: number) {
        if (index !== -1) {
            this.times.splice(index, 1);
        }
    }

    addNewLocation() {
        this.locations.push({

        });
    }
    deleteLocation(index: number) {
        if (index !== -1) {
            this.locations.splice(index, 1);
        }
    }


    addNewTimeAndLocation() {
        this.TimeAndlocations.push({

        });
    }

    deleteTimeAndLocation(index: number) {
        if (index !== -1) {
            this.TimeAndlocations.splice(index, 1);
        }
    }
}
