import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Team } from '@jms/app/shared/Entities/admin/Team';
import { TeamService } from '@jms/app/shared/Services/TeamService';
import { User } from '@jms/app/shared/models/UserModel';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-team-management',
    templateUrl: './team-management.component.html',
    styleUrls: ['./team-management.component.css']
})
export class TeamManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'userCount', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted: boolean = false;
    name: string = '';
    team: Team;
    teamUsers: User[] = null;
    teamName: string = '';
    public dataSource = new MatTableDataSource<any>();
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private teamService: TeamService,
        private swalService: SwalService
    ) { }
    ngOnInit() {
        this.team = this.initObject();
        this.initList();
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.team = this.initObject(); }
    Update(row_obj: any) { this.isSubmitted = false; this.team = row_obj; }
    BrowseUser(row_obj: any) {
        this.teamName = row_obj.name;
        this.teamUsers = row_obj.users;
    }

    onSubmit() {
        if (this.validateObject()) {
            if (this.team.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.teamService.Add(this.team).then((result: any) => {
            Swal.fire('Add Department', `Your Department ${this.team.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        this.teamService.Update(this.team).then((data) => {
            Swal.fire('Update Department', `Your Department ${this.team.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `Department ${row_obj.name}`).then(result => {
            if (result.value) {
                this.teamService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                then((deleteResult :any)=> {
                  if(deleteResult.isSuccessful===false || deleteResult.status===0){
                    Swal.fire({
                      icon: 'info',
                      title: `Sorry can't deactivate`,
                      text: deleteResult.message
                    })
                  }
                  this.initList();
                });
            }
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Department ',
            text: `Are you sure to delete this ${row_obj.name} Department?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.teamService.Delete(row_obj.rowId)
                .then((deleteResult :any)=> {
                  console.log(deleteResult)
                  if(deleteResult){
                    if(deleteResult.isSuccessful===false || deleteResult.status===0){
                      Swal.fire({
                        icon: 'info',
                        title: `Sorry can't delete`,
                        text: deleteResult.message
                      })
                    }
                  }

                  this.initList();
                });
            }
        });
    }
    initList() {
        this.teamService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Team = { id: 0, rowId: '', name: '', description: '', isHavingActiveJourney: false, isActive: true, userCount: 0, users: null };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.team.name == null || this.team.name.trim() == '') return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.team = this.initObject();
    }
}
