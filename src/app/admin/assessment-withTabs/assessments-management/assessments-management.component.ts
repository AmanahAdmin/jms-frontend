import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { User } from '@jms/app/shared/models/UserModel';

@Component({
  selector: 'app-assessments-management',
  templateUrl: './assessments-management.component.html',
  styleUrls: ['./assessments-management.component.css'],
})
export class AssessmentsManagementComponent implements OnInit {
  currentUser: User;
  constructor(private authenticationService: AuthenticationService) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
  }
  onTabClick(e) {

  }
}
