import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { WorkflowService } from '@jms/app/shared/Services/WorkflowService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { Workflow, WorkflowStep, WorkflowStepWorkflow, WorkflowStepRole, WorkflowStepRoleCondition } from '@jms/app/shared/Entities/Journey/Workflow';
import { CommonService } from '@jms/app/shared/Services/CommonService';
import { RoleService } from '@jms/app/shared/Services/RoleService';
import { ConcatenateType } from '@jms/app/shared/enums/concatenate-type.enum';
import { inherits } from 'util';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-workflow-management',
    templateUrl: './workflow-management.component.html',
    styleUrls: ['./workflow-management.component.css']
})
export class WorkflowManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted: boolean = false;
    name: string = '';
    workflow: Workflow;
    Features: [] = [];
    Roles: any = [];
    CustomRoles: any[] = [{ id: 'system', name: 'System' }, { id: 'driver', name: 'Driver' }, { id: 'approvedRole', name: 'Journey Approved Roles' }, { id: 'notifiedRole', name: 'Journey Notified Roles' }];
    Workflows: [] = [];
    Conditions: [] = [];
    ConcatenateTypes: any = [ConcatenateType.Or, ConcatenateType.And];
    ConcatenateTypeEnum: ConcatenateType;

    public dataSource = new MatTableDataSource<any>();
    constructor(public dialog: MatDialog, private authService: AuthenticationService,
        private commonService: CommonService,
        private roleService: RoleService,
        private workflowService: WorkflowService,
        private swalService: SwalService
    ) { }
    ngOnInit() {
        this.workflow = this.initObject();
        this.initList();
        this.commonService.GetWorkflowFeature().then((data: any) => { this.Features = data.data; });
        this.commonService.GetWorkflowCondition().then((data: any) => { this.Conditions = data.data; });
        this.roleService.GetLookup().then((data: any) => {
            this.Roles = data.data;
            for (var index = 0; index < this.Roles.length; index++) {
                this.CustomRoles.push({ id: this.Roles[index].id, name: this.Roles[index].name });
            }
        });
        this.workflowService.GetLookup().then((data: any) => { this.Workflows = data.data; });
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.isSubmitted = false; this.workflow = this.initObject(); }
    Update(row_obj: any) {
        this.isSubmitted = false;
        this.workflow = row_obj;
        if (this.workflow.workflowSteps != null && this.workflow.workflowSteps.length > 0) {
            for (var index = 0; index < this.workflow.workflowSteps.length; index++) {
                var workflowStep = this.workflow.workflowSteps[index];
                if (workflowStep.workflowStepRoles != null && workflowStep.workflowStepRoles.length > 0)
                    for (var rIndex = 0; rIndex < workflowStep.workflowStepRoles.length; rIndex++) {
                        var stepRole = workflowStep.workflowStepRoles[rIndex];
                        workflowStep.workflowStepRoles[rIndex] = this.GetWorkflowStepRole(stepRole);
                    }
                if (workflowStep.workflowStepWorkflows != null && workflowStep.workflowStepWorkflows.length > 0)
                    for (var wIndex = 0; wIndex < workflowStep.workflowStepWorkflows.length; wIndex++) {
                        var stepWorkflow = workflowStep.workflowStepWorkflows[wIndex];
                        workflowStep.workflowStepWorkflows[wIndex] = this.GetWorkflowStepWorkflow(stepWorkflow);
                    }
            }
        }
    }

    addFeature() {
        var stepFeature = new WorkflowStep();
        stepFeature.isFeature = true;
        stepFeature.workflowId = this.workflow.id;
        stepFeature.workflowStepRoles.push(new WorkflowStepRole());
        this.workflow.workflowSteps.push(stepFeature);
    }
    addWorkflow() {
        var stepFeature = new WorkflowStep();
        stepFeature.isFeature = false;
        stepFeature.workflowId = this.workflow.id;
        var subWorkFlow = new WorkflowStepWorkflow();
        stepFeature.workflowStepWorkflows.push(subWorkFlow);
        this.workflow.workflowSteps.push(stepFeature);
    }
    deleteStep(stepIndex) {
        if (stepIndex !== -1) {
            this.workflow.workflowSteps.splice(stepIndex, 1);
        }
    }
    addFeatureRole(stepIndex) {
        let role = new WorkflowStepRole();
        role.worflowStepId = this.workflow.workflowSteps[stepIndex].id
        this.workflow.workflowSteps[stepIndex].workflowStepRoles.push(role);
    }
    deleteFeatureRole(stepIndex, roleIndex) {
        if (stepIndex !== -1 && roleIndex !== -1) {
            this.workflow.workflowSteps[stepIndex].workflowStepRoles.splice(roleIndex, 1);
        }
    }
    addRoleCondition(stepIndex, roleIndex) {
        var condition = new WorkflowStepRoleCondition();
        condition.workflowStepRoleId = this.workflow.workflowSteps[stepIndex].workflowStepRoles[roleIndex].id;
        this.workflow.workflowSteps[stepIndex].workflowStepRoles[roleIndex].workflowStepRoleConditions.push(condition);
    }
    deleteRoleCondition(stepIndex, roleIndex, conditionIndex) {
        if (stepIndex !== -1 && roleIndex !== -1 && conditionIndex !== -1) {
            this.workflow.workflowSteps[stepIndex].workflowStepRoles[roleIndex].workflowStepRoleConditions.splice(conditionIndex, 1);
        }
    }
    addSubWorkflow(stepIndex) {
        var stepWorkflow = new WorkflowStepWorkflow();
        stepWorkflow.workflowStepId = this.workflow.workflowSteps[stepIndex].id;
        this.workflow.workflowSteps[stepIndex].workflowStepWorkflows.push(stepWorkflow);
    }
    deleteSubWorkflow(stepIndex, subWorkflowIndex) {
        if (stepIndex !== -1 && subWorkflowIndex !== -1) {
            this.workflow.workflowSteps[stepIndex].workflowStepWorkflows.splice(subWorkflowIndex, 1);
        }
    }

    onSubmit() {
        if (this.validateObject()) {
            if (this.workflow.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.workflowService.Add(this.workflow).then((result: any) => {
            Swal.fire('Add Workflow', `Your workflow ${this.workflow.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        this.workflowService.Update(this.workflow).then((data) => {
            Swal.fire('Update Workflow', `Your workflow ${this.workflow.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `workflow ${row_obj.name}`).then(result => {
            if (result.value) {
                this.workflowService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then(() => { this.initList(); });
            }
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Workflow ',
            text: `Are you sure to delete this ${row_obj.name} workflow?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.workflowService.Delete(row_obj.rowId).then(() => {
                    this.dataSource.data = this.dataSource.data.filter((value, key) => {
                        return value.id != row_obj.id;
                    });
                });
            }
        });
    }
    initList() {
        this.workflowService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        return new Workflow();
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.workflow.workflowSteps != null && this.workflow.workflowSteps.length > 0) {
            for (var index = 0; index < this.workflow.workflowSteps.length; index++) {
                var workflowStep = this.workflow.workflowSteps[index];
                if (workflowStep.workflowStepRoles != null && workflowStep.workflowStepRoles.length > 0)
                    for (var rIndex = 0; rIndex < workflowStep.workflowStepRoles.length; rIndex++) {
                        var stepRole = workflowStep.workflowStepRoles[rIndex];
                        workflowStep.workflowStepRoles[rIndex] = this.SetWorkflowStepRole(stepRole);
                    }
                if (workflowStep.workflowStepWorkflows != null && workflowStep.workflowStepWorkflows.length > 0)
                    for (var wIndex = 0; wIndex < workflowStep.workflowStepWorkflows.length; wIndex++) {
                        var stepWorkflow = workflowStep.workflowStepWorkflows[wIndex];
                        workflowStep.workflowStepWorkflows[wIndex] = this.SetWorkflowStepWorkflow(stepWorkflow);
                    }
            }
        }

        if (this.workflow.name == null || this.workflow.name.trim() == '') return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.workflow = this.initObject();
    }
    SetWorkflowStepRole(workflowStepRole: WorkflowStepRole) {
        switch (workflowStepRole.customRoleId) {
            case 'system': {
                workflowStepRole.isSystemRole = true;
                workflowStepRole.isNotifiedManagerRole = false;
                workflowStepRole.isApprovedManagerRole = false;
                workflowStepRole.roleId = null;
            } break;
            case 'approvedRole': {
                workflowStepRole.isSystemRole = false;
                workflowStepRole.isNotifiedManagerRole = false;
                workflowStepRole.isApprovedManagerRole = true;
                workflowStepRole.roleId = null;
            } break;
            case 'notifiedRole': {
                workflowStepRole.isSystemRole = false;
                workflowStepRole.isNotifiedManagerRole = true;
                workflowStepRole.isApprovedManagerRole = true;
                workflowStepRole.roleId = null;
            } break;
            default: {
                workflowStepRole.isSystemRole = false;
                workflowStepRole.isNotifiedManagerRole = false;
                workflowStepRole.isApprovedManagerRole = false;
                workflowStepRole.roleId = workflowStepRole.customRoleId;
            } break;
        }
        return workflowStepRole;
    }
    GetWorkflowStepRole(workflowStepRole: WorkflowStepRole) {
        if (workflowStepRole.isSystemRole == true) workflowStepRole.customRoleId = 'system';
        else if (workflowStepRole.isApprovedManagerRole == true) workflowStepRole.customRoleId = 'approvedRole';
        else if (workflowStepRole.isNotifiedManagerRole == true) workflowStepRole.customRoleId = 'notifiedRole';
        else workflowStepRole.customRoleId = workflowStepRole.roleId;
        return workflowStepRole;
    }

    SetWorkflowStepWorkflow(workflowStepWorkflow: WorkflowStepWorkflow) {
        switch (workflowStepWorkflow.customRoleId) {
            case 'system': {
                workflowStepWorkflow.isSystemRole = true;
                workflowStepWorkflow.isNotifiedManagerRole = false;
                workflowStepWorkflow.isApprovedManagerRole = false;
                workflowStepWorkflow.roleId = null;
            } break;
            case 'approvedRole': {
                workflowStepWorkflow.isSystemRole = false;
                workflowStepWorkflow.isNotifiedManagerRole = false;
                workflowStepWorkflow.isApprovedManagerRole = true;
                workflowStepWorkflow.roleId = null;
            } break;
            case 'notifiedRole': {
                workflowStepWorkflow.isSystemRole = false;
                workflowStepWorkflow.isNotifiedManagerRole = true;
                workflowStepWorkflow.isApprovedManagerRole = true;
                workflowStepWorkflow.roleId = null;
            } break;
            default: {
                workflowStepWorkflow.isSystemRole = false;
                workflowStepWorkflow.isNotifiedManagerRole = false;
                workflowStepWorkflow.isApprovedManagerRole = false;
                workflowStepWorkflow.roleId = workflowStepWorkflow.customRoleId;
            } break;
        }
        return workflowStepWorkflow;
    }
    GetWorkflowStepWorkflow(workflowStepWorkflow: WorkflowStepWorkflow) {
        if (workflowStepWorkflow.isSystemRole == true) workflowStepWorkflow.customRoleId = 'system';
        else if (workflowStepWorkflow.isApprovedManagerRole == true) workflowStepWorkflow.customRoleId = 'approvedRole';
        else if (workflowStepWorkflow.isNotifiedManagerRole == true) workflowStepWorkflow.customRoleId = 'notifiedRole';
        else workflowStepWorkflow.customRoleId = workflowStepWorkflow.roleId;
        return workflowStepWorkflow;
    }


}