import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentLocalizationQuestionsComponent } from './assessment-localization-questions.component';

describe('AssessmentLocalizationQuestionsComponent', () => {
  let component: AssessmentLocalizationQuestionsComponent;
  let fixture: ComponentFixture<AssessmentLocalizationQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentLocalizationQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentLocalizationQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
