import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { DisplayWay } from '@jms/app/shared/enums/display-way.enum';

import { AssessmentLocalization, AssessmentRisk, Assessment, AssessmentQuestion, AssessmentQestionAnswer } from '@jms/app/shared/Entities/admin/Assessment';
@Component({
  selector: 'app-assessment-localization-questions',
  templateUrl: './assessment-localization-questions.component.html',
  styleUrls: ['./assessment-localization-questions.component.scss']
})
export class AssessmentLocalizationQuestionsComponent implements OnInit {

  @Input() assessmentLocalization: AssessmentLocalization;
  @Input() assessmentRisks: AssessmentRisk[] = [];
  @Input() assessment: Assessment;
  @Input() isSubmitted;
  @Input() isAutomaticRisk;
  displayWays: any;

  constructor(
    private cd: ChangeDetectorRef,

  ) { }

  ngOnInit() {
    this.displayWays = [{ id: DisplayWay.RadioButton, name: 'Multichoice' }];

  }

  validateRiskValues() {
    // let questionCount=this.assessment.assessmentLocalizations[0].assessmentQuestions.length;
    // for(var index=0; index<this.assessment.assessmentRisks.length;index++){
    //     if(this.assessment.assessmentRisks[index].startValue<0 || this.assessment.assessmentRisks[index].startValue>questionCount ) this.assessment.assessmentRisks[index].startValue='';
    //     if(this.assessment.assessmentRisks[index].endValue<0 || this.assessment.assessmentRisks[index].endValue>questionCount ) this.assessment.assessmentRisks[index].endValue='';
    // }
  }
  addQuestion() {
    for (var index = 0; index < this.assessment.assessmentLocalizations.length; index++) {
      var questions = this.assessment.assessmentLocalizations[index].assessmentQuestions;
      if (questions != null && questions.length > 0) {
        var order = Math.max.apply(Math, questions.map(function (o) { return o.order; }));
        questions.push({ id: 0, assessmentId: this.assessment.id, isRequired: false, order: order + 1, name: '', assessmentQuestionAnswers: [] })
      } else {
        questions = [];
        questions.push({ id: 0, assessmentId: this.assessment.id, isRequired: false, order: 1, name: '', assessmentQuestionAnswers: [] })
      }

      this.assessment.assessmentLocalizations[index].assessmentQuestions = questions;
    }
    this.cd.detectChanges();
  }

  /**
   * delete question
   * 
   * 
   * @param assessmentQuestionIndex 
   */
  deleteQuestion(assessmentQuestionIndex: number) {
    if (assessmentQuestionIndex !== -1) {
      for (var index = 0; index < this.assessment.assessmentLocalizations.length; index++) {
        this.assessment.assessmentLocalizations[index].assessmentQuestions.splice(assessmentQuestionIndex, 1);
      }
    }
    this.cd.detectChanges();
  }

  /**
   * drop item 
   * 
   * 
   * @param event 
   */
  drop(event: CdkDragDrop<AssessmentQuestion[]>) {
    for (let index = 0; index < this.assessment.assessmentLocalizations.length; index++) {
      this.assessment.assessmentLocalizations[index].assessmentQuestions[event.previousIndex].order = event.currentIndex;
      this.assessment.assessmentLocalizations[index].assessmentQuestions[event.currentIndex].order = event.previousIndex;
      moveItemInArray(this.assessment.assessmentLocalizations[index].assessmentQuestions, event.previousIndex, event.currentIndex);
    }
    this.cd.detectChanges();
  }


  addAnswer(questionIndex) {
    for (var index = 0; index < this.assessment.assessmentLocalizations.length; index++) {
      var question = this.assessment.assessmentLocalizations[index].assessmentQuestions[questionIndex];
      if (question.assessmentQuestionAnswers != null && question.assessmentQuestionAnswers.length > 0) {
        question.assessmentQuestionAnswers.push({ pointValue: 0, name: '', assessmentQuestionId: question.id, id: 0 })
      } else {
        question.assessmentQuestionAnswers = [];
        question.assessmentQuestionAnswers.push({ pointValue: 0, name: '', assessmentQuestionId: question.id, id: 0 })
      }

      this.assessment.assessmentLocalizations[index].assessmentQuestions[questionIndex].assessmentQuestionAnswers = question.assessmentQuestionAnswers;
    }
    this.cd.detectChanges();

  }

  deleteAnswer(assessmentQuestionIndex: number, answerIndex) {
    if (assessmentQuestionIndex !== -1) {
      for (var index = 0; index < this.assessment.assessmentLocalizations.length; index++) {
        this.assessment.assessmentLocalizations[index].assessmentQuestions[assessmentQuestionIndex].assessmentQuestionAnswers.splice(answerIndex, 1);
      }
    }
    this.cd.detectChanges();
  }
}
