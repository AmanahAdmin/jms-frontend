import { Component, ViewChild, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Assessment, AssessmentLocalization, AssessmentQuestion, AssessmentRisk } from '@jms/app/shared/Entities/admin/Assessment';
import { AssessmentService } from '@jms/app/shared/Services/AssessmentService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { Language } from '@jms/app/shared/Entities/admin/Language';
import { LanguageService } from '@jms/app/shared/Services/LanguageService';
import { AssessmentCategory } from '@jms/app/shared/Entities/admin/AssessmentCategory';
import { AssessmentCategoryService } from '@jms/app/shared/Services/AssessmentCategoryService';
import { Level } from '@jms/app/shared/enums/level.enum';
import { DisplayWay } from '@jms/app/shared/enums/display-way.enum';
import { SwalService } from '@jms/app/shared/Services';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-assessment-management',
    templateUrl: './assessment-management.component.html',
    styleUrls: ['./assessment-management.component.css']
})
export class AssessmentManagementComponent implements OnInit, AfterViewInit {
    displayedColumns: string[] = ['name', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted = false;
    isAutomaticRisk = false;
    level: Level;
    assessment: Assessment = this.initObject();
    assessmentLocalizations: AssessmentLocalization[] = [];
    insertLanguages: Language[] = [];
    assessmentCategory: AssessmentCategory = { id: null, rowId: '', name: '', isActive: false, isAllowAutoRiskManagement: false };
    assessmentCategories: AssessmentCategory[] = [];
    dataSource = new MatTableDataSource<any>();

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;
    isAssessmentQuestionsNotMissing: boolean;
    isAssessmentQuestionsAnswersNotMissing: boolean;

    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private assessmentService: AssessmentService,
        private languageService: LanguageService,
        private assessmentCategoryService: AssessmentCategoryService,
        private cd: ChangeDetectorRef,
        private swalService: SwalService,
    ) { }

    ngOnInit() {
        this.assessmentCategoryService.GetLookup().then((data: any) => {
            this.assessmentCategories = data.data;
        });

        this.languageService.GetBy(true).then((data: any) => {
            this.insertLanguages = data.data;
            this.assessment = this.initObject();
        });

        this.initList();
    }

    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    initModal() {
        this.assessment = this.initObject();
    }

    /**
     * open modal with selected previous assessment
     * 
     * 
     * @param assessment 
     */
    Update(assessment: any) {
        this.assessment = assessment;
        this.changeAssessmentCategory();
        if (assessment.isPreTrip) {
            this.checkValue('isPreTrip', assessment.isPreTrip);
        }

        if (this.insertLanguages != null && this.insertLanguages.length > 0) {
            for (let i = 0; i < this.insertLanguages.length; i++) {
                let assessmentLocalization = this.getLanguageByFind(assessment.assessmentLocalizations, this.insertLanguages[i].id);
                if (assessmentLocalization == null) {
                    this.assessment.assessmentLocalizations.push({
                        languageId: this.insertLanguages[i].id,
                        languageName: this.insertLanguages[i].name,
                        languageIsDefault: this.insertLanguages[i].isDefault,
                        name: '', assessmentQuestions: []
                    });
                }
            }
        }
    }

    getLanguageByFind(list: any, id: any) {
        if (list != null) return list.find((x: { languageId: any; }) => x.languageId === id);
        return null;
    }

    changeAssessmentCategory() {
        if (this.assessmentCategories.length > 0) {
            const index: number = this.assessmentCategories.findIndex(e => e.id == this.assessment.assessmentCategoryId);
            if (index !== -1) {
                this.assessmentCategory = this.assessmentCategories[index];
                if (this.assessmentCategory.isAllowAutoRiskManagement && this.assessment.isPreTrip) {
                    this.checkValue('isPreTrip', this.assessment.isPreTrip);
                    this.isAutomaticRisk = true;
                } else {
                    this.isAutomaticRisk = false;
                }
            }
        }
    }

    checkValue(value: string, isChecked) {
        //var isChecked = assessmentQuestionIndex != null ? $(`#${value}-${assessmentQuestionIndex}`)[0]['checked'] : $(`#${value}`)[0]['checked'];
        switch (value) {
            case 'isPreTrip': this.assessment.isPreTrip = isChecked;
                if (this.assessmentCategory.isAllowAutoRiskManagement && this.assessment.isPreTrip) {
                    this.isAutomaticRisk = true;
                } else {
                    this.isAutomaticRisk = false;
                }
                break;
            case 'isCheckPoint': this.assessment.isCheckPoint = isChecked; break;
            case 'isCargoList': this.assessment.isCargoList = isChecked; break;
            case 'isPostTrip': this.assessment.isPostTrip = isChecked; break;
            case 'isDefaultInAnyJourney': this.assessment.isDefaultInAnyJourney = isChecked; break;
            case 'isAllowUploadingImage': this.assessment.isAllowUploadingImage = isChecked; break;
            default: {
                for (var index = 0; index < this.assessment.assessmentLocalizations.length; index++) {
                    // this.assessment.assessmentLocalizations[index].assessmentQuestions[assessmentQuestionIndex]['isRequired'] = isChecked;
                }
            } break;
        }
    }






    onSubmit() {
        if (this.validateObject()) {
            if (this.assessment.id > 0) this.UpdateRowData();
            else this.AddRowData();
        } else if (this.isAssessmentQuestionsNotMissing && this.isAssessmentQuestionsAnswersNotMissing) {
            Swal.fire('', 'please fill all mandatory fields', 'error');
        }
    }

    AddRowData() {
        this.assessmentService.Add(this.assessment).then((result: any) => {
            if (result.isSuccessful) {
                Swal.fire('Add Assessment', `Your assessment ${this.assessment.name} added successfully`, 'success');
                this.initList();
                this.closeModal();
            } else {
                Swal.fire(`Can't Add Assessment`, result.message, 'error');
            }
        }).catch((err) => {
            Swal.fire(`Can't Add Assessment`, err, 'error');
        });
    }

    UpdateRowData() {
        this.assessmentService.Update(this.assessment).then((data) => {
            if (data.isSuccessful) {
                Swal.fire('Update Assessment', `Your assessment ${this.assessment.name} updated successfully`, 'success');
                this.initList();
                this.closeModal();
            } else {
                Swal.fire(`Can't Update Assessment`, data.message, 'error');
            }

        }).catch((err) => {
            Swal.fire(`Can't Update Assessment`, err, 'error');
        });
    }

    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        const messageRes = !row_obj.isActive ? 'unblocked' : 'blocked';
        this.swalService.showConfirmMessage(message, `assessment ${row_obj.name}`).then(result => {
            if (result.value) {
                this.assessmentService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then((res: any) => {
                        if (res.isSuccessful) {
                            Swal.fire({
                                icon: 'success',
                                title: `${message}d successfully`,
                                text: `Assessment ${message}d Successfully`
                            })
                            this.initList();
                        } else {
                            Swal.fire({
                                icon: 'info',
                                title: `Sorry can't ${message}`,
                                text: res.message
                            });
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't ${message}`,
                            text: err
                        })
                    });
            }
        })
    }

    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Assessment ',
            text: `Are you sure to delete this ${row_obj.name} assessment?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.assessmentService.Delete(row_obj.rowId).then((deleteResult: any) => {
                    if (!deleteResult.isSuccessful) {
                        Swal.fire({
                            icon: 'info',
                            title: `Sorry can't delete`,
                            text: deleteResult.message
                        });
                    } else {
                        Swal.fire({
                            icon: 'success',
                            title: `Assessment Category`,
                            text: deleteResult.message
                        });
                        this.initList();
                    }
                }).catch((err) => {
                    Swal.fire({
                        icon: 'info',
                        title: `Sorry can't delete`,
                        text: err
                    });
                })
            }
        });
    }

    initList() {
        this.assessmentService.GetAll().pipe(map(res => res)).subscribe((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        this.assessmentLocalizations = [];
        if (this.insertLanguages != null && this.insertLanguages.length > 0) {
            for (var i = 0; i < this.insertLanguages.length; i++) {
                this.assessmentLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, name: '', languageIsDefault: this.insertLanguages[i].isDefault, assessmentQuestions: [] });
            }
        }
        var assessmentRisks: AssessmentRisk[] = [{ assessmentId: 0, riskStatus: Level.Low, startValue: null, endValue: null, isRange: false },
        { assessmentId: 0, riskStatus: Level.Medium, startValue: null, endValue: null, isRange: false },
        { assessmentId: 0, riskStatus: Level.High, startValue: null, endValue: null, isRange: false }];
        var newObject: Assessment = {
            id: 0, name: '', isActive: true, assessmentCategoryId: null, isAllowUploadingImage: false, isCheckPoint: false, isCargoList: false, isDefaultInAnyJourney: false, isPostTrip: false, isPreTrip: false,
            isValuesRelatedToYesOrChecked: true, oneNoPointEqual: 0, oneYesPointEqual: 1,
            assessmentLocalizations: this.assessmentLocalizations, assessmentRisks: assessmentRisks
        };
        return newObject;
    }

    validateObject() {
        this.isSubmitted = true;
        let isAssessmentLocalizationValid = true;
        this.isAssessmentQuestionsNotMissing = true;
        this.isAssessmentQuestionsAnswersNotMissing = true;
        for (var index = 0; index < this.assessment.assessmentLocalizations.length; index++) {

            if (this.assessment.assessmentLocalizations[index].name.trim() == '') {
                isAssessmentLocalizationValid = false;
                break;
            }
            if (this.assessment.assessmentLocalizations[index].assessmentQuestions.length > 0) {
                let assessmentQuestions = this.assessment.assessmentLocalizations[index].assessmentQuestions;
                for (var questionIndex = 0; questionIndex < assessmentQuestions.length; questionIndex++) {
                    if (assessmentQuestions[questionIndex].name.trim() == '' || (!assessmentQuestions[questionIndex].displayway && this.assessment.assessmentLocalizations[index].languageIsDefault)) {
                        isAssessmentLocalizationValid = false;
                        break;
                    }
                    else if (assessmentQuestions[questionIndex].assessmentQuestionAnswers.length == 0) {
                        isAssessmentLocalizationValid = false;
                        this.isAssessmentQuestionsAnswersNotMissing = false;
                        Swal.fire('Assessment Error', `Your're missing adding answers to your question(s) , Please add them`, 'error');
                        break;
                    }
                    else if (assessmentQuestions[questionIndex].assessmentQuestionAnswers.length > 0) {
                        assessmentQuestions[questionIndex].assessmentQuestionAnswers.forEach(answer => {
                            if (!answer.name) {
                                isAssessmentLocalizationValid = false;
                            }
                            if ((!answer.pointValue && answer.pointValue != 0) && this.assessment.assessmentLocalizations[index].languageIsDefault) {
                                isAssessmentLocalizationValid = false;
                            }
                        });

                    }
                }
            }
            else {
                this.isAssessmentQuestionsNotMissing = false;
                isAssessmentLocalizationValid = false;
                break;
            }
        }
        if (this.assessment.assessmentCategoryId == null || isAssessmentLocalizationValid == false) {
            if (this.isAssessmentQuestionsNotMissing == false) {
                Swal.fire('Assessment Error', `Your're missing adding questions to your assessment ${this.assessment.name}, Please add them`, 'error');
            }
            return false;
        }
        else return true;
    }

    validateRiskValues() {
        // let questionCount=this.assessment.assessmentLocalizations[0].assessmentQuestions.length;
        // for(var index=0; index<this.assessment.assessmentRisks.length;index++){
        //     if(this.assessment.assessmentRisks[index].startValue<0 || this.assessment.assessmentRisks[index].startValue>questionCount ) this.assessment.assessmentRisks[index].startValue='';
        //     if(this.assessment.assessmentRisks[index].endValue<0 || this.assessment.assessmentRisks[index].endValue>questionCount ) this.assessment.assessmentRisks[index].endValue='';
        // }
    }

    closeModal() {
        $('.close').click();
        this.assessment = this.initObject();
    }

    config = {
        backdrop: true,
        ignoreBackdropClick: true
    };
}
