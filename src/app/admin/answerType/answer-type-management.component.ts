import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { AnswerType, AnswerTypeLocalization } from '@jms/app/shared/Entities/admin/AnswerType';
import { AnswerTypeService } from '@jms/app/shared/Services/AnswerTypeService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { LanguageService } from '@jms/app/shared/Services/LanguageService';
import { Language } from '@jms/app/shared/Entities/admin/Language';
import { DisplayWay } from '@jms/app/shared/enums/display-way.enum';
import { SwalService } from '@jms/app/shared/Services';

@Component({
  selector: 'app-answer-type-management',
  templateUrl: './answer-type-management.component.html',
  styleUrls: ['./answer-type-management.component.css']
})
export class AnswerTypeManagementComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'isActive', 'action'];
  featureConst = constant;
  isSubmitted = false;
  answerTypeLocalizations: AnswerTypeLocalization[] = [];
  insertLanguages: Language[] = [];
  answerType: AnswerType = this.initObject();;
  answerTypeAddress: string = '';
  DisplayWays: any;
  defaultLanguageAnswers: any[] = [];

  dataSource = new MatTableDataSource<any>();
  doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
  validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;


  /**
   * 
   * @param dialog 
   * @param authService 
   * @param answerTypeService 
   * @param languageService 
   * @param swalService 
   */
  constructor(
    public dialog: MatDialog,
    private authService: AuthenticationService,
    private answerTypeService: AnswerTypeService,
    private languageService: LanguageService,
    private swalService: SwalService
  ) { }

  ngOnInit() {
    this.initList();
    this.languageService.GetBy(true).then((data: any) => {
      this.insertLanguages = data.data;
      this.answerType = this.initObject();
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  initModal() {
    this.answerType = this.initObject();
  }

  /**
   * update answer type
   * 
   * 
   * @param rowObject 
   */
  update(rowObject: any) {
    this.isSubmitted = false;
    this.answerTypeLocalizations = [];
    this.answerType = rowObject;
    if (this.insertLanguages != null && this.insertLanguages.length > 0) {
      for (var i = 0; i < this.insertLanguages.length; i++) {
        var answerTypeItems = [];
        var answerTypeLocalization = this.getLanguageByFind(rowObject.answerTypeLocalizations, this.insertLanguages[i].id);
        if (answerTypeLocalization != null) {
          answerTypeItems = answerTypeLocalization.answerTypeItems;
          this.answerTypeLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, languageIsDefault: this.insertLanguages[i].isDefault, answerTypeItems: answerTypeItems });
        } else {
          this.answerTypeLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, languageIsDefault: this.insertLanguages[i].isDefault, answerTypeItems: this.defaultLanguageAnswers });
        }
      }

      this.answerType.answerTypeLocalizations = this.answerTypeLocalizations;
    }
  }

  /**
   * 
   * @param list 
   * @param id 
   */
  getLanguageByFind(list: AnswerTypeLocalization[], id: number): any | null {
    if (list != null) {
      const language = list.find((language: { languageId: number; }) => language.languageId === id);
      if (language && language.languageIsDefault) {
        language.answerTypeItems.forEach((item, index) => {
          this.defaultLanguageAnswers.push({ id: 0, name: item.name, answerTypeId: this.answerType.id, point: 0, order: index + 1 });
        });
      }

      return language;
    }
  }

  /**
   * new answer
   * 
   * 
   */
  addAnswer() {
    for (var index = 0; index < this.answerType.answerTypeLocalizations.length; index++) {
      var answers = this.answerType.answerTypeLocalizations[index].answerTypeItems;
      if (answers != null && answers.length > 0) {
        let order = Math.max.apply(Math, answers.map(function (o) { return o.order; }));
        answers.push({ id: 0, name: '', answerTypeId: this.answerType.id, point: 0, order: order + 1 });
      }
      else {
        answers = [];
        answers.push({ id: 0, name: '', answerTypeId: this.answerType.id, point: 0, order: 0 });
      }
      this.answerType.answerTypeLocalizations[index].answerTypeItems = answers;
    }
  }

  /**
   * delete answer
   * 
   * 
   * @param answerItemIndex 
   */
  deleteAnswer(answerItemIndex) {
    if (answerItemIndex !== -1) {
      for (var index = 0; index < this.answerType.answerTypeLocalizations.length; index++) {
        this.answerType.answerTypeLocalizations[index].answerTypeItems.splice(answerItemIndex, 1);
      }
    }
  }

  onSubmit() {
    if (this.validateObject()) {
      if (this.answerType.id > 0) this.UpdateRowData();
      else this.AddRowData();
    }
  }

  AddRowData() {
    this.answerTypeService.Add(this.answerType).then((result: any) => {
      Swal.fire('Add Answer Type', `Your answerType ${this.answerType.name} added successfully`, 'success');
      this.initList();
      this.closeModal();
    });
  }

  UpdateRowData() {
    this.answerTypeService.Update(this.answerType).then((data) => {
      Swal.fire('Update Answer Type', `Your answer type ${this.answerType.name} updated successfully`, 'success');
      this.initList();
      this.closeModal();
    });
  }

  ActivateOrDeactivate(event: Event, rowObject: any) {
    event.preventDefault();
    event.stopPropagation();
    const message = !rowObject.isActive ? 'Activate' : 'Deactivate';
    this.swalService.showConfirmMessage(message, `answer type ${rowObject.name}`).then(result => {
      if (result.value) {
        this.answerTypeService.ActivateOrDeactivate(rowObject.rowId, !rowObject.isActive).
          then((res: any) => {
            if (res.isSuccessful) {
              Swal.fire({
                icon: 'success',
                title: `${message}d successfully`,
                text: `Answer Type ${message}d Successfully`
              })
              this.initList();
            } else {
              Swal.fire({
                icon: 'info',
                title: `Sorry can't ${message}`,
                text: res.message
              });
            }
          }).catch((err) => {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't ${message}`,
              text: err
            })
          });
      }
    })
  }

  Delete(rowObject: any) {
    Swal.fire({
      title: 'Delete Answer Type ',
      text: `Are you sure to delete this ${rowObject.name} answer type?`,
      showCancelButton: true,
      confirmButtonColor: '#17BC9B',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.answerTypeService.Delete(rowObject.rowId).then((res: any) => {
          if (res.isSuccessful) {
            Swal.fire({
              icon: 'success',
              title: `Answer type`,
              text: res.message
            });
            this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != rowObject.id; });
          } else {
            Swal.fire({
              icon: 'info',
              title: `Sorry can't delete`,
              text: res.message
            });
          }
        }).catch((err) => {
          Swal.fire({
            icon: 'info',
            title: `Sorry can't delete`,
            text: err
          });
        })
      }
    });
  }

  initList() {
    this.answerTypeService.GetAll().then((data: any) => {
      this.dataSource.data = data.data;
      this.table.renderRows();
    });
  }

  initObject() {
    this.isSubmitted = false;
    this.DisplayWays = [{ id: DisplayWay.RadioButton, name: 'Multichoice' },
    // { id: DisplayWay.SingleSelectDropList, name: 'Drop List' },
    // { id: DisplayWay.CheckedUncCheckedBox, name: 'Check Box' },
    // { id: DisplayWay.Text, name: 'Text' },
    // { id: DisplayWay.ImageUpload, name: 'Image Upload' }
  ];

    this.answerTypeLocalizations = [];
    if (this.insertLanguages != null && this.insertLanguages.length > 0) {
      for (var i = 0; i < this.insertLanguages.length; i++) {
        this.answerTypeLocalizations.push({ languageId: this.insertLanguages[i].id, languageName: this.insertLanguages[i].name, languageIsDefault: this.insertLanguages[i].isDefault, answerTypeItems: [] });
      }
    }
    var newObject: AnswerType = { id: 0, rowId: '', name: '', displayWay: null, isActive: true, answerTypeLocalizations: this.answerTypeLocalizations };
    return newObject;
  }

  validateObject() {
    this.isSubmitted = true;
    let isAnswerTypeLocalizationValid = true;
    if (this.answerType.displayWay != DisplayWay.CheckedUncCheckedBox && this.answerType.displayWay != DisplayWay.ImageUpload && this.answerType.displayWay != DisplayWay.Text) {
      for (var index = 0; index < this.answerType.answerTypeLocalizations.length; index++) {
        for (var itemIndex = 0; itemIndex < this.answerType.answerTypeLocalizations[index].answerTypeItems.length; itemIndex++) {
          let item = this.answerType.answerTypeLocalizations[index].answerTypeItems[itemIndex];
          if (item.name.trim() == '' || item.point < 0) {
            isAnswerTypeLocalizationValid = false;
            break;
          }
        }
      }
    }
    else {
      this.answerType.answerTypeLocalizations = [];
      if (this.answerType.displayWay == DisplayWay.CheckedUncCheckedBox) {
        if (this.answerType.checkPoint == null || this.answerType.checkPoint < 0 || this.answerType.unCheckPoint == null || this.answerType.unCheckPoint < 0)
          isAnswerTypeLocalizationValid = false;
      }
    }


    if (this.answerType.name.trim() == '' || this.answerType.displayWay == null || isAnswerTypeLocalizationValid == false) return false;
    return true;
  }

  closeModal() {
    $('.close').click();
    this.answerType = this.initObject();
  }
}
