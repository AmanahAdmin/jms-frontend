import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralSettingsService } from '@jms/app/shared/Services/general-settings.service';
import Swal from 'sweetalert2';
import { constant } from '@jms/app//shared/const/constant'
import { AuthenticationService } from '@jms/app/shared/Services';

interface ISettings {
  id?: number;
  distanceRestriction: number
  drivingHourLimit: number;
  isActive?: boolean;
  isCheckIn?: boolean;
  isSuccess?: boolean;
}

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss']
})
export class GeneralSettingsComponent implements OnInit {
  form: FormGroup;

  settings: ISettings;
  rowId: number;
  featureConst = constant;

  constructor(fb: FormBuilder,
    private readonly generalSettingsService: GeneralSettingsService, private authService: AuthenticationService) {
    this.form = fb.group({
      drivingHourLimit: ['', [Validators.required]],
      distanceRestriction: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    // previous settings
    this.getSettings();
  }

  validateFeature(featurename: string) {
    return this.authService.validateFeature(featurename)
  }

  /**
   * previous settings
   * 
   * 
   */
  getSettings() {
    this.generalSettingsService.list.subscribe((res: any) => {
      if (res.data) {
        this.settings = res.data;
        this.rowId = res.data.id;
        this.form.patchValue(this.settings);
      } else {
        this.setDefaultSettings();
      }
    });
  }

  /**
   * set settings for first time
   * 
   * 
   */
  setDefaultSettings() {
    this.generalSettingsService.add.subscribe(res => {
      if (res.data) {
        this.getSettings();
      }
    });
  }

  /**
   * update settings
   * 
   * 
   */
  updateSettings() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const body = this.form.value;

    body.id = this.rowId;
    body.drivingHourLimit = +body.drivingHourLimit;

    this.generalSettingsService.update(body).subscribe(res => {
      Swal.fire('', 'Settings updated successfully!', 'success');
    });
  }

  /**
   * check if form input has an error
   *
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'drivingHourLimit':
        if (this.form.get('drivingHourLimit').hasError('required')) {
          return `Driving Hour Limit required*`;
        }
        break;

      case 'distanceRestriction':
        if (this.form.get('distanceRestriction').hasError('required')) {
          return `Distance Restrictions required*`;
        }
        break;
    }
  }
}
