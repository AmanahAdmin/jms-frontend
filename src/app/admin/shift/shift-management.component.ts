import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Shift } from '@jms/app/shared/Entities/admin/Shift';
import { ShiftService } from '@jms/app/shared/Services/ShiftService';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';
import { format } from 'url';
import { formatDate, DatePipe } from '@angular/common';
import { SwalService } from '@jms/app/shared/Services';

@Component({
    selector: 'app-shift-management',
    templateUrl: './shift-management.component.html',
    styleUrls: ['./shift-management.component.css']
})
export class ShiftManagementComponent implements OnInit {
    displayedColumns: string[] = ['startTime', 'endTime', 'isNightDriving', 'isActive', 'action'];
    featureConst = constant;
    pipe = new DatePipe('en-US');
    isSubmitted = false;
    shift: Shift = this.initObject();
    public dataSource = new MatTableDataSource<any>();
    constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private shiftService: ShiftService,
        private swalService: SwalService
    ) { }
    ngOnInit() { this.initList(); }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => { this.dataSource.filter = value.trim().toLocaleLowerCase(); }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.shift = this.initObject(); }
    Update(row_obj: any) {
        this.isSubmitted = false;
        this.shift = row_obj;
        this.shift.fromTime = this.pipe.transform(new Date(row_obj.startTime), 'HH:mm');
        this.shift.toTime = this.pipe.transform(new Date(row_obj.endTime), 'HH:mm');
    }

    onSubmit() {
        if (this.validateObject()) {
            if (this.shift.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.shiftService.Add(this.shift).then((result: any) => {
            Swal.fire('Add Shift', `Your shift added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        this.shiftService.Update(this.shift).then((data) => {
            Swal.fire('Update Shift', `Your shift updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    ActivateOrDeactivate(event: Event, row_obj: any) {
        event.preventDefault();
        event.stopPropagation();
        const message = !row_obj.isActive ? 'Activate' : 'Deactivate';
        this.swalService.showConfirmMessage(message, `shift`).then(result => {
            if (result.value) {
                this.shiftService.ActivateOrDeactivate(row_obj.rowId, !row_obj.isActive).
                    then(() => { this.initList(); });
            }
        })
    }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Shift',
            text: `Are you sure to delete this shift?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                this.shiftService.Delete(row_obj.rowId).then(() => {
                    this.dataSource.data = this.dataSource.data.filter((value, key) => { return value.id != row_obj.id; });
                });
            }
        });
    }
    initList() {
        this.shiftService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Shift = { id: 0, rowId: '', startTime: null, endTime: null, fromTime: null, toTime: null, isActive: true, isNightDriving: false };
        return newObject;
    }
    validateObject() {
        console.log(this.shift);
        this.shift.startTime = new Date().toDateString() + ' ' + this.shift.fromTime;
        this.shift.endTime = new Date().toDateString() + ' ' + this.shift.toTime;
        console.log(this.shift);
        this.isSubmitted = true;
        if (this.shift.startTime == null || this.shift.endTime == null || this.shift.endTime <= this.shift.startTime) return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.shift = this.initObject();
    }
}