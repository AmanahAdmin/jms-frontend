import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatTable, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Workforce } from '@jms/app/shared/Entities/admin/Workforce';
import { WorkforceService } from '@jms/app/shared/Services/WorkforceService';
import { User } from '@jms/app/shared/models/UserModel';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { constant } from '@jms/app/shared/const/constant';

@Component({
    selector: 'app-workforce-management',
    templateUrl: './workforce-management.component.html',
    styleUrls: ['./workforce-management.component.css']
})
export class WorkforceManagementComponent implements OnInit {
    displayedColumns: string[] = ['name', 'userCount', 'isActive', 'action'];
    featureConst = constant;
    isSubmitted: boolean = false;
    name: string = '';
    workforce: Workforce;
    workforceUsers: User[] = null;
    workforceName: string = '';
    public dataSource = new MatTableDataSource<any>();
    constructor(public dialog: MatDialog, private authService: AuthenticationService, private workforceService: WorkforceService) { }
    ngOnInit() {
        this.workforce = this.initObject();
        this.initList();
    }
    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }
    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    public validateFeature(featurename: string) { return this.authService.validateFeature(featurename) }
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    initModal() { this.isSubmitted = false; this.workforce = this.initObject(); }
    Update(row_obj: any) { this.isSubmitted = false; this.workforce = row_obj; }
    BrowseUser(row_obj: any) {
        this.workforceName = row_obj.name;
        this.workforceUsers = row_obj.users;
    }

    onSubmit() {
        if (this.validateObject()) {
            if (this.workforce.id > 0) this.UpdateRowData();
            else this.AddRowData();
        }
    }
    AddRowData() {
        this.workforceService.Add(this.workforce).then((result: any) => {
            Swal.fire('Add Workforce', `Your workforce ${this.workforce.name} added successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    UpdateRowData() {
        this.workforceService.Update(this.workforce).then((data) => {
            Swal.fire('Update Workforce', `Your workforce ${this.workforce.name} updated successfully`, 'success');
            this.initList();
            this.closeModal();
        });
    }
    ActivateOrDeactivate(row_obj: any) { this.workforceService.ActivateOrDeactivate(row_obj.rowId, row_obj.isActive).then(() => { this.initList(); }); }
    Delete(row_obj: any) {
        Swal.fire({
            title: 'Delete Workforce ',
            text: `Are you sure to delete this ${row_obj.name} workforce?`,
            showCancelButton: true,
            confirmButtonColor: '#17BC9B',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                console.log(row_obj.rowId);
                this.workforceService.Delete(row_obj.rowId).then(() => {
                    this.dataSource.data = this.dataSource.data.filter((value, key) => {
                        return value.id != row_obj.id;
                    });
                });
            }
        });
    }
    initList() {
        this.workforceService.GetAll().then((data: any) => {
            this.dataSource.data = data.data;
            this.table.renderRows();
        });
    }

    initObject() {
        this.isSubmitted = false;
        var newObject: Workforce = { id: 0, rowId: '', name: '', description: '', isHavingActiveJourney: false, isActive: true, userCount: 0, users: null };
        return newObject;
    }
    validateObject() {
        this.isSubmitted = true;
        if (this.workforce.name == null || this.workforce.name.trim() == '') return false;
        else return true;
    }
    closeModal() {
        $('.close').click();
        this.workforce = this.initObject();
    }
}