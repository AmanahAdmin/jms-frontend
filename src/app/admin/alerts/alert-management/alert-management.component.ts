import { element } from "protractor";
import { Component, OnInit, OnDestroy, ViewChild, Input } from "@angular/core";
import { constant } from "@jms/app/shared/const/constant";
import {
  MatDialog,
  MatTable,
  MatSort,
  MatTableDataSource,
  MatPaginator,
} from "@angular/material";
import { AuthenticationService } from "@jms/app/shared/Services/AuthenticationService";
import { AlertServiceService } from "@jms/app/shared/Services/alert-service.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { AlertTypesService } from "@jms/app/shared/Services/alert-types.service";
import { AlertTypes } from "@jms/app/shared/Entities/alertType";
import { Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { Alert } from "@jms/app/shared/Entities/alerts";
import { CommonService } from "@jms/app/shared/Services/CommonService";
import Swal from "sweetalert2";
import { _fixedSizeVirtualScrollStrategyFactory } from "@angular/cdk/scrolling";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-alert-management",
  templateUrl: "./alert-management.component.html",
  styleUrls: ["./alert-management.component.css"],
})
export class AlertManagementComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    "events",
    "alertName",
    "notifyafter",
    "notifyuserroles",
    "actions",
  ];
  groupdisplayedColumns: string[] = [
    "SubalertName",
    "Subnotifyafter",
    "Subnotifyuserroles",
    "Subremove",
    "Subactions",
  ];
  featureConst = constant;
  Roles = [];
  features: any;
  alerts: Alert[] = [];
  dropdownSettings: IDropdownSettings;
  selectMitues = false;
  selectHours = true;
  groubs: Alert[] = [];
  selectedElement = [];
  expandedElement: Alert | null;
  isInReturen = true;
  isnewAlert = false;
  //Duration
  mintues = [
    "5",
    "10",
    "15",
    "20",
    "25",
    "30",
    "35",
    "40",
    "45",
    "50",
    "55",
    "60",
  ];
  hours = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
    "24",
  ];
  //endDuration

  public dataSource = new MatTableDataSource<any>();
  subs: Subscription = new Subscription();
  modelalerts: Alert[] = [];

  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertServiceService,
    private commonService: CommonService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  ngOnInit() {
    this.initMultiDropList();
    this.authService.GetLookupRoles().then((data: any) => {
      this.Roles = data.data;
    });
    this.getAlertFeatures();

    this.getAlerts();
  }
  inializeAlerts() {
    this.dataSource.data.push(this.inializeObject());
    this.table.renderRows();
  }
  inializeObject() {
    let obj = new Alert();
    obj.id = 0;
    obj.eventName = "";
    obj.alertDuration = 0;
    obj.after = 0;
    obj.name = "";
    obj.usersToNotify = [];
    obj.isSubmitted = false;
    obj.isMain = true;
    obj.groubs = [];
    obj.isnewAlert = true;
    obj.isReturnedFromDb = false;
    obj.ismiutesSelected = false;
    obj.isHoursSelected = true;
    return obj;
  }
  AddNew(element) {
    // element.groups.push(['hh' ,'uuuuuu']);
    let obj = this.inializeObject();

    obj.isMain = false;
    if (element.groubs && element.groubs.length != 0) {
      // Object.assign(element,{'groubs':  element.alerts} );
      element.groubs.push(obj);
    } else {
      this.groubs = [];
      this.groubs.push(obj);
      Object.assign(element, { groubs: this.groubs });
    }

    this.table.renderRows();
  }
  addNewAlert() {
    this.groubs = [];
    let obj = this.inializeObject();
    const objec = {
      after: obj.after,
      alertDuration: obj.alertDuration,
      eventName: obj.eventName,
      id: obj.id,
      isMain: obj.isMain,
      name: obj.name,
      usersToNotify: obj.usersToNotify,
      groubs: obj.groubs,
      ismiutesSelected: false,
      isHoursSelected: true,
      isnewAlert: true,
      isReturnedFromDb: false,
    };
    this.dataSource.data = [...this.dataSource.data, objec];
    this.table.renderRows();
  }

  removeFromDb(ele, index) {
    if (ele.id != 0) {
      this.alertService
        .Delete(ele.id)
        .pipe(map((res) => res))
        .subscribe((del) => {
          if (!ele.isMain) {
            Swal.fire("Delete Alert", `Alert deleted successfully`, "success");
            // this.onReload();
            this.getAlerts();
          }
        });
    } else {
      if (ele.groubs.length != 0) {
        ele.groubs.splice(index, 1);
      }
      if (ele.groubs.length == 0) {
        this.dataSource.data.filter((data) => {
          if (data.groubs != undefined) {
            data.groubs.map((groubs) => {
              const index: number = data.groubs.indexOf(ele);
              if (index !== -1) {
                data.groubs.splice(index, 1);
              }
            });
          }
        });
      }
    }
  }
  onReload() {
    //   this.router.navigateByUrl('/admin/alert', { skipLocationChange: true }).then(() => {
    //     this.router.navigate(['/admin/alert']);
    // });
    window.location.reload();
  }
  getAlerts() {
    this.subs.add(
      this.alertService.GetAll().subscribe((alerts: any) => {
        if (alerts.length == 0) {
          this.dataSource.data = [];
          //this.dataSource.data.push(this.inializeObject());
          this.table.renderRows();
        }

        if (alerts.length != 0) {
          alerts.map((aa) => {
            Object.assign(aa, { groubs: aa.alerts });
            aa.alerts.map((alert) => {
              alert.isMain = false;
              alert.isReturnedFromDb = true;
              alert.isnewAlert = false;
              if (alert.alertDuration == 0) {
                alert.isHoursSelected = true;
              }
              if (alert.alertDuration == 1) {
                alert.ismiutesSelected = true;
              }
            });
            this.alerts = alerts;
          });
          this.dataSource.data = this.alerts;
          this.table.renderRows();
        }
      })
    );
  }
  ngAfterViewInit() {
    // this.inializeObject();
  }
  initMultiDropList() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: "id",
      textField: "name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true,
      clearSearchFilter: true,
      closeDropDownOnSelection: true,
    };
  }

  /**
   * add new alerts and update alerts with save
   */
  saveAlert() {
    this.modelalerts = [];
    let isValid = true;
    let isIdenticalAlert = false;
    this.dataSource.data.forEach((event) => {
      event.isSubmitted = true;
      if (!event.alerts) {
        this.modelalerts.push(event);
        if (!this.validateObject(event)) {
          isValid = false;
        }
        if (event.groubs.length != 0) {
          event.groubs.forEach((groub) => {
            groub.eventName = event.eventName;
            this.modelalerts.push(groub);
            if (!this.validateObject(groub)) {
              isValid = false;
            }
          });
        }
      } else {
        event.alerts.forEach((alert) => {
          alert.eventName = event.eventName;
          this.modelalerts.push(alert);
          if (!this.validateObject(alert)) {
            isValid = false;
          }
        });
      }
    });
    let j = 1;
    for (let index = 0; index < this.modelalerts.length - 1; index++) {
      const element = this.modelalerts[index];
      const secondElement = this.modelalerts[j];
      if (
        element.eventName == secondElement.eventName &&
        element.name == secondElement.name &&
        element.after == secondElement.after &&
        element.alertDuration == secondElement.alertDuration
      ) {
        const results = element.usersToNotify.filter((user) =>
          secondElement.usersToNotify.some((user_s) => user_s.id !== user.id)
        );

        if (results.length == 0) {
          isIdenticalAlert = true;
        } else {
          isIdenticalAlert = false;
        }
      }
      j++;
    }
    if (isValid && !isIdenticalAlert) {
      this.AddRowData(this.modelalerts);
    } else if (isIdenticalAlert) {
      Swal.fire("", `can't add duplicate events`, "error");
      return;
    } else {
      return;
    }
  }
  selecGrouptDuration(sender, group) {
    const selected = sender.target.value;
    if (selected == "0") {
      group.isHoursSelected = true;
      group.ismiutesSelected = false;
    }
    if (selected == "1") {
      group.ismiutesSelected = true;
      group.isHoursSelected = false;
    }
  }
  AddRowData(model: Alert[]) {
    this.subs.add(
      this.alertService.Add(model).subscribe(
        (al) => {
          Swal.fire("Save Alert", `Alert Saved successfully`, "success");
          this.getAlerts();
        },
        (error) => {
          Swal.fire(`can't save`, error, "error");
        }
      )
    );
  }
  UpdateRowData(model: any) {
    if (!this.validateObject(model)) {
      return;
    } else {
      this.subs.add(
        this.alertService.Update(model.id, model).subscribe(
          (al) => {
            Swal.fire("Update Alert", `Alert Updated successfully`, "success");
            this.getAlerts();
          },
          (err) => {
            Swal.fire(`can't update`, err, "error");
          }
        )
      );
    }
  }
  getAlertFeatures() {
    this.subs.add(
      this.commonService
        .GetFeatures()
        .pipe(map((res) => res))
        .subscribe((features) => {
          this.features = features.data;
          // this.features= this.features .filter(x => x.isAppearInAlert == true);

          this.features = this.features.filter(
            (feature) => feature.isAppearInAlert
          );
        })
    );
  }

  validateObject(alertObj: Alert) {
    alertObj.isSubmitted = true;
    if (
      alertObj.after == null ||
      alertObj.after == 0 ||
      alertObj.usersToNotify.length <= 0 ||
      alertObj.eventName.trim() == "" ||
      alertObj.eventName.trim() == null ||
      alertObj.name.trim() == "" ||
      alertObj.name == null
    ) {
      return false;
    } else return true;
  }
  // start select Duration
  selectDuration(sender, ele: any) {
    const selected = sender.target.value;
    if (selected == "0") {
      ele.isHoursSelected = true;
      ele.ismiutesSelected = false;
    }
    if (selected == "1") {
      ele.ismiutesSelected = true;
      ele.isHoursSelected = false;
    }
  }
  //end select duration
  // Start select Events for groubs

  onItemDeSelectGroup(item: any, element: any, i) {
    element.usersToNotify = element.usersToNotify.filter(function (roles) {
      return roles.roleId != item.id;
    });
  }

  onSelectAllGroup(items: any, element: any, i) {
    element.usersToNotify = [];
    for (var index = 0; index < items.length; index++) {
      element.usersToNotify.push(items[index]);
    }
  }
  onItemSelectGroup(item: any, element: any, i) {
    element.usersToNotify.push(item);
    const index: number = element.usersToNotify.indexOf(item);
    if (index !== -1) {
      element.usersToNotify.splice(index, 1);
    }
  }

  onItemDeSelectAllGroup(item: any, element: any, i) {
    element.usersToNotify = [];
  }
  // end select Events in groups

  // Start select Events

  onItemDeSelect(item: any, element: any) {
    element.usersToNotify = element.usersToNotify.filter(function (roles) {
      return roles.roleId != item.id;
    });
  }

  onSelectAll(items: any, element: any) {
    element.usersToNotify = [];
    for (var index = 0; index < items.length; index++) {
      element.usersToNotify.push(items[index]);
    }
  }
  onItemSelect(item: any, element: any) {
    element.usersToNotify.push(item);
    const index: number = element.usersToNotify.indexOf(item);
    if (index !== -1) {
      element.usersToNotify.splice(index, 1);
    }
  }

  onItemDeSelectAll(item: any, element: any) {
    element.usersToNotify = [];
  }
  // end select Events

  public validateFeature(featurename: string) {
    return this.authService.validateFeature(featurename);
  }
  removeUnSavedAlert(alert) {
    const index = this.dataSource.data.indexOf(alert);
    if (!alert.groubs || alert.groubs.length == 0) {
      this.dataSource.data.splice(index, 1);
      this.table.renderRows();
    } else {
      this.removeFromDb(alert, 0)
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
