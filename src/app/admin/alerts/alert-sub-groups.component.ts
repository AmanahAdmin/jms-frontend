import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Alert } from '@jms/app/shared/Entities/alerts';
import { constant } from '@jms/app/shared/const/constant';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { MatTableDataSource, MatTable } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alert-sub-groups',
  templateUrl: './alert-sub-groups.component.html',
  styleUrls: ['./alert-sub-groups.component.css']
})
export class AlertSubGroupsComponent implements OnInit {
  @Input() groubs: Alert[] = [];
  featureConst = constant;
  Roles = [];
  features: any;
  alerts: Alert[] = [];
  dropdownSettings: IDropdownSettings;
  selectMitues = false;
  selectHours = true;

  //Duration
  mintues = ['Never', '5', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55', '60'];
  hours = ['Never', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];
  //endDuration
  public dataSource = new MatTableDataSource<any>();
  subs: Subscription = new Subscription();


  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  groupdisplayedColumns: string[] = ['SubGroupEvent', 'SubalertName', 'Subnotifyafter', 'Subnotifyuserroles', 'Subremove', 'Subactions'];
  constructor() { }

  ngOnInit() {
  }
  selectDuration(sender) {
    const selected = sender.target.value;
    if (selected == "0") {

      this.selectHours = true;
      this.selectMitues = false;
    }
    if (selected == "1") {

      this.selectMitues = true;
      this.selectHours = false;
    }

  }
  initMultiDropList() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      clearSearchFilter: true,
      closeDropDownOnSelection: true,
    };
  }
  onSelectAll(items: any, element: any) {
    element.usersToNotify = [];
    for (var index = 0; index < items.length; index++) {
      element.usersToNotify.push({ roleId: items[index].id, order: index + 1 });
    }
  }
  onItemSelect(item: any, element: any) {
    let maxOrder = element.usersToNotify.length > 0 ? Math.max.apply(Math, element.usersToNotify.map(function (o) { return o.order; })) : 0;
    element.usersToNotify.push({ roleId: item.id, order: maxOrder + 1 });
  }
  onItemDeSelectAll(item: any, element: any) {
    element.usersToNotify = [];
  }
  onItemDeSelect(item: any, element: any) {

    element.usersToNotify = element.usersToNotify.filter(function (roles) {
      return roles.roleId != item.id;
    });
  }
  remove(ele) {
    const index: number = this.dataSource.data.indexOf(ele);
    if (index !== -1) {
      this.dataSource.data.splice(index, 1)
      this.table.renderRows();
    }
  }
  AddNew(element) {
    // element.groups.push(['hh' ,'uuuuuu']);
    let obj = this.inializeObject();
    this.groubs.push(obj);
    Object.assign(element, { 'groubs': this.groubs });
    this.table.renderRows();
    console.log('element', element);
  }
  inializeObject() {
    let obj = new Alert();
    obj.id = 0;
    obj.eventName = "";
    obj.alertDuration = 0;
    obj.after = 0;
    obj.name = "";
    obj.usersToNotify = [];
    obj.isSubmitted = false;
    obj.isMain = false;
    return obj;

  }
}
