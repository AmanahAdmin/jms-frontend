import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@jms/app/shared/Helpers/auth.guard';
import { AlertManagementComponent } from './alert-management/alert-management.component';


const routes: Routes = [
  { path: '', component: AlertManagementComponent, canActivate: [AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlertRoutingModule { }
