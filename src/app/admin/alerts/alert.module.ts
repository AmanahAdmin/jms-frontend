import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertRoutingModule } from './alert-routing.module';
import { AlertManagementComponent } from './alert-management/alert-management.component';
import { MatTableModule } from '@angular/material/table';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AlertSubGroupsComponent } from './alert-sub-groups.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [AlertManagementComponent, AlertSubGroupsComponent],
  exports:[AlertManagementComponent],
  imports: [
    CommonModule,
    AlertRoutingModule,
    FormsModule,
    MatTableModule,
    NgMultiSelectDropDownModule
  ]
})
export class AlertModule { }
