import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../shared/Services/AuthenticationService';
import swal from "sweetalert2";
import { Router } from '@angular/router';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  submitted = false;
  loginForm: FormGroup;
  loading: boolean = false;
  constructor(
    private formBuilder: FormBuilder, private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  get f() {
    return this.loginForm.controls;
  }

  async onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    swal.fire({
      title: 'Are you sure to reset your password?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reset it!'
    }).then(async (res) => {
      if (res.value) {
        this.loading = true;
        var result = await this.authenticationService.forgetPassword(this.f.username.value);
        this.loading = false;
        switch (result.status) {
          case 1:
            swal.fire("", "Please check your email for reset password instructions", "success");
            this.router.navigate(['/login']);
            break;
          case 6:
            swal.fire("", "Email incorrect", "error");
            break;
          case 0:
            swal.fire("", result.message, "error");
          default:
            console.log("system error")
            break;
        }
      }
    })


  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
    });
  }

} 
