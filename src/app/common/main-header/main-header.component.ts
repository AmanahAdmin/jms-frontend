import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/Services/AuthenticationService';
import { User } from '../../shared/models/UserModel';
import { Subscription, Observable, timer } from 'rxjs';
import { NotificationModel } from '../../shared/models/NotificationModel';
import { NotificationService } from '../../shared/Services/NotificationService';
import * as M from 'materialize-css';
import { Language } from '../../shared/Entities/admin/Language';
import { LanguageService } from '@jms/app/shared/Services/LanguageService';
import { UserRoles } from '@jms/app/shared/enums/user-roles.enum';
import { environment } from '../../../environments/environment';
declare var $: any;

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit, AfterViewInit {
  BrowsingLanguages: Language[];
  browsingLanguage: Language;
  browsingLanguageId: any;
  userRoleEnum = UserRoles;

  currentUser: User;
  componentName: string;
  timer: Observable<any>;
  Notifications: NotificationModel[] = [];
  subscription: Subscription;
  sideNavElement = "slideout";

  constructor(
    private NotificationService: NotificationService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private languageService: LanguageService) {
    this.authenticationService.currentUser.subscribe(x => {
      this.currentUser = x;
      if (this.currentUser) {
        let isDriver = this.currentUser.roles.filter(function (e) { return e.userRoleType == UserRoles.Driver; }) != null;
        this.languageService.GetBy(isDriver).then((data: any) => {
          this.BrowsingLanguages = data.data;
          if (this.BrowsingLanguages != null) {
            this.browsingLanguageId = this.currentUser.browsingLanguageId != null ? this.currentUser.browsingLanguageId : this.BrowsingLanguages[0].id;
            const index: number = this.BrowsingLanguages.findIndex(e => e.id == this.browsingLanguageId)
            if (index !== -1) this.browsingLanguage = this.BrowsingLanguages[index];
          }
        });
        if (this.currentUser.accessNumberOnTheSystem <= 1) {
          $(`.browseLanguage`).click();
          this.authenticationService.UpdateNumberOfBrowsing();
        }
      }
    });
  }

  ngOnInit() {
    if (this.currentUser) {
      this.NotificationService.notifications.subscribe((data: any) => {
        this.Notifications = data.data;
        // this.setTimer();
      });
    }
  }

  ngAfterViewInit(): void {
    setTimeout(function () {
      var elem = document.querySelector('.sidenav');
      var instance = M.Sidenav.init(elem, {});
    }, 0);
  }

  setTimer() {
    this.timer = timer(60000);
    this.subscription = this.timer.subscribe(() => {
      this.NotificationService.notifications.toPromise().then((data: any) => {
        this.Notifications = data.data;
        this.setTimer();
      });
    });
  }

  navigateToNotifications() {
    this.router.navigate(['/notifications']);
  }

  createImgPath = (serverPath: string) => {
    return `${environment.JMSURL}/${serverPath}`;
  }

  logout() {
    this.authenticationService.logout();
    $(".sidenav-overlay").click();

    this.router.navigateByUrl('/login', { skipLocationChange: false }).then(() => {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.navigate([this.router.url]);
    });
  }

  changeBrowsingLanguage() {
    if (this.BrowsingLanguages != null) {
      const index: number = this.BrowsingLanguages.findIndex(e => e.id == this.browsingLanguageId);
      if (index !== -1) this.browsingLanguage = this.BrowsingLanguages[index];
      this.authenticationService.UpdateUserDefaultLanguage(this.currentUser.rowId, this.browsingLanguageId);
    }
    $("#bntClose").click();
  }

  openProfile() {
    const isAdmin = this.currentUser.roles.find(role => role.userRoleType === UserRoles.Admin);
    if (isAdmin) {
      this.router.navigate(['/admin/profile'])
    } else {
      this.router.navigate(['/manager/profile'])
    }
  }

}
