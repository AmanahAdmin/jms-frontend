import { Component, OnInit } from '@angular/core';
import { Subscription, Observable, timer } from 'rxjs';
import { NotificationModel } from '../../shared/models/NotificationModel';
import { NotificationService } from '../../shared/Services/NotificationService';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})

export class NotificationsComponent implements OnInit {
  timer: Observable<any>;
  Notifications: NotificationModel[];
  subscription: Subscription;
  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.notifications.subscribe((data: any) => {
      this.Notifications = data.data;
      // this.setTimer();
    });
  }

  setTimer() {
    this.timer = timer(60000 * 5);
    this.subscription = this.timer.subscribe(() => {
      this.notificationService.notifications.subscribe((data: any) => {
        this.Notifications = data.data;
        this.setTimer();
      });
    });
  }
}
