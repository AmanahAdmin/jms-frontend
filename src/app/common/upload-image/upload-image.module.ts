import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadImageRoutingModule } from './upload-image-routing.module';
import { ChangeprofileImageComponent } from './changeprofile-image/changeprofile-image.component';
import { MatButtonModule, MatDialogModule } from '@angular/material';


@NgModule({
  declarations: [ChangeprofileImageComponent],
  imports: [
    CommonModule,
    UploadImageRoutingModule,
    MatDialogModule,
    MatButtonModule
  ],
  exports: [ChangeprofileImageComponent],
  entryComponents: [ChangeprofileImageComponent]
})
export class UploadImageModule { }
