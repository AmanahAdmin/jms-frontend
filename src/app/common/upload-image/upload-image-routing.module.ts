import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangeprofileImageComponent } from './changeprofile-image/changeprofile-image.component';


const routes: Routes = [
  {
    path: '',
    component: ChangeprofileImageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadImageRoutingModule { }
