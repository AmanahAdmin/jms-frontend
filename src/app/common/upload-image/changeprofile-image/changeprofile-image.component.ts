import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from "@angular/core";
import { HttpClient, HttpEventType } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { Subscription } from "rxjs";
import { UserService } from "@jms/app/shared/Services/UserService";
import { map } from "rxjs/operators";
import { User } from "@jms/app/shared/models/UserModel";
import { AuthenticationService } from "@jms/app/shared/Services/AuthenticationService";
import Swal from "sweetalert2";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "app-changeprofile-image",
  templateUrl: "./changeprofile-image.component.html",
  styleUrls: ["./changeprofile-image.component.css"],
})
export class ChangeprofileImageComponent implements OnInit, OnDestroy {
  public progress: number;
  public message: string;
  isCanSubmit: boolean;
  model: any = {};
  imgURL: string;
  userId: string;
  currentUser: User;
  subs: Subscription = new Subscription();
  @Output() public onUploadFinished = new EventEmitter();
  @Output() public onSaveImage = new EventEmitter<string>();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<ChangeprofileImageComponent>
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.userId = this.currentUser.id;
    dialogRef.disableClose = true;
  }

  ngOnInit() {}

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];

    let pattern = /image-*/;
    if (!fileToUpload.type.match(pattern)) {
      Swal.fire("", "Invalid image format", "error");
      return;
    }

    this.progress = 0;
    const formData = new FormData();
    formData.append("file", fileToUpload, fileToUpload.name);

    this.model.userId = this.userId;
    this.model.imagePath = fileToUpload.name;
    this.imgURL = "Uploads" + "/" + fileToUpload.name;
    this.http
      .post(`${environment.JMSApiURL}/Upload`, formData, {
        reportProgress: true,
        observe: "events",
      })
      .subscribe((event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round((100 * event.loaded) / event.total);
        } else if (event.type === HttpEventType.Response) {
          this.message = "Upload success.";
          this.isCanSubmit = true;
          this.onUploadFinished.emit(event.body);
        }
      });
  };
  createImgPath = (serverPath: string) => {
    return `${environment.JMSURL}/${serverPath}`;
  };
  changeImage() {
    this.subs.add(
      this.userService.changeProfileImage(this.model).subscribe(
        (profile) => {
          if (profile.status == 1) {
            this.currentUser.image = profile.data.image;
            this.onSaveImage.emit(profile.data.image);
            this.isCanSubmit = false;
            localStorage.setItem(
              "currentUser",
              JSON.stringify(this.currentUser)
            );
            Swal.fire(
              "Profile image Edit",
              `your Profile image changed successfully`,
              "success"
            );
          }
        },
        (err) => {
          this.isCanSubmit = false;
        }
      )
    );
  }
  close() {
    this.dialogRef.close();
  }
  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
