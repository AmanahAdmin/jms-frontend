import { Component, OnInit, ViewChild } from '@angular/core';
import { IntiateJourneyService } from '@jms/app/shared/Services/initiate-journey.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { journeyStatusArray } from '@jms/app/shared/enums/journey-status.enum';
import { MatOption } from '@angular/material';
import Swal from 'sweetalert2';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;

interface Department {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface VehicleType {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface ThirdParty {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface Location {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface Zone {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface JourneyCreator {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface JourneyManager {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface RequestType {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface Driver {
  id: number;
  name: string;
  isDisabled: boolean;
}

interface Data {
  departments: Department[];
  vehicleTypes: VehicleType[];
  thirdPartys: ThirdParty[];
  locations: Location[];
  zones: Zone[];
  journeyCreators: JourneyCreator[];
  journeyManagers: JourneyManager[];
  requestTypes: RequestType[];
  drivers: Driver[];
  fromMapLocations: string[];
}

interface Body {
  creationDateFrom: Date;
  creationDateTo: Date;
  deliveryDateFrom: Date;
  deliveryDateTo: Date;
  journeyId: number;
  driversIds: number[];
  departmentsIds: number[];
  vehiclesIds: number[];
  vehicleTypesIds: number[];
  thirdPartysIds: number[];
  requestTypesIds: number[];
  zonesIds: number[];
  locationsIds: number[];
  fromMapLocations: string[];
  creatorsIds: number[];
  managersIds: number[];
  status: number[];
  showMergedJourneys: boolean;
  page: number;
  pageSize: number;
}

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  form: FormGroup;
  data: Data;
  status: any = journeyStatusArray();
  vehiclesNoList: any[] = [];

  @ViewChild('selectedDepartments', { static: false }) private selectedDepartments: MatOption;
  @ViewChild('selectedCreators', { static: false }) private selectedCreators: MatOption;
  @ViewChild('selectedManagers', { static: false }) private selectedManagers: MatOption;
  @ViewChild('selectedDrivers', { static: false }) private selectedDrivers: MatOption;
  @ViewChild('selectedThridParties', { static: false }) private selectedThridParties: MatOption;
  @ViewChild('selectedVehicleTypes', { static: false }) private selectedVehicleTypes: MatOption;
  @ViewChild('selectedLocationsAndZones', { static: false }) private selectedLocationsAndZones: MatOption;
  @ViewChild('selectedRequestTypes', { static: false }) private selectedRequestTypes: MatOption;
  @ViewChild('selectedStatus', { static: false }) private selectedStatus: MatOption;
  @ViewChild('selectedVehiclesIds', { static: false }) private selectedVehiclesIds: MatOption;
  @ViewChild(MatAccordion, { static: false }) accordion: MatAccordion;

  displayedColumns: string[] = ['position', 'creationDateTime', 'weight', 'symbol'];

  pageIndex: number = 1;
  pageSize: number = 10;
  length: number = 0;

  departmentsIds: number[] = [];
  thirdpartysIds: number[] = [];
  vehiclesTypesIds: number[] = [];

  journeys: any[] = [];
  noResult: boolean = false;
  expanded: boolean = false;

  /**
   * 
   * @param intiateJourneyService 
   * @param fb 
   */
  constructor(
    private intiateJourneyService: IntiateJourneyService,
    fb: FormBuilder) {
    this.form = fb.group({
      creationDateFrom: [new Date(), [Validators.required]],
      creationDateTo: [new Date(), [Validators.required]],
      deliveryDateFrom: [null],
      deliveryDateTo: [null],
      journeyId: [null],
      driversIds: [[]],
      departmentsIds: [[]],
      vehiclesIds: [[]],
      vehicleTypesIds: [[]],
      thirdPartysIds: [[]],
      locationsZones: [[]],
      requestTypesIds: [[]],
      managersIds: [[]],
      creatorsIds: [[]],
      status: [[]],
      showMergedJourneys: [true]
    });
  }

  ngOnInit() {
    this.lookups();
  }

  /**
   * journey lookups
   * 
   * 
   */
  lookups() {
    this.intiateJourneyService.reportLookUps.subscribe(res => {
      this.data = res.data;
      this.data.fromMapLocations = this.data.fromMapLocations.filter(location => {
        return location.trim().length > 0;
      });
    });
  }

  /**
   * search in journeys
   * 
   * 
   */
  search() {
    this.expanded = false;

    if (this.form.invalid) {
      return Swal.fire('Validation Error', 'Required Creation Date From / To', 'error');
    }

    this.noResult = false;
    this.accordion.closeAll();

    let body: Body = this.form.value;

    body.page = this.pageIndex;
    body.pageSize = this.pageSize;

    body.zonesIds = [];
    body.locationsIds = [];
    body.fromMapLocations = [];

    if (body['locationsZones'] && body['locationsZones'].length > 0) {
      body['locationsZones'].forEach(element => {
        if (element && element.trim().startsWith("location")) {
          body.locationsIds.push(element.replace(/[^0-9]/g, ''));
        } else if (element && element.trim().startsWith("zone")) {
          body.zonesIds.push(element.replace(/[^0-9]/g, ''));
        } else {
          body.fromMapLocations.push(element);
        }
      });
    }

    delete body['locationsZones'];
    this.intiateJourneyService.jurneyReport(body).subscribe(res => {
      this.journeys = res.data.jourenys;
      this.length = res.data.count;

      if (this.journeys.length == 0) {
        this.noResult = true;
      }
    });
  }

  /**
   * select one value
   * 
   * 
   * @param key 
   */
  selectOne(key: string) {
    switch (key) {
      case 'departments':
        if (this.selectedDepartments.selected) {
          this.selectedDepartments.deselect();
          return false;
        }

        if (this.form.controls.departmentsIds.value.length == this.data.departments.length) {
          this.selectedDepartments.select();
        }
        break;

      case 'creators':
        if (this.selectedCreators.selected) {
          this.selectedCreators.deselect();
          return false;
        }

        if (this.form.controls.creatorsIds.value.length == this.data.journeyCreators.length) {
          this.selectedCreators.select();
        }
        break

      case 'drivers':
        if (this.selectedDrivers.selected) {
          this.selectedDrivers.deselect();
          return false;
        }

        if (this.form.controls.driversIds.value.length == this.data.drivers.length) {
          this.selectedDrivers.select();
        }
        break


      case 'managers':
        if (this.selectedManagers.selected) {
          this.selectedCreators.deselect();
          return false;
        }

        if (this.form.controls.managersIds.value.length == this.data.journeyManagers.length) {
          this.selectedManagers.select();
        }
        break

      case 'thirdParties':
        if (this.selectedThridParties.selected) {
          this.selectedThridParties.deselect();
          return false;
        }

        if (this.form.controls.thirdPartysIds.value.length == this.data.thirdPartys.length) {
          this.selectedThridParties.select();
        }
        break

      case 'vehicles':
        if (this.selectedVehicleTypes.selected) {
          this.selectedVehicleTypes.deselect();
          return false;
        }

        if (this.form.controls.vehicleTypesIds.value.length == this.data.vehicleTypes.length) {
          this.selectedVehicleTypes.select();
        }
        break


      case 'vehiclesIds':
        if (this.selectedVehiclesIds.selected) {
          this.selectedVehiclesIds.deselect();
          return false;
        }

        if (this.form.controls.vehiclesIds.value.length == this.vehiclesNoList.length) {
          this.selectedVehiclesIds.select();
        }
        break


      case 'locationsAndZones':
        if (this.selectedLocationsAndZones.selected) {
          this.selectedLocationsAndZones.deselect();
          return false;
        }

        if (this.form.controls.locationsZones.value.length == this.data.locations.length + this.data.zones.length + this.data.fromMapLocations.length) {
          this.selectedLocationsAndZones.select();
        }
        break

      case 'requestTypes':
        if (this.selectedRequestTypes.selected) {
          this.selectedRequestTypes.deselect();
          return false;
        }

        if (this.form.controls.requestTypesIds.value.length == this.data.requestTypes.length) {
          this.selectedRequestTypes.select();
        }
        break

      case 'status':
        if (this.selectedStatus.selected) {
          this.selectedStatus.deselect();
          return false;
        }

        if (this.form.controls.status.value.length == this.status.length) {
          this.selectedStatus.select();
        }
        break
    }
  }

  /**
   * select all / deselect all
   * 
   * 
   * @param type 
   */
  toggleAllSelection(type: string) {
    switch (type) {
      case 'creators':
        if (this.selectedCreators.selected) {
          this.form.controls.creatorsIds.patchValue([...this.data.journeyCreators.map(item => item.id), 0]);
        } else {
          this.form.controls.creatorsIds.patchValue([]);
        }
        break;

      case 'departments':
        if (this.selectedDepartments.selected) {
          this.form.controls.departmentsIds.patchValue([...this.data.departments.map(item => item.id), 0]);
        } else {
          this.form.controls.departmentsIds.patchValue([]);
        }
        this.onSelectDepartment();
        break;

      case 'managers':
        if (this.selectedManagers.selected) {
          this.form.controls.managersIds.patchValue([...this.data.journeyManagers.map(item => item.id), 0]);
        } else {
          this.form.controls.managersIds.patchValue([]);
        }
        break;

      case 'drivers':
        if (this.selectedDrivers.selected) {
          this.form.controls.driversIds.patchValue([...this.data.drivers.map(item => item.id), 0]);
        } else {
          this.form.controls.driversIds.patchValue([]);
        }
        break;

      case 'thirdParties':
        if (this.selectedThridParties.selected) {
          this.form.controls.thirdPartysIds.patchValue([...this.data.thirdPartys.map(item => item.id), 0]);
        } else {
          this.form.controls.thirdPartysIds.patchValue([]);
        }
        this.onSelectThirdparty();
        break;

      case 'vehicles':
        if (this.selectedVehicleTypes.selected) {
          this.form.controls.vehicleTypesIds.patchValue([...this.data.vehicleTypes.map(item => item.id), 0]);
        } else {
          this.form.controls.vehicleTypesIds.patchValue([]);
        }
        this.onSelectVehicleType();
        break;

      case 'vehiclesIds':
        if (this.selectedVehiclesIds.selected) {
          this.form.controls.vehiclesIds.patchValue([...this.vehiclesNoList.map(item => item.id), 0]);
        } else {
          this.form.controls.vehiclesIds.patchValue([]);
        }
        break;

      case 'requestTypes':
        if (this.selectedRequestTypes.selected) {
          this.form.controls.requestTypesIds.patchValue([...this.data.requestTypes.map(item => item.id), 0]);
        } else {
          this.form.controls.requestTypesIds.patchValue([]);
        }
        break;

      case 'status':
        if (this.selectedStatus.selected) {
          this.form.controls.status.patchValue([...this.status.map(item => item.id), 0]);
        } else {
          this.form.controls.status.patchValue([]);
        }
        break;

      case 'locationsAndZones':
        if (this.selectedLocationsAndZones.selected) {
          this.form.controls.locationsZones.patchValue([...this.data.locations.map(item => 'location_' + item.id), ...this.data.zones.map(item => 'zone_' + item.id), ...this.data.fromMapLocations.map(item => item), 0]);
        } else {
          this.form.controls.locationsZones.patchValue([]);
        }
        break;
    }
  }

  /**
   * select department(s)
   * 
   * 
   * @param event 
   */
  onSelectDepartment() {
    const body = {
      departmentsIds: this.filterArrayFromZero(this.departmentsIds),
      thirdpartysIds: this.filterArrayFromZero(this.thirdpartysIds)
    }
    this.intiateJourneyService.driverLookups(body).subscribe(res => {
      this.data.drivers = res.data;
    });
  }

  /**
   * select thirdparty
   * 
   * 
   * @param event 
   */
  onSelectThirdparty() {
    const driversBody = {
      departmentsIds: this.filterArrayFromZero(this.departmentsIds),
      thirdpartysIds: this.filterArrayFromZero(this.thirdpartysIds)
    }

    this.intiateJourneyService.driverLookups(driversBody).subscribe(res => {
      this.data.drivers = res.data;
    });

    const vehiclesbody = {
      vehiclesTypesIds: this.filterArrayFromZero(this.vehiclesTypesIds),
      thirdpartysIds: this.filterArrayFromZero(this.thirdpartysIds)
    }
    if (vehiclesbody.thirdpartysIds.length > 0 || vehiclesbody.vehiclesTypesIds.length > 0) {
      this.intiateJourneyService.vehiclesLookups(vehiclesbody).subscribe(res => {
        this.vehiclesNoList = res.data;
      });
    } else {
      this.vehiclesNoList = [];
    }
  }

  /**
   * select vehicle(s)
   * 
   * 
   * @param event 
   */
  onSelectVehicleType() {
    let vehiclesbody = {
      vehiclesTypesIds: this.filterArrayFromZero(this.vehiclesTypesIds),
      thirdpartysIds: this.filterArrayFromZero(this.thirdpartysIds)
    }
    if (vehiclesbody.vehiclesTypesIds.length > 0 || vehiclesbody.thirdpartysIds.length > 0) {
      this.intiateJourneyService.vehiclesLookups(vehiclesbody).subscribe(res => {
        this.vehiclesNoList = res.data;
      });
    } else {
      vehiclesbody.vehiclesTypesIds = [0];
      vehiclesbody.thirdpartysIds = [0];
      this.intiateJourneyService.vehiclesLookups(vehiclesbody).subscribe(res => {
        this.vehiclesNoList = res.data;
      });
      this.vehiclesNoList = [];
    }

  }


  /**
   * delete zero from array
   * 
   * 
   * @param array 
   */
  filterArrayFromZero(array: number[]): Array<number> {
    return array.filter(number => Number(number));
  }

  /**
   * export csv
   * 
   * 
   */
  export() {
    if (this.form.invalid) {
      return Swal.fire('Validation Error', 'Required Creation Date From / To', 'error');
    }

    let body: Body = this.form.value;

    body.page = this.pageIndex;
    body.pageSize = this.pageSize;

    body.zonesIds = [];
    body.locationsIds = [];
    body.fromMapLocations = [];

    if (body['locationsZones'] && body['locationsZones'].length > 0) {
      body['locationsZones'].forEach(element => {
        if (element && element.trim().startsWith("location")) {
          body.locationsIds.push(element.replace(/[^0-9]/g, ''));
        } else if (element && element.trim().startsWith("zone")) {
          body.zonesIds.push(element.replace(/[^0-9]/g, ''));
        } else {
          body.fromMapLocations.push(element);
        }
      });
    }

    delete body['locationsZones'];
    this.intiateJourneyService.exportCSV(body).subscribe(res => {
      this.saveFile('Reports.csv', "data:attachment/text", res);
    });
  }

  /**
   * save file as download link
   * 
   * 
   * @param name 
   * @param type 
   * @param data 
   */
  saveFile(name: any, type: any, data) {
    if (data !== null && navigator.msSaveBlob) {
      return navigator.msSaveBlob(new Blob([data], { type: type }), name);
    }

    let a = $("<a style='display: none'/>");
    const url = window.URL.createObjectURL(new Blob([data], { type: type }));

    a.attr("href", url);
    a.attr("download", name);
    $("body").append(a);
    a[0].click();

    window.URL.revokeObjectURL(url);
    a.remove();
  }

  /**
   * change page
   * 
   * 
   * @param event 
   */
  onChangePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize
    this.search();
  }

  /**
   * clear all filters
   * 
   * 
   */
  reset() {
    this.form.patchValue({
      creationDateFrom: new Date(),
      creationDateTo: new Date(),
      deliveryDateFrom: null,
      deliveryDateTo: null,
      journeyId: null,
      driversIds: [],
      departmentsIds: [],
      vehiclesIds: [],
      vehicleTypesIds: [],
      thirdPartysIds: [],
      locationsZones: [],
      requestTypesIds: [],
      managersIds: [],
      creatorsIds: [],
      status: [],
      showMergedJourneys: true
    });

    this.noResult = false;
    this.journeys = [];
  }
}
