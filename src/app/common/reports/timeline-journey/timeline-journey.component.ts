import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { IntiateJourneyService } from '@jms/app/shared/Services/initiate-journey.service';
import { Subscription } from 'rxjs';

export class TimeLine {
  userName: string;
  description: string;
  action: string;
  actionDate: string;
}

@Component({
  selector: 'app-timeline-journey',
  templateUrl: './timeline-journey.component.html',
  styleUrls: ['./timeline-journey.component.scss']
})
export class TimelineJourneyComponent implements OnInit, OnDestroy {

  timelineDetails: TimeLine[] = [];
  timelineSubscription: Subscription;
  journeyId: number;
  displayedColumns: string[] = ['actionDate', 'action', 'userName', 'description'];
  dataSource = new MatTableDataSource<any>();

  constructor(
    private intiateJourneyService: IntiateJourneyService,
    private dialogRef: MatDialogRef<TimelineJourneyComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    dialogRef.disableClose = true;
    this.journeyId = data.journeyId;
  }

  ngOnInit() {
    this.getJourneyTimelineDetails();
  }

  /**
   * get journey timeline with all details
   */
  getJourneyTimelineDetails() {
    if (this.timelineSubscription) {
      this.timelineSubscription.unsubscribe();
    }
    this.timelineSubscription = this.intiateJourneyService.getJourneyTimeLine(this.journeyId).subscribe(
      (res: any) => {
        this.timelineDetails = res.data;
      }
    );
  }
  /**
 * close modal dialog
 */
  close() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (this.timelineSubscription) {
      this.timelineSubscription.unsubscribe();
    }
  }

}
