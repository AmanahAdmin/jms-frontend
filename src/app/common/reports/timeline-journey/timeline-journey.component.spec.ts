import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineJourneyComponent } from './timeline-journey.component';

describe('TimelineJourneyComponent', () => {
  let component: TimelineJourneyComponent;
  let fixture: ComponentFixture<TimelineJourneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineJourneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineJourneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
