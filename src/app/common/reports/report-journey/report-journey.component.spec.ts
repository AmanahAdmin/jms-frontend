import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportJourneyComponent } from './report-journey.component';

describe('ReportJourneyComponent', () => {
  let component: ReportJourneyComponent;
  let fixture: ComponentFixture<ReportJourneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportJourneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportJourneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
