import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TimelineJourneyComponent } from '../timeline-journey/timeline-journey.component';

@Component({
  selector: 'app-report-journey',
  templateUrl: './report-journey.component.html',
  styleUrls: ['./report-journey.component.scss']
})
export class ReportJourneyComponent implements OnInit {

  @Input() journey;

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  /**
   * 
   * @param journeyId 
   * open popup with journey timeline details
   */
  openTimelineJourneyModal(journeyId: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "850px";
    dialogConfig.height = "500px";
    dialogConfig.data = {
      journeyId: journeyId
    };
    const dialogRef = this.dialog.open(
      TimelineJourneyComponent,
      dialogConfig
    );
  }

}
