import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ProfileViewComponent } from './profile/profile-view/profile-view.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
//import { ChangeprofileImageComponent } from './profile/changeprofile-image/changeprofile-image.component';
import { UploadComponent } from '../upload/upload.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [ProfileViewComponent,ProfileEditComponent ],
  exports: [
    ProfileViewComponent,ProfileEditComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class UsersModule { }
