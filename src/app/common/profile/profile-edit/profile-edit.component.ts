import { NgForm, FormGroup } from '@angular/forms';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '@jms/app/shared/Services/UserService';
import { User } from '@jms/app/shared/models/UserModel';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit, OnDestroy {
  currentUser: User;
  userId: string;
  subs: Subscription = new Subscription();
  profile: any = {};
  isSubmitted = false;
  tempModel: any = {};
  @ViewChild('profileFrm', { static: true, read: NgForm }) profileFrm: any;
  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.userId = this.currentUser.id;
  }

  ngOnInit() {
    this.getProfile();

  }
  getProfile() {

    this.subs.add(this.userService.getProfile(this.userId)
      .pipe(map(res => res)).subscribe(profile => {

        if (profile.status == 1) {

          this.profile = profile.data;
          this.tempModel = {
            "fullName": profile.data.fullName,
          };


        }

      }));

  }
  validateFileds(model) {
    if (model.fullName == '' || model.fullName == null
    ) {
      return false;
    }
    return true;
  }
  update() {
    this.isSubmitted = true;
    const modelToPost = Object.assign({}, this.profile)
    if (!this.validateFileds(modelToPost)) {
      return;
    }
    else {
      this.subs.add(this.userService.editProfile(this.userId, modelToPost)
        .pipe(map(res => res)).subscribe(profile => {

          if (profile.status == 1) {
            this.currentUser.fullName = profile.fullName;
            this.currentUser.image = profile.image;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
            Swal.fire('Profile Edit', `Profile edited successfully`, 'success');
            this.router.navigate(['../'], { relativeTo: this.route });
          }

        }));
    }
    console.log(' befor updated', this.profile);

  }
  _markFormPristine(form: FormGroup | NgForm): void {
    Object.keys(form.controls).forEach(control => {
      form.controls[control].markAsPristine();
    });
  }
  cancel() {
    this.profileFrm.form.reset();
    this.profile.fullName = this.tempModel.fullName;
    this._markFormPristine(this.profileFrm.form)
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  ngOnDestroy() { this.subs.unsubscribe(); }

}
