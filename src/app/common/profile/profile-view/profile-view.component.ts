import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '@jms/app/shared/Services/UserService';
import { User } from '@jms/app/shared/models/UserModel';
import { AuthenticationService } from '@jms/app/shared/Services/AuthenticationService';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProfileData } from '@jms/app/shared/Entities/User/profileData';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ProfileChangePasswordModalComponent } from '@jms/app/shared/components/profile-change-password-modal/profile-change-password-modal.component';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit, OnDestroy {
  currentUser: User;
  subs: Subscription = new Subscription();
  profile: ProfileData = new ProfileData();
  isAdmin = false

  constructor(private userService: UserService,
    private authenticationService: AuthenticationService,
    private sanitization: DomSanitizer,
    private matDialog: MatDialog,
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.checkIfHaveAdminPermission();
  }
  public getSantizeUrl(url: string) {
    return this.sanitization.bypassSecurityTrustResourceUrl(url);
  }
  checkIfHaveAdminPermission() {
    const roles = this.currentUser.roles.forEach(item => {
      if (item.name == "JMS Admin") {
        this.isAdmin = true
        this.getProfile();
      }

    })
  }
  getProfile() {
    const userId = this.currentUser.id;
    this.subs.add(this.userService.getProfile(userId)
      .pipe(map(res => res)).subscribe(profile => {

        if (profile.status == 1) {

          this.profile = profile.data;

          console.log('this.currentUser', this.profile);
        }

      }));

  }

  openChangePasswordModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      userId: this.currentUser.id,
      adminId: 0
    }
    const modal = this.matDialog.open(ProfileChangePasswordModalComponent, dialogConfig);

  }

  ngOnDestroy() { this.subs.unsubscribe(); }
}
