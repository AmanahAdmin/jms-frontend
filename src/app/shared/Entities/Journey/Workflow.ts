import { ConcatenateType } from '../../enums/concatenate-type.enum';

export class Workflow {
    id: number = 0;
    rowId: string = '';
    name: string = '';
    isActive: boolean = true;
    isDefault: boolean = false;
    workflowSteps: WorkflowStep[] = [];
}

export class WorkflowStep {
    id: number = 0;
    workflowId: number = 0;
    stepNumber: number = 0;
    isRequired: boolean = true;
    isFeature: boolean = true;
    featureKey?: string = '';
    workflowStepWorkflows: WorkflowStepWorkflow[] = [];
    workflowStepRoles: WorkflowStepRole[] = [];
}

export class WorkflowStepWorkflow {
    id: number = 0;
    workflowStepId: number = 0;
    workflowId: number = 0;
    concatenateType?: ConcatenateType = null;
    isSystemRole: boolean = false;
    isNotifiedManagerRole: boolean = false;
    isApprovedManagerRole: boolean = false;
    roleId?: number = null;
    customRoleId: any = null
}

export class WorkflowStepRole {
    id: number = 0;
    worflowStepId: number = 0;
    isSystemRole: boolean = false;
    isNotifiedManagerRole: boolean = false;
    isApprovedManagerRole: boolean = false;
    roleId?: number = null;
    customRoleId: any = null
    workflowStepRoleConditions: WorkflowStepRoleCondition[] = [];
}

export class WorkflowStepRoleCondition {
    id: number = 0;
    workflowStepRoleId: number = 0;
    conditionKey: string = '';
    conditionIsTrue: boolean = true;
    concatenateType?: ConcatenateType;
}