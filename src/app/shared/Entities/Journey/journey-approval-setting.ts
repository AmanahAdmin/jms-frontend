import { Level } from '../../enums/level.enum';
import { DrivingType } from '../../enums/driving-type.enum';
import { MultiSelect } from '../multi-select';

export class JourneyApprovalSetting {
    id: any = 0;
    rowId: string = '';
    riskType: Level = null;
    drivingType: DrivingType = null;
    isProcessing: boolean = false;
    journeyApprovalRoles: JourneyApprovalNotifyRole[] = [];
    approvalRoles: MultiSelect[] = [];
    journeyNotifyRoles: JourneyApprovalNotifyRole[] = [];
    notifyRoles: MultiSelect[] = [];
    isSubmitted: boolean = false;
}

export class JourneyApprovalNotifyRole {
    roleId: number = null;
    order: number = null;
}


