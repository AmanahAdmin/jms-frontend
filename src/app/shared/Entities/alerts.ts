export class Alert{
  id:number ;
  rowId:string;
  eventName:string;
  alertDuration:number;
  after:number;
  name:string;
  usersToNotify:any[]=[];
  isSubmitted:boolean=false;
  isMain=true;
  groubs:any[] =[];
  isHoursSelected=true;
  ismiutesSelected=false;
  isnewAlert=false;
  isReturnedFromDb=false;
}
