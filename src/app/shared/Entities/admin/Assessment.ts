import { Level } from '../../enums/level.enum';

export class Assessment {
    id: any = 0;
    rowId?: string = '';
    name: string = '';
    isActive: boolean = true;
    assessmentCategoryId: any = 0;
    isPreTrip: boolean = false;
    isCheckPoint: boolean = false;
    isCargoList: boolean = false;
    isPostTrip: boolean = false;
    isDefaultInAnyJourney: boolean = false;
    isAllowUploadingImage: boolean = false;
    assessmentLocalizations: AssessmentLocalization[] = [];
    oneYesPointEqual: any = 1;
    oneNoPointEqual: any = 0;
    assessmentRisks: AssessmentRisk[] = [];
    isValuesRelatedToYesOrChecked: boolean = true;
}

export class AssessmentLocalization {
    languageId: any = 0;
    languageName: string = '';
    languageIsDefault: boolean = false;
    name: string = '';
    assessmentQuestions: AssessmentQuestion[] = [];
}

export class AssessmentQuestion {
    id: any = 0;
    assessmentId: any = 0;
    isRequired: boolean = false;
    displayway?: number = null;
    isBrowsingAnswerType?: boolean = false;
    browsingAnswerTypes?: any;
    order: any = 0;
    name: string = '';
    assessmentQuestionAnswers: AssessmentQestionAnswer[]
}
export class AssessmentQestionAnswer {
    id?: number;
    assessmentQuestionId: number;
    pointValue: number;
    name: string
}

export class AssessmentRisk {
    assessmentId: any = 0;
    riskStatus: Level = 0;
    isRange: boolean = false;
    startValue: any = 0;
    endValue: any = 0;
}
