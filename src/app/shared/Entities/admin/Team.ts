import { User } from '../../models/UserModel';

export class Team {
    id:any=0;
    rowId:string='';
    name:string ='';
    description: string='';
    isHavingActiveJourney:boolean=false;
    isActive:boolean=true;
    userCount:0;
    users:User[]; 
}
