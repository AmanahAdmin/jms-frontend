export class Zone {
    id: any = 0;
    rowId: string = '';
    name: string = '';
    description: string = '';
    isActive: boolean = true;
    latitude: number;
    longitude: number;
    zoom: number = 12;
    zoneLocations: ZoneLocation[] = [];
    search: string;
}

export class ZoneLocation {
    lat: number | string;
    lng: number | string;
    zoneId?: number;
    pointIndex?: number;
}
