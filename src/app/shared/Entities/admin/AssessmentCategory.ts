export class AssessmentCategory {
  id: any = 0;
  rowId: string = "";
  name: string = "";
  isActive: boolean = true;
  isAllowAutoRiskManagement: boolean;
}
