export class Location {
    id:any=0;
    rowId:string='';
    name:string ='';
    description: string='';
    isActive:boolean=true;
    locationAddress: string=''; 
    latitude:any=27.61;
    longitude:any=30.32;
    zoom:number=10;
    locationLocalizations:LocationLocalization[];

}

export class LocationLocalization{
    languageId:any=0;
    languageName:string='';
    name:string ='';
    description: string='';
}
