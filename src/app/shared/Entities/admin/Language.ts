import { LanguageDirection } from '../../enums/language-direction-enum';

export class Language {
    id: any = 0;
    rowId: string = '';
    name: string = '';
    isDefault: boolean = false;
    isActive: boolean = true;
    isForDriver: boolean = false;
    isForManager: boolean = false;
    direction: LanguageDirection = 0;
    image: string = '';
}
