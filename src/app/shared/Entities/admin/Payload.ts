export class Payload{
    id:any=0;
    rowId:string='';
    name:string ='';
    details:string ='';
    isActive:boolean=true;
    payloadTypeId?:number=null;
}