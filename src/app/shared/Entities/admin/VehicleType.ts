import { Time } from '@angular/common';

export class VehicleType {
    id:any=0;
    rowId:string='';
    name:string ='';
    isActive:boolean=true;
    times:any[]=[];
    locations:any[]=[];
    IsInSpecificTime:boolean=false;
    IsInSpecificRoute :boolean=false;
    IsInBothTimeAndRoue:boolean=false;
    isNeedingPreRequestForJourneyInitiation:boolean=false;
    vehicleTypeTimeAndLocation:VehicleTypeTimeAndLocation[]=[];
}
export class VehicleTypeTimeAndLocation{
    zoneId?:number=null;
    locationId?:number=null;
    time:any=null;
}
