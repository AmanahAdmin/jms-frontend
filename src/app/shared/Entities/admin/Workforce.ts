import { User } from '../../models/UserModel';

export class Workforce {
    id:any=0;
    rowId:string='';
    name:string ='';
    description: string='';
    isHavingActiveJourney:boolean=false;
    isActive:boolean=false;
    userCount:any=0;
    users:User[]; 
}
