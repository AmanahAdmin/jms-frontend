import { Time } from '@angular/common';

export class Shift {
    id:any=0;
    rowId:string='';
    startTime:any =null;
    endTime:any=null;
    fromTime:any =null;
    toTime:any=null;
    isActive:boolean=true;
    isNightDriving:boolean=false;
}
