export class AnswerType{
    id:any=0;
    rowId?:string='';
    name:string ='';
    isActive:boolean=true;
    displayWay:any=0;
    checkPoint?:number=0;
    unCheckPoint?:number=0;
    answerTypeLocalizations:AnswerTypeLocalization[]=[];
}

export class AnswerTypeLocalization{
    languageId:any=0;
    languageName:string='';
    languageIsDefault:boolean=false;
    answerTypeItems:AnswerTypeItem[]=[];
}

export class AnswerTypeItem{
    id:any=0;
    answerTypeId:number=0;
    isBrowsingAnswerType?:boolean=false;
    name:string ='';
    point:any=0;
    order:number=0;
}