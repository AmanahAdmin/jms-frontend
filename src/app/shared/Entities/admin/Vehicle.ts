export class Vehicle {
    id: any = 0;
    rowId: string = '';
    plateNumber: string = '';
    modelAndYear?: any = null;
    vehicleTypeId: number = null;
    color: string = '';
    licenseExpiryDateFrom: Date = new Date();
    licenseExpiryDateTo: Date = new Date();
    status?: any = null;
    vehicleTypeName: string;
    notes?: any = '';
    isInUsed?: boolean = false;
    isActive: boolean = true;
    supplierContractNo: string = '';
    isCompanyVehicle: boolean = true;
    thirdPartyManagementId: number = null;
    gatePassDetails: GatePassDetails[] = [];
    typeName?: string;
}

export class GatePassDetails {
    id: any = 0;
    vehicleId?: number = null;
    driverId?: number = null;
    islLocation?: boolean = false;
    zoneId?: number = null;
    locationId?: number = null;
    expiredDate: Date = new Date();
    contractNumber: string;
    checkpointId?: number = null;
}

