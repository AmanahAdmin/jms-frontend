import { UserStatus } from '../../enums/user-status.enum';
import { MultiSelect } from '../multi-select';

export class Manager {
    id = 0;
    rowId: string = '';
    username: string = '';
    email: string = '';
    fullName: string = '';
    workForceId?: any = null;
    userTeams?: any = null;
    teams?: MultiSelect[] = [];
    userRoles?: any = null;
    roles: MultiSelect[] = [];
    password: string = '';
    confrimPassword: string = '';
    isActive: boolean = true;

    position: string = '';
    comments: string = '';
    phoneNumber: string = '';
    emergencyContact: string = '';
    image: string = '';

    userStatus: UserStatus = 0;
    browsingLanguageId?: any = null;
    accessNumberOnTheSystem: any = 0;
    rolesNames?: string[];
}
