import { DriveFor } from '../../enums/drive-for.enum';
import { UserStatus } from '../../enums/user-status.enum';
import { MultiSelect } from '../multi-select';

export class Driver {
    id = 0;
    rowId: string = '';
    username: string = '';
    email: string = '';
    fullName: string = '';
    workForceId?: any = null;
    userTeams?: any = null;
    teams?: MultiSelect[] = [];
    userRoles?: any = null;
    gatePassStatus: string = '';
    trainingDetails: string = '';
    licenseNo: string = '';
    licenseExpiryDate: Date = null;
    password: string = '';
    confrimPassword: string = '';
    isActive: boolean = true;

    nationality: string = '';
    address: string = '';
    phoneNumber: string = '';
    emergencyContact: string = '';
    driveFor: DriveFor = 0;
    isHavingChronicDiseases: boolean = false;
    chronicDiseases: string = '';
    comments: string = '';
    cIDPasswordNumber: string = '';
    image: string = '';
    driverVehicleTypes: any[] = [];
    uiDriverVehicleTypes?: MultiSelect[] = [];
    isCompanyEmployee: boolean = true;
    browsingLanguageId?: any = null;
    accessNumberOnTheSystem: any = 0;
    employeeFor: any[] = [];
    uiEmployeeFor?: MultiSelect[] = [];
    employeeId?: string;
    userDepartmentsNames?: string[];
}
