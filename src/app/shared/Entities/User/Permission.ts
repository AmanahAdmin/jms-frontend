export class Permission{
    name: string;
    value: string;
    isAppearInRoles:boolean;
    features: Feature[];
}
export class Feature{
    name: string;
    value: string;
    groupId:number;
    gruopName:string;
    groupKeys:string;
    mainGroupId:number;
}
