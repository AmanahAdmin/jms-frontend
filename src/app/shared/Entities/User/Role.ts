import { UserRoles } from '../../enums/user-roles.enum'
import { Permission } from '../User/Permission'

export class Role {
    id: any = 0;
    rowId: string = '';
    name: string = '';
    userRoleType: UserRoles;
    permissions: Permission[] = [];
    isCompanyRole: boolean = true;
}
