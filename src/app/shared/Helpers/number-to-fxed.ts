/**
 * convert number to fixed point
 * 
 * 
 * @param x 
 */
export default function mapPointToFixed(number: any) {
    return +Number.parseFloat(number).toFixed(10);
}