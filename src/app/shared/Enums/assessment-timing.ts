export enum AssessmentTiming {
    PreTrip = 0,
    CheckPoint = 1,
    AddingCargo = 2,
    PostTrip = 3,

}
