export enum JourneyStatus {
  PendingOnDriverAndVehicleSelection = 19,
  PendingOnVehicleSelection = 24,
  PendingOnDriverPreAssessment = 25,
  PendingOnPreAssessmentApproval = 36,
  PendingOnDriverStartJourney = 8,
  PendingCheckpointAssessment = 42,
  PendingCheckpointAssessmentApproval = 37,
  PendingOnDriverPostTripAssessment = 32,
  PendingPostTripAssessmentApproval = 35,
  PendingRescheduleTheJourney = 21,
  PendingOnChangingDriver = 33,
  PendingOnChangingVehicle = 34,
  RollBackToDriverToResubmitAssessments = 23,
  Started = 28,
  InProgress = 31,
  JourneyCompleted = 17,
  JourneyRejected = 7,
  JourneyCancelled = 41,
}


/**
 * Journey Status
 * 
 * 
 */
export function journeyStatusArray(): Object[] {
  const array = []
  for (const [key, value] of Object.entries(JourneyStatus)) {
    if (!Number.isNaN(Number(key))) {
      continue;
    }
    array.push({ id: value, name: key });
  }
  return array;
}
