export enum DisplayWay{
    RadioButton= 1,
    CheckedUncCheckedBox=2,
    CheckBox=3,
    SingleSelectDropList=4,
    MultiSelectDropList=5,
    Text=6,
    MultiLineTex=7,
    ImageUpload=8,
    FileUpload=9,
    MultiFileUploads=10,
}