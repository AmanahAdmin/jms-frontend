export enum UserRoles {
        ProductLine,
        Dispatcher ,
        JMC,
        Driver ,
        QHSE ,
        GBM,
        OperationManager,
        Admin
}
export enum ManagerRoles {
       Driver= UserRoles.Driver,
        ProductLine = UserRoles.ProductLine,
        Dispatcher = UserRoles.Dispatcher,
        JMC = UserRoles.JMC,
        QHSE = UserRoles.QHSE,
        GBM = UserRoles.GBM,
        OperationManager = UserRoles.OperationManager
}
