import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileChangePasswordModalComponent } from './profile-change-password-modal.component';

describe('ProfileChangePasswordModalComponent', () => {
  let component: ProfileChangePasswordModalComponent;
  let fixture: ComponentFixture<ProfileChangePasswordModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileChangePasswordModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileChangePasswordModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
