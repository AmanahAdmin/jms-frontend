import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../Helpers/MustMatch';
import { UserService } from '../../Services';
import { Subscription } from 'rxjs';
import { ChangePasswordModel } from '../../Models/change-password-model';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-profile-change-password-modal',
  templateUrl: './profile-change-password-modal.component.html',
  styleUrls: ['./profile-change-password-modal.component.scss']
})
export class ProfileChangePasswordModalComponent implements OnInit, OnDestroy {
  changePsswordForm: FormGroup;
  @Input() adminId;
  @Input() userId;
  changePasswordSubscription: Subscription;
  changePasswordModel: ChangePasswordModel;
  isSubmitted;

  /**
   * 
   * @param dialogRef 
   * @param formBuilder 
   * @param userService 
   * @param data 
   */
  constructor(
    private dialogRef: MatDialogRef<ProfileChangePasswordModalComponent>,
    private formBuilder: FormBuilder,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) data,
  ) {
    this.adminId = data.adminId;
    this.userId = data.userId;
  }

  ngOnInit() {
    this.changePsswordForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('newPassword', 'confirmPassword')
    });
    this.changePasswordModel = new ChangePasswordModel();
    this.changePasswordModel.adminId = this.adminId;
    this.changePasswordModel.userId = this.userId;

  }

  get f() { return this.changePsswordForm.controls; }

  /**
   * close change password modal
   */
  close() {
    this.dialogRef.close();
  }

  /**
   * save new password
   */
  save() {
    this.isSubmitted = true;
    if (this.changePsswordForm.invalid) {
      return;
    }
    this.changePasswordModel.oldPassword = this.changePsswordForm.get('oldPassword').value;
    this.changePasswordModel.newPassword = this.changePsswordForm.get('newPassword').value;
    this.changePasswordModel.confirmPassword = this.changePsswordForm.get('confirmPassword').value;
    if (this.changePasswordSubscription) {
      this.changePasswordSubscription.unsubscribe();
    }
    this.changePasswordSubscription = this.userService.changePassword(this.changePasswordModel).subscribe(
      (res: any) => {
        if (res.isSuccessful) {
          Swal.fire('', res.message, 'success');
          this.dialogRef.close();
        } else {
          Swal.fire('', res.message, 'error')
        }
      }, err => {
        Swal.fire('', err.message, 'error')
      }
    );
  }

  ngOnDestroy(): void {
    if (this.changePasswordSubscription) {
      this.changePasswordSubscription.unsubscribe();
    }
  }

}
