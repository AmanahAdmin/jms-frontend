import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() currentPage: number = 1;
  totalPages: number;
  @Input() pageSize: number;
  @Input() set count(count: number) {
    if (this.pageSize) {
      this.totalPages = Math.ceil(count / this.pageSize);
    }
  }
  @Output() onChangePage = new EventEmitter<number>();
  public nearDistance: number = 2;

  ngOnInit(): void {
  }

  changePage(pageNumber) {
    if (pageNumber > 0 && pageNumber <= this.totalPages) {
      this.currentPage = pageNumber;
      this.onChangePage.emit(pageNumber);
    }
  }

  isNearToCurrentPage(pageNumber) {
    return Math.abs(this.currentPage - pageNumber) <= this.nearDistance;
  }

  isShowStartDots() {
    return this.currentPage - 1 > this.nearDistance + 1;
  }
  isShowEndDots() {
    return this.totalPages - this.currentPage > this.nearDistance + 1;
  }

}
