import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { ChangeprofileImageComponent } from '@jms/app/common/upload-image/changeprofile-image/changeprofile-image.component';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.css']
})
export class ProfileImageComponent implements OnInit {
  @Input() profileImg;

  constructor(
    private matDialog: MatDialog
  ) { }

  ngOnInit() {
  }
  createImgPath = (serverPath: string) => {
    return `${environment.JMSURL}/${serverPath}`;
  }
  openUserUplaodModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    const modal = this.matDialog.open(ChangeprofileImageComponent, dialogConfig);
    modal.componentInstance.onSaveImage.subscribe(
      res => {
        if (res) {
          this.profileImg = res;
        }
      }
    );
  }

}
