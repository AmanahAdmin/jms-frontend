import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwalService } from './Services';
import { ProfileImageComponent } from './components/profile-image/profile-image.component';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material';
import { UploadImageModule } from '../common/upload-image/upload-image.module';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ProfileChangePasswordModalComponent } from './components/profile-change-password-modal/profile-change-password-modal.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ProfileImageComponent, PaginationComponent, ProfileChangePasswordModalComponent],
  entryComponents: [ProfileChangePasswordModalComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatDialogModule,
    UploadImageModule,
    ReactiveFormsModule
  ],
  exports: [ProfileImageComponent, PaginationComponent],
  providers: [SwalService]
})
export class SharedModule { }
