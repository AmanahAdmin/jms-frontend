import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestTypesService {

  constructor(private http: HttpClient) { }
  uri: string = `${environment.JMSApiURL}/RequestType`;



  Add(model: any) {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/Add`, model);
  }
  Update(model: any) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/Edit`, model);
  }
  GetAll(query) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAll?name=` + query);
  }
  Delete(id) {
    return this.http.delete<ServiceResponse<any>>(`${this.uri}/Delete?id=` + id);
  }

  Active(id, isActive) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/Activate?id=` + id + `&isActive=` + isActive, null);
  }
}
