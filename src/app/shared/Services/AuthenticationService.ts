import { Data } from "@angular/router";
import { CommonService } from "@jms/app/shared/Services/CommonService";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../models/UserModel";
import { environment } from "../../../environments/environment";
import { ServiceResponse } from "../Models/ServiceResponseModel";
import { UserStatus } from "../enums/user-status.enum";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private groupFeatures: any[] = [];

  constructor(private http: HttpClient, private commonService: CommonService) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public validateFeature(featurename: string) {
    let isFeatureExist = false;
    if (this.currentUserValue != null && this.currentUserValue.roles != null) {
      let permissions = [];
      for (var index = 0; index < this.currentUserValue.roles.length; index++) {
        permissions = this.currentUserValue.roles[index].permissions;

        if (permissions != null) {
          const index: number = permissions.indexOf(featurename);
          if (index !== -1) {
            isFeatureExist = true;
            break;
          }
        }
      }
    }
    return isFeatureExist;
  }
  public validateFeatureGroups(featurename: string, groupId) {
    let isFeatureExist = false;
    if (this.currentUserValue != null && this.currentUserValue.roles != null) {
      let permissions = [];
      for (var index = 0; index < this.currentUserValue.roles.length; index++) {
        permissions = this.currentUserValue.roles[index].permissions;
        if (permissions != null) {
          const index: number = permissions.indexOf(featurename);
          if (index !== -1 && groupId == 0) {
            isFeatureExist = true;
            break;
          }
        }
      }
    }
    return isFeatureExist;
  }

  login(username: string, password: string) {
    const FIREBASE_TOKEN = JSON.parse(localStorage.getItem("FIREBASE_TOKEN"));
    return this.http
      .post<ServiceResponse<any>>(
        `${environment.JMSApiURL}/user/Authenticate`,
        { username, password, DeviceToken: FIREBASE_TOKEN }
      )
      .pipe(
        map((user) => {
          if (user.data && user.data.token) {
            localStorage.setItem("currentUser", JSON.stringify(user.data));
            this.currentUserSubject.next(user.data);
          }
          return user;
        })
      );
  }

  logout() {
    if (this.currentUserSubject.value) {
      this.UpdateUserStatus(
        this.currentUserSubject.value.rowId,
        UserStatus.unavailable
      );
      localStorage.removeItem("currentUser");
      this.currentUserSubject.next(null);
    }
  }
  logoutClean() {
    if (this.currentUserSubject.value) {
      this.UpdateUserStatus(
        this.currentUserSubject.value.rowId,
        UserStatus.unavailable
      );
      this.currentUserSubject.next(null);
    }
  }

  async forgetPassword(username) {
    return await this.http
      .post<any>(
        environment.JMSApiURL + `/user/ForgetPassword?username=${username}`,
        {}
      )
      .toPromise();
  }

  async forgetChangePassword(token, newpassword) {
    return await this.http
      .post<any>(
        environment.JMSApiURL +
          `/user/ResetForgetPassword?token=${token}&newPassword=${newpassword}`,
        {}
      )
      .toPromise();
  }

  async UpdateUserStatus(rowId: string, userStatus: any) {
    return await this.http
      .get(
        `${environment.JMSApiURL}/user/UpdateUserStatus/${rowId}/${userStatus}`
      )
      .toPromise();
  }

  async UpdateUserDefaultLanguage(rowId: string, languageId: any) {
    this.currentUserValue.browsingLanguageId = languageId;
    this.updateLocalStorage();
    return await this.http
      .get(
        `${environment.JMSApiURL}/user/UpdateUserDefaultLanguage/${rowId}/${languageId}`
      )
      .toPromise();
  }

  async UpdateNumberOfBrowsing() {
    this.currentUserValue.accessNumberOnTheSystem += 1;
    this.updateLocalStorage();
  }

  updateLocalStorage() {
    localStorage.setItem("currentUser", JSON.stringify(this.currentUserValue));
    this.currentUserSubject.next(this.currentUserValue);
  }

  //Roles
  GetRoles() {
    return this.http
      .get<ServiceResponse<any>>(environment.JMSApiURL + `/role/GetAll`)
      .toPromise();
  }

  GetLookupRoles() {
    return this.http
      .get<ServiceResponse<any>>(environment.JMSApiURL + `/role/GetLookup`)
      .toPromise();
  }

  //Teaams
  async GetTeams() {
    return await this.http
      .get(environment.JMSApiURL + `/user/getgroups`)
      .toPromise();
  }

  GetAllTeam() {
    return this.http
      .get<ServiceResponse<any>>(environment.JMSApiURL + `/team/getall`)
      .toPromise();
  }

  async AddTeam(name) {
    return await this.http
      .post<any>(environment.JMSApiURL + `/user/addgroup`, { Name: name })
      .toPromise();
  }

  async DeleteTeam(groupId) {
    return await this.http
      .post<any>(
        environment.JMSApiURL + `/user/deletegroup?groupId=${groupId}`,
        {}
      )
      .toPromise();
  }

  //Workforce
  async GetWorkFoces() {
    return await this.http
      .get(environment.JMSApiURL + `/workforce`)
      .toPromise();
  }

  GetAllWorkFoce() {
    return this.http
      .get<ServiceResponse<any>>(environment.JMSApiURL + `/workforce/getall`)
      .toPromise();
  }

  async AddWorkForce(name) {
    return await this.http
      .post<any>(environment.JMSApiURL + `/user/addworkforce`, { Name: name })
      .toPromise();
  }

  async DeleteWorkForce(workforceId) {
    return await this.http
      .post<any>(
        environment.JMSApiURL +
          `/user/deleteworkforce?workforceId=${workforceId}`,
        {}
      )
      .toPromise();
  }

  //users
  async GetAllUsers() {
    return await this.http
      .get<any>(environment.JMSApiURL + `/user/getAllUsers`)
      .toPromise();
  }

  async Register(data) {
    return await this.http
      .post<any>(environment.JMSApiURL + `/user/register`, data)
      .toPromise();
  }

  public GetManagerPermissions() {
    return this.http
      .get<any>(environment.JMSApiURL + `/common/GetManagerPermission`)
      .toPromise();
  }
}
