import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor() { }
  public showCustomMessages(
    params: {
      title?: string,
      messages?: string[],
      type: 'success' | 'warning' | 'question' | 'error' | 'info'
    }) {
    let messagesTemplate = "";
    for (let i = 0; i < params.messages.length; i++) {
      messagesTemplate += `<p class="custom-swal-p">${params.messages[i]}</p>`
    }
    return Swal.fire(params.title, messagesTemplate, params.type)
  }
  public showConfirmMessage(message: string, name: string){
    return Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${message} this ${name} !`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${message} it!`
    })
  }

}
