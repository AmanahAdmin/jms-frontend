import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({
  providedIn: 'root'
})
export class AlertServiceService {

  constructor(private http: HttpClient) { }
  uri: string = `${environment.JMSApiURL}/Alert`;

  Add(model: any) {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/Add`, model);
  }
  Update(id, model: any) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/Update?id=` + id, model);
  }
  GetAll() {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAll`);
  }
  Delete(id) {
    return this.http.delete<ServiceResponse<any>>(`${this.uri}/Delete?id=` + id);
  }
}
