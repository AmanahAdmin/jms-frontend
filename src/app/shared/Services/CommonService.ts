import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({ providedIn: 'root' })
export class CommonService{

    constructor(private http: HttpClient) {
    }
    GetManagerRolePermission(){
        return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetManagerRolePermission`).toPromise();
    }
    
    GetWorkflowFeature(){  
        return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetWorkflowFeature`).toPromise();
    }
    GetWorkflowCondition(){  
        return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetWorkflowCondition`).toPromise();
    }

    GetDriverRolePermission() :Observable<any>{
      return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetDriverRolePermission`);
  }
    Add(model){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/Driver/Add`, model).toPromise();
    }
    Update(model){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/Driver/Update`, model).toPromise();
    }
    Delete(rowId){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/Driver/Delete`, rowId).toPromise();
    }
    ActivateOrDeactivate(rowId, isActive){
        return  this.http.get(`${environment.JMSApiURL}/driver/ActivateDeactivate?rowId=${rowId}&isActive=${isActive}`);
    }

    GetSubPagesPermission(groupId:number) :Observable<any>{
      return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetPermissionsOfSubGroups?groupId=`+groupId);
  }
  GetMainPagesPermission() :Observable<any>{
    return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetPermissionsOfMainGroups`);
}
GetFeatures() :Observable<any>{
  return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/Common/GetFeatures` );
}
}
