import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({ providedIn: 'root' })
export class ManagerService{
   
    constructor(private http: HttpClient) { 
       
    }
    GetManagers(managername=""){
       return  this.http.get(`${environment.JMSApiURL}/manager/getmanagers?managername=${managername}`);    
    }
    GetAll(){
        return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/GetAll`).toPromise();    
    }
    Add(model:any){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/Add`, model);
    }
    Update(model:any){
        return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/Update`, model);
    }
    Delete(rowId:string){
        return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/SoftDelete/${rowId}`).toPromise(); 
    }
    ActivateOrDeactivate(rowId:string, isActive:boolean){
        return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/manager/ActivateDeactivate/${rowId}/${isActive}`).toPromise();
    }

}
