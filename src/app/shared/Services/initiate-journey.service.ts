import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';
import { Observable } from 'rxjs';
import { JourneyAssessmentModel } from '../Models/JourneyAssessmentModel';
import { AssessmentAnswersModal } from '../Models/assessmentAnswer.model';
import { mergedJourneysModal } from '../Models/merged-journey.model';

@Injectable({
  providedIn: 'root'
})
export class IntiateJourneyService {

  constructor(private http: HttpClient) { }
  uri: string = `${environment.JMSApiURL}/IntiateJouerny`;
  newUri: string = `${environment.JMSApiURL}/AssessmentResult`;

  GetAll(model: any) {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/GetAll`, model);
  }

  GetLookUps(): Observable<any> {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetLookUps`);
  }

  GetDriverJourenies(id) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetDriverJourenies?driverId=` + id);
  }

  /**
   * journey details
   * 
   * 
   * @param id 
   */
  getJourenyFullDetails(id: number) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/Get/${id}`);
  }

  /**
   * update drivers
   * 
   * 
   * @param body 
   */
  editDrivers(body: Object) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/EditDrivers`, body);
  }

  /**
   * update vehicles
   * 
   * 
   * @param body 
   */
  editVehicles(body: Object) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/EditVehicles`, body);
  }

  /**
   * update drivers and Vvehicles
   * 
   * 
   * @param body 
   */
  editDriversAndVehicles(body: Object) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/EditVehiclesAndDriver`, body);
  }

  Add(model: any) {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/Add`, model);
  }

  /**
   * edit journey
   * 
   * 
   * @param body 
   */
  update(body: any) {
    return this.http.put<ServiceResponse<any>>(`${this.uri}/Edit`, body);
  }

  Delete(id) {
    return this.http.delete<ServiceResponse<any>>(`${this.uri}/Delete?id=` + id);
  }

  Drivers(Vid, isCompanyEmpolyee) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/Drivers/${Vid}?isCompanyEmpolyee=${isCompanyEmpolyee}`);
  }

  GetAssessmentByCategory(CatID, LanId) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAssessmentByCategory/` + CatID + `/` + LanId);
  }

  Vehicles(vehicleTypeId, isCompanyVechile) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/Vehicles/${vehicleTypeId}?isCompanyVechile=${isCompanyVechile}`);
  }

  CheckISPajero(vehicleTypeId) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/CheckISPajero?vehicelTypeId=` + vehicleTypeId);
  }

  GetDEfaultAssessments(LanId) {
    return this.http.get<ServiceResponse<JourneyAssessmentModel[]>>(`${this.uri}/GetDEfaultAssessments?LanguageId=` + LanId);
  }

  GetAllDefaultAddCargoAssessments(LanId) {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAllDefaultAddCargoAssessments?LanguageId=` + LanId);
  }


  getAssessmentAnswers(journeyId, languageId) {
    if (!languageId) {
      languageId = 1;
    }
    return this.http.get<ServiceResponse<AssessmentAnswersModal>>(`${this.uri}/GetAssessmentAnswers/${journeyId}?languageId=${languageId}`);
  }

  rejectJourney(journeyId, rejectionReason) {
    return this.http.put(`${this.uri}/Reject`, { journeyId, rejectionReason })
  }

  approveJourney(journeyId) {
    return this.http.put(`${this.uri}/approve/${journeyId}`, {})
  }

  requestChangeDriver(journeyId, changingReason) {
    const action = '/request-changeDrivers';
    return this.http.put(this.uri + action, { journeyId, changingReason });
  }

  requestChangeVehicle(journeyId, changingReason) {
    const action = '/request-changeVehicles';
    return this.http.put(this.uri + action, { journeyId, changingReason });
  }


  /**
   * joureny action
   * 
   * 
   * @param body 
   */
  requstReschedule(body: object) {
    const action = '/RequestToHandleJourneyAction';
    return this.http.put(this.uri + action, body);
  }

  /**
   * perform joureny action
   * 
   * 
   * @param body 
   */
  performTheRequestToHandleJourneyAction(body: object) {
    const action = '/PerformTheRequestToHandleJourneyAction';
    return this.http.put(this.uri + action, body);
  }

  rollBackAssessments(journeyId, langId) {
    if (!langId) {
      langId = 1;
    }
    const action = 'RollBackAssessments';
    return this.http.get(`${this.uri}/${action}?${journeyId}?${langId}`);
  }

  changeJourneyStatus(journeyId, journeyStatus) {
    const action = '/ChangeStatus'
    return this.http.put(this.uri + action, { journeyId, journeyStatus });
  }

  /**
   * make journey checked-in
   * 
   * 
   * @param body 
   */
  checkIn(body: object): Observable<any> {
    return this.http.put(`${this.uri}/CheckInFromWeb`, body);
  }

  /**
   * merge selected journeys
   * @param model 
   */
  mergeJourneys(model: mergedJourneysModal) {
    return this.http.post<ServiceResponse<mergedJourneysModal>>(`${this.uri}/Merge`, model);

  }

  /**
   * unmerge journey
   * 
   * 
   * @param journeyId 
   */
  unmergeJourney(journeyId) {
    return this.http.put(`${this.uri}/UnMerge/${journeyId}`, {})
  }

  /**
   * filter journeys
   * 
   * 
   * @param body 
   */
  jurneyReport(body: object): Observable<any> {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/JourneyReport`, body);
  }

  /**
 * journey reports lookups
 * 
 * 
 * @param journeyId 
 */
  get reportLookUps(): Observable<any> {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetReportLookUps`);
  }

  /**
   * drivers lookups
   * 
   * 
   * @param body 
   */
  driverLookups(body: object): Observable<any> {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/GeDriversLookUps`, body);
  }

  /**
   * vehicles lookups
   * 
   * 
   * @param body 
   */
  vehiclesLookups(body: object): Observable<any> {
    return this.http.post<ServiceResponse<any>>(`${this.uri}/GeVehiclesLookUps`, body);
  }

  /**
   * export
   * 
   * 
   * @param body 
   */
  exportCSV(body: object): Observable<any> {
    return this.http.post(`${this.uri}/exportToCVS`, body, {
      responseType: 'blob'
    });
  }

  /**
   * 
   * @param journeyId 
   * get journey timeline details in journey report
   */
  getJourneyTimeLine(journeyId: number) {
    return this.http.get(this.uri + `/GetJourneyTimeLine?journeyId=${journeyId}`)
  }
}
