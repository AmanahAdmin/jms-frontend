import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertTypesService {

  constructor(private http: HttpClient) { }
  uri: string = `${environment.JMSApiURL}/AlertTypes`;
  GetAll(): Observable<any> {
    return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAll`);
  }
}
