import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@jms/environments/environment';
import { CommunicationPoint } from '../Models/communication-point-model';
import { JourneyLocationsModel } from '../Models/journey-locations-model';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({
  providedIn: 'root'
})
export class CommunicationPointService {

  uri: string = `${environment.JMSApiURL}/ComunicationPoint`;

  constructor(
    private http: HttpClient,
  ) {

  }

  /**
   * add comunication
   * 
   * 
   * @param point 
   */
  addCommunicationPoint(point: CommunicationPoint) {
    const action = "/AddComunicationPoint";
    return this.http.post(this.uri + action, point)
  }

  /**
   * save communication
   * 
   * 
   * @param points 
   */
  save(points: CommunicationPoint[], jouernyId: number) {
    const action = "SaveComunicationPoint";
    return this.http.post(`${this.uri}/${action}?JouernyID=${jouernyId}`, points);
  }

  /**
   * updatr selected communication
   * 
   * 
   * @param id 
   */
  updateCommunicationPoint(point: CommunicationPoint) {
    const action = "/JourneyCommunicationPointUpdate";
    return this.http.put(`${this.uri}/${action}/${point.id}`, point);
  }

  /**
   * delete selected communication
   * 
   * 
   * @param id 
   */
  deleteCommunicationPoint(id: number) {
    const action = "/DeleteJourneyCommunicationPoint";
    return this.http.delete(`${this.uri}/${action}/${id}`);
  }


  /**
   * communications by journey
   * 
   * 
   * @param id 
   */
  communicationPointsByJourney(id: number) {
    const action = "/JourneyCommunicationPoints";
    return this.http.get(`${this.uri}/${action}/${id}`);
  }

  /**
   * 
   * @param journeyId
   * get all locations in a journey with journey id
   */
  getJourneyLocations(journeyId) {
    return this.http.get<ServiceResponse<JourneyLocationsModel[]>>(`${this.uri}/GetJourneyLocations?journeyId=${journeyId}`);
  }
}
