import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({ providedIn: 'root' })
export class LanguageService{
    constructor(private http: HttpClient) {   }
    uri: string=`${environment.JMSApiURL}/language`;
    
    GetAll(){
        return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAll`).toPromise(); 
    }
    GetBy(isDriver:boolean){
        return this.http.get<ServiceResponse<any>>(`${this.uri}/GetAllBy/${isDriver}`).toPromise(); 
    }
    Add(model:any){
        return this.http.post<ServiceResponse<any>>(`${this.uri}/Add`, model).toPromise();
    }
    Update(model:any){
        return this.http.post<ServiceResponse<any>>(`${this.uri}/Update`, model).toPromise();
    }
    Delete(rowId:string){
        return this.http.get<ServiceResponse<any>>(`${this.uri}/Delete/${rowId}`).toPromise(); 
    }
    ActivateOrDeactivate(rowId:string,isActive:boolean){
        return this.http.get<ServiceResponse<any>>(`${this.uri}/ActivateDeactivate/${rowId}/${isActive}`).toPromise(); 
    }
}
