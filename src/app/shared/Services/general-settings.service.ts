/**
 * @class GeneralSettingsService
 * 
 * general setting 
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@jms/environments/environment';
import { Observable } from 'rxjs';
import { ServiceResponse } from '../Models/ServiceResponseModel';

@Injectable({
  providedIn: 'root'
})
export class GeneralSettingsService {

  /**
   * 
   * @param http 
   */
  constructor(private http: HttpClient) {
  }

  /**
   * all settings
   * 
   * 
   */
  get list(): Observable<any> {
    return this.http.get<ServiceResponse<any>>(`${environment.JMSApiURL}/GeneralSetting/GetAll`);
  }

  /**
   * add settings
   * 
   * 
   * @param body 
   */
  get add(): Observable<any> {
    const body = {
      distanceRestriction: 100,
      drivingHourLimit: 0,
      isActive: true,
      isCheckIn: true,
      isSuccess: true
    }

    return this.http.post<ServiceResponse<any>>(`${environment.JMSApiURL}/GeneralSetting/Add`, body);
  }

  /**
   * update settings
   * 
   * 
   * @param body 
   */
  update(body: any): Observable<any> {
    return this.http.put<ServiceResponse<any>>(`${environment.JMSApiURL}/GeneralSetting/Edit`, body);
  }

}
