import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ChangePasswordModel } from '../Models/change-password-model';

@Injectable({
   providedIn: 'root'
})
export class UserService {
   constructor(private http: HttpClient) { }

   getMangersOutCompany(): Observable<any> {
      return this.http.get(`${environment.JMSApiURL}/user/GetLookupMangers`);
   }
   getProfile(id: any): Observable<any> {
      return this.http.get(`${environment.JMSApiURL}/user/Profile?userId=` + id);
   }
   editProfile(id: any, model: any): Observable<any> {
      return this.http.put(`${environment.JMSApiURL}/user/EditProfile?userId=` + id, model);
   }
   changeProfileImage(model: any): Observable<any> {
      return this.http.post(`${environment.JMSApiURL}/user/EditProfileImage`, model)
   }
   /**
    * 
    * @param model change password
    */
   changePassword(model: ChangePasswordModel) {
      return this.http.post(`${environment.JMSApiURL}/user/ChangePassword`, model)
   }


}
