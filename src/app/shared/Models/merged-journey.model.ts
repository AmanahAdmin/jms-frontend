export class MergedJourney {
    title?: string;
    status?: string;
    isHold: boolean;
    driver?: string;
    journeyId: number;
    vehiclePlateNo?: string;
    order?: number;
    vehicleId?: number;
    vehicleTypeId?: number;
    driverId: number;
}
export class MergedJourneyForm {
    journeyId: number;
    order: number;
    isHold: boolean;
}
export class mergedJourneysModal {
    vehicleId: number;
    vehicleTypeId: number;
    driverId: number;
    mergedJourneys: MergedJourneyForm[];
}