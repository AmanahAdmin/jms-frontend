import { ResponseStatus } from '../enums/response-status.enum';

export interface ServiceResponse<T> {
    status: ResponseStatus,
    message: string,
    exception: string,
    isSuccessful?: boolean,
    data: T
}