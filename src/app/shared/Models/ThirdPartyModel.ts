export class ThirdPartyModel {
  constructor(
    public id: number,
    public name: string,
    public isActive: boolean
  ) {}
}
