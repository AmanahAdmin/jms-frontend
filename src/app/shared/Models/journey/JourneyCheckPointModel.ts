
export class JourneyCheckPointModel {
  constructor(
    public id: number,
    public journeyId: number,
    public haveCargo: boolean,
    public ETA: string,
    public arrivedAtHoures: number,
    public arrivedAtMintues: number,
    public locationId: number | null,
    public zoneId: number | null,
    public IsMapLocation: boolean,
    public FromLocation: string,
    public FromLocationLat: string | number,
    public FromLocationLng: string | number,
    public ToLocation: string,
    public LocationLat: string,
    public LocationLng: string,
    public Order: number,
    public zone: object,
    public location: object,
    public allowUpdateDeleteAction: boolean,
  ) { }
}
