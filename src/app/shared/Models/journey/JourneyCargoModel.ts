import { JourneyTruckModel } from '../JourneyTruckModel';
import { JourneyPassengerModel } from './JourneyPassengerModel';
import { PayloadModel } from '@jms/app/shared/Models/PayloadModel';

export class JourneyCargoModel {
  constructor(
    public id: number,
    public journeyId: number,
    public PayloadTypes: PayloadModel[],
    public journeyPassengers: JourneyPassengerModel[],
    public journeyTrucks: JourneyTruckModel[],
  ) { }
}
