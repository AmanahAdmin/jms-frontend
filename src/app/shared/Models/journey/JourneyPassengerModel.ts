
export class JourneyPassengerModel {
  constructor(
    public id: number,
    public fullName: string,
    public contactNumber: string,
  ) {}
}
