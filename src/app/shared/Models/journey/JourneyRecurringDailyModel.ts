export class JourneyRecurringDailyModel {
  constructor(
    public id: number,
    public journeyBasicInfoId: number,
    public fromDate: string,
    public fromTime: string,
    public isNoEndDate: boolean,
  public isEndAfter: boolean,
  public endAfterJourneyNumbers: number,
  public isEndBy: boolean,
  public endByDate: string,
  ) {}
}
