export class JourneyRecurringMonthlyModel {
  constructor(
    public id: number,
    public journeyBasicInfoId: number,
    public isDaily: boolean,
    public dayiesNumber: number,
    public everyMonthsNumber: number,
    public  isWeekly: boolean,
    public weekOrder: number,
    public  dayOfWeek: number,
    public weeklyMonthsNumber: number,
    public  fromDate: string,
    public  fromTime: string,
    public  isNoEndDate: boolean,
    public  isEndAfter: boolean,
    public endAfterJourneyNumbers: number,
    public  isEndBy: boolean,
    public endByDate: string,
  ) {}
}
