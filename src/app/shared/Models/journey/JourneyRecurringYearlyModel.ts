
export class JourneyRecurringYearlyModel {
  constructor(
    public  id: number,
    public journeyBasicInfoId: number,
    public every: number,
    public  isMonthly: boolean,
    public  monthNumber: number,
    public  dayNumber: number,

    public  isThe: boolean,
    public  order: number,
    public  day: number,
    public month: number,
    public  fromDate: string,
    public  fromTime: string,
    public  isNoEndDate: boolean,
    public isEndAfter: boolean,
    public endAfterJourneyNumbers: number,
    public isEndBy: boolean,
    public  endByDate: string,
  ) {}
}
