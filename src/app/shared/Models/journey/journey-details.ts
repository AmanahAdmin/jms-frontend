export interface Vehicle {
    id?: number;
    journeyId?: number;
    vehicleId: number;
    vehicleName: string;
}

export interface Driver {
    id: number;
    journeyId: number;
    driverId: number;
    driverName: string;
}

export interface DriverVehicle {
    journeyId: number;
    vehicles: Vehicle[];
    drivers: Driver[];
}