import { PayloadModel } from '../PayloadModel';

export class JourneyTruckModel {
  constructor(
    public  id: number,
    public payloadId: number,
    public  payload: PayloadModel,
    public weight: string,
  ) {}
}
