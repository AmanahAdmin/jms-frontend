
export class JourneyRecurringWeeklyModel {
  constructor(
    public id: number,
    public journeyBasicInfoId: number,
    public every: number,
    public dayName: string,
    public dayNumber: number,
    public fromDate: string,
    public fromTime: string,
    public isNoEndDate: boolean,
    public isEndAfter: boolean,
    public endAfterJourneyNumbers: number,
    public  isEndBy: boolean,
    public endByDate: string,
  ) {}
}
