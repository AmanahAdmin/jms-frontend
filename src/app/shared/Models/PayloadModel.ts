export interface PayloadModel {
  id?: number;
  rowId?: string;
  payloadTypeId?: number | null;
  name?: string;
  details?: string;
  isActive?: boolean;
  isPeople?: boolean;
}
