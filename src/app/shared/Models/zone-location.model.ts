export class ZoneLocationRoutes {
    locations: Route[];
    zones: Route[];
}
export class Route {
    id:number;
    name: string;
    isDisabled: boolean;
}