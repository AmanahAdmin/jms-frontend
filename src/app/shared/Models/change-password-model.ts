export class ChangePasswordModel {
    adminId: number;
    userId: number;
    oldPassword: string;
    newPassword: string;
    confirmPassword: string
}