export interface JourneyVehicleModel {
  id?: number;
  journeyId?: number;
  vehicleId: number;
  vehicleName: string;
}
