import { JourneyStatus } from "../enums/journey-status.enum";
import { Level } from "../enums/level.enum";
import { JourneyVehicleModel } from "./JourneyVehicleModel";
import { JourneyDriverModel } from "./JourneyDriverModel";
import { JourneyAssessmentModel } from "./JourneyAssessmentModel";
import { JourneyRecurringDailyModel } from "./journey/JourneyRecurringDailyModel";
import { JourneyCargoModel } from "./journey/JourneyCargoModel";
import { JourneyCheckPointModel } from "./journey/JourneyCheckPointModel";
import { JourneyRecurringMonthlyModel } from "./journey/JourneyRecurringMonthlyModel";
import { JourneyRecurringWeeklyModel } from "./journey/JourneyRecurringWeeklyModel";
import { JourneyRecurringYearlyModel } from "./journey/JourneyRecurringYearlyModel";

export interface JourneyInfoModel {
  id: number;
  title: string;
  departmentId?: number;
  department?: object;
  requestTypeId: number;
  journeyStatus: JourneyStatus;
  startDate: string;
  deliveryDate: string;
  purpose: string;
  emergencyContact: string;
  fromZone: number;
  toZone: number;
  isMapLocation: boolean;
  fromDestination: string;
  fromLat: number | null;
  fromLng: number | null;
  toDestination: string;
  toLat: number | null;
  toLng: number | null;
  priorityDegree: Level;
  isCompanyVehicle: boolean;
  vehicleTypeId: number;
  ispajero: boolean;
  isRecurringJourney: boolean;
  isRecurringDaily: boolean;
  isRecurringWeekly: boolean;
  isRecurringMonthly: boolean;
  isRecurringyearly: boolean;
  isNight: boolean | null;
  journeyRecurringWeekly: JourneyRecurringWeeklyModel;
  journeyRecurringMonthly: JourneyRecurringMonthlyModel;
  journeyRecurringDaily: JourneyRecurringDailyModel;
  journeyRecurringYearly: JourneyRecurringYearlyModel;
  vehicles: JourneyVehicleModel[];
  drivers: JourneyDriverModel[];
  checkPoints: JourneyCheckPointModel[];
  cargoes: JourneyCargoModel[];
  journeyAssessments: JourneyAssessmentModel[];
}

export class IntiateJourney {
  constructor(
    public id: number,
    public title: string,
    public departmentId: number,
    public requestTypeId: number,
    public journeyStatus: any,
    public startDate: Date,
    public deliveryDate: string,
    public purpose: string,
    public emergencyContact: string,
    public fromZone: number | object | null,
    public toZone: number | object | null,
    public fromLocation: number | object | null,
    public toLocation: number | object | null,
    public isMapLocation: boolean,
    public fromDestination: string | object,
    public fromLat: number | null,
    public fromLng: number | null,
    public toDestination: string | object,
    public toLat: number | null,
    public toLng: number | null,
    public priorityDegree: Level | null,
    public isCompanyVehicle: boolean,
    public vehicleTypeId: number,
    public ispajero: boolean,
    public isRecurringJourney: boolean,
    public isRecurringDaily: boolean,
    public isRecurringWeekly: boolean,
    public isRecurringMonthly: boolean,
    public isRecurringyearly: boolean,
    public isNight: boolean | null,
    public journeyRecurringWeekly: JourneyRecurringWeeklyModel,
    public journeyRecurringMonthly: JourneyRecurringMonthlyModel,
    public journeyRecurringDaily: JourneyRecurringDailyModel,
    public journeyRecurringYearly: JourneyRecurringYearlyModel,
    public vehicles: JourneyVehicleModel[] | any,
    public drivers: JourneyDriverModel[] | any,
    public checkPoints: JourneyCheckPointModel[],
    public cargoes: JourneyCargoModel[],
    public journeyAssessments?: JourneyAssessmentModel[],
    public requestType?: {
      id: number;
      name: string;
    },
    public vehicleType?: {
      id: number;
      name: string;
    },
    public department?: any,
    public isPajero?: boolean
  ) {}
}
