export class ManagerProfile {
    data: ProfileData;
    exception: any
    message: string;
    methodName: string;
    status: number;
}
export class ProfileData {
    address: string;
    email: string;
    emergencyContact: string;
    fullName: string;
    id: number;
    image: string;
    isActive: boolean;
    nationality: string;
    password: string;
    confirmPassword?: string;
    phoneNumber: string;
    rowId: string;
    username: string;
}