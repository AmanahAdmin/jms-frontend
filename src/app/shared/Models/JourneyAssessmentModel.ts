import { AssessmentTiming } from '../enums/assessment-timing';

export class JourneyAssessmentModel {
  constructor(
    public journeyId: number,
    public assessmentTiming: AssessmentTiming,
    public id: number,
    public assessmentCategoryId: number,
    public assessmentId: number,
    public isDriverAnswered: boolean,
    public assessmentName: string,
    public allowUpdateDeleteAction: boolean,
  ) { }
}
