import { UserStatus } from '../enums/user-status.enum';
import { UserRoles } from '../enums/user-roles.enum';

export interface Role {
    id: string;
    name: string;
    userRoleType: UserRoles;
    permissions: string[];
}

export interface User {
    id: string;
    rowId: string;
    username: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    fullName: string;
    userGroupId?: any;
    userWorkForceId?: any;
    licenseNo?: any;
    licenseExpiryDate?: any;
    trainingDetails?: any;
    gatePassStatus?: any;
    token?: string;
    isActive: boolean;
    role: Role;
    roles: Role[];
    phoneNumber: string;
    emergencyContact: string;
    nationality: string;
    address: string;
    comments: string;
    image: string;
    attachments: string[];

    userStatus: UserStatus;
    browsingLanguageId?: any;
    accessNumberOnTheSystem?: any;
}





