import { ComunicationPointLocationTypesEnum } from '../Enums/comunication-point-location-type-enum';

export class CommunicationPoint {
    id?: number;
    journeyId?: number;
    locationFromType?: ComunicationPointLocationTypesEnum;
    locationToType?: ComunicationPointLocationTypesEnum;
    checkpointFromId?: number;
    checkpointToId?: number;
    duration: string;
    durationUnit?: number;
    locationFrom?: string;
    locationTo?: string;
    locationFromTypeName?: string;
    locationToTypeName?: string;
    isFinished?: boolean;
    allowUpdateDeleteAction?: boolean;
}
