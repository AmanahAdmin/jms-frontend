import { PayloadModel } from './PayloadModel';

export interface JourneyTruckModel {
  id: number;
  payloadId: number;
  payload: PayloadModel;
  weight: string;
}
