export class RequestTypeModel {
  constructor(
    public id: number,
    public name: string,
    public isActive: boolean
  ) {}
}
