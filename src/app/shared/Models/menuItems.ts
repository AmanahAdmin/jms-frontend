import { Injectable } from '@angular/core';
import { constant } from '../const/constant';

const featureConst = constant;

export interface MenuItem {
    route: string;
    name: string;
    icon: string;
    permissions?: string[];
    routeName?: string;
    id?: number;
    childrens?: MenuItem[];
}

@Injectable({
    providedIn: 'root'
})
export class MenuItems {
    userManagementMenu: MenuItem[] = [
        {
            name: 'Drivers',
            icon: 'assets/images/JMS/driver.svg',
            route: '/admin/driver',
            permissions: [featureConst.driverBrowse],
        }, {
            name: 'Managers',
            icon: 'assets/images/JMS/manager.svg',
            route: '/admin/manager',
            permissions: [featureConst.managerBrowse],
        }, {
            name: 'Departments',
            icon: 'assets/images/JMS/team.svg',
            route: '/admin/teams',
            permissions: [featureConst.teamBrowse],
        }, {
            name: 'Roles',
            icon: 'assets/images/JMS/role.svg',
            route: '/admin/role',
            permissions: [featureConst.roleBrowse],
        }, {
            name: 'Third Party',
            icon: 'assets/images/JMS/current-journey.svg',
            route: '/third-party',
            permissions: [featureConst.thirdPartyBrowse],
        },

    ];


    journeyManagementMenu: MenuItem[] = [
        {
            name: 'Assessment Management',
            icon: 'assets/images/JMS/008-behavior.svg',
            route: '/admin/Assessments',
            permissions: [featureConst.assessmentBrowse],
        }, {
            name: 'Request Types',
            icon: 'assets/images/JMS/current-journey.svg',
            route: '/request-type',
            permissions: [featureConst.requestTypeBrowse],
        },
        // {
        //     name: 'Answer Types',
        //     icon: 'assets/images/JMS/003-question.svg',
        //     route: '/admin/answerType',
        //     permissions: [featureConst.answerTypeBrowse],
        // },
        {
            name: 'Journey Approval Settings',
            icon: 'assets/images/JMS/002-check.svg',
            route: '/admin/journeyApprovalSetting',
            permissions: [featureConst.journeyApprovalSettingBrowse],
        }, {
            name: 'Alerts Management',
            icon: 'assets/images/JMS/004-alert.svg',
            route: '/admin/alert',
            permissions: [featureConst.AlertsManagement],
        }, {
            name: 'General settings',
            icon: 'assets/images/JMS/gear-1.svg',
            route: '/admin/general-settings',
            permissions: [featureConst.generalSettingsBrowse],
        },

    ];


    settingsMenu: MenuItem[] = [
        {
            name: 'Locations',
            icon: 'assets/images/JMS/map-locator.svg',
            route: '/admin/location',
            permissions: [featureConst.locationBrowse],
        }, {
            name: 'Geofencing',
            icon: 'assets/images/JMS/007-mexico.svg',
            route: '/admin/geofencing',
            permissions: [featureConst.zoneBrowse],
        }, {
            name: 'Vehicles Management',
            icon: 'assets/images/JMS/vehicle.svg',
            route: '/admin/Vehicles',
            permissions: [featureConst.vehicleBrowse],
        }, {
            name: 'Payloads Management',
            icon: 'assets/images/JMS/cargo-box.svg',
            route: '/admin/Payloads',
            permissions: [featureConst.payloadBrowse],
        }, {
            name: 'Shifts',
            icon: 'assets/images/JMS/006-shift.svg',
            route: '/admin/shift',
            permissions: [featureConst.shiftBrowse],
        }, {
            name: 'Languages Management',
            icon: 'assets/images/JMS/language.svg',
            route: '/admin/language',
            permissions: [featureConst.languageBrowse],
        },

    ];

    menu: MenuItem[] = [
        {
            name: 'Initiate Journey',
            icon: 'assets/images/JMS/add-journey.svg',
            route: '/initiate-journey',
            permissions: [featureConst.journeyCreate]
        },
        {
            name: 'Journeys List',
            icon: 'assets/images/JMS/doubt.svg',
            route: '/journey-list',
            permissions: [featureConst.journeysList]
        },
        {
            name: 'Users Management',
            icon: 'assets/images/JMS/group-copy.svg',
            routeName: 'Users-Management',
            id: 3,
            route: '/sub-groups',
            childrens: this.userManagementMenu,
            permissions: [featureConst.driverBrowse, featureConst.managerBrowse, featureConst.roleBrowse, featureConst.teamBrowse, featureConst.thirdPartyBrowse]
        },
        {
            name: 'Journey Management',
            icon: 'assets/images/JMS/destination.svg',
            routeName: 'Journey-Management',
            id: 4,
            route: '/sub-groups',
            childrens: this.journeyManagementMenu,
            permissions: [featureConst.journeyApprovalSettingBrowse, featureConst.assessmentBrowse, featureConst.requestTypeBrowse, featureConst.answerTypeBrowse, featureConst.AlertsManagement, featureConst.generalSettingsBrowse]
        },
        {
            name: 'Settings',
            icon: 'assets/images/JMS/gear-1.svg',
            routeName: 'Settings',
            id: 7,
            route: '/sub-groups',
            childrens: this.settingsMenu,
            permissions: [featureConst.settingsManageMyProfile]
        },
        {
            name: 'Workflow Management',
            icon: 'assets/images/JMS/workflow.svg',
            route: '/admin/workflow',
            permissions: [featureConst.workflowBrowse]
        },
        {
            name: 'Reports',
            icon: 'assets/images/JMS/doubt.svg',
            route: '/reports',
            permissions: ['*']
        },
    ];

    getMenuitem(): MenuItem[] {
        return this.menu;
    }
}