import { ComunicationPointLocationTypesEnum } from "../Enums/comunication-point-location-type-enum";

export class JourneyLocationsModel {
  locationId: number;
  checkPointId: number;
  locationName: string;
  locationType: ComunicationPointLocationTypesEnum;
  isChecPointLocation: boolean;
  isFromLocation: boolean;
}
