export class JourneyList {

    constructor() {
        this.mergedJourneyIds = [];
    }

    id: number;
    title: string;
    departmentName: string;
    fromLocation: string;
    toLocation: string;
    startDate: string;
    deliveryDate: string;
    status: { statusName: string, statusId: number };
    driverName: string;
    creationDate: string;
    reason: string;
    mergedJourneyIds?: number[];
    vehicleId?: number;
    vehiclePlateNo: string;
    vehicleTypeId: number;
    driverId: number;
    isSelectedToMerge: boolean;
}

export class JourneyListModel {
    journeyList: JourneyList[];
    count: number;
}

export class JourneyFilterParams {
    title: string;
    creationDate: string | Date;
    deliveryDate: string;
    status: string | number;
    page: number;
    pageSize: number;
}