
import { AssessmentTiming } from '../Enums/assessment-timing';

export class AssessmentAnswersModal {
    jourenyData: JourenyData;
    answers: AssessmentAnswersTiming[];
}

export class AssessmentAnswersTiming {
    assessmentTiming: AssessmentTiming;
    assessmentWithAnwswers: AssessmentWithAnwswers[];
    assessmentData: AssessmentData;
}
export class AssessmentWithAnwswers {
    assessmentName: string;
    answers: assessAnswers[];
}
export class JourenyData {
    riskResult: string
    isNightDriving: boolean;
    journeyStatus: string;
    totalRiskCount?: number;
}

// export class AssessmentAnswer {
//     answers: assessAnswers[];
//     assessmentData: AssessmentData;
// }

class assessAnswers {
    id: number;
    questionId: number;
    questionOrder: number;
    questionName: string;
    driverAnswer: string;
    driverName: string;
    driverId: number;
}
class AssessmentData {
    assessmentId: number;
    assessmentTimming: string;
    isApproved: true;
    approvedDate: string;
    location: string;
}