import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  public progress: number;
  public message: string;
  @Output() public onUploadFinished = new EventEmitter();
  @Input() showDetails: boolean = true;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  public uploadFile = (files, event) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    let pattern = /image-*/;
    if (!fileToUpload.type.match(pattern)) {
      Swal.fire("", "Invalid image format", "error");
      event.target && (event.target.value = null);
      return;
    }
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.http.post(`${environment.JMSApiURL}/Upload`, formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          this.onUploadFinished.emit(event.body);
        }
      });
  }
}