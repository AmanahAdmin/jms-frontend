import { AlertDuration } from '../shared/Enums/alert-duration-enum';

export interface IETA {
    title: string;
    value: string;
    unit?: AlertDuration;
}

export interface IMonth {
    name: string;
    number: string;
}

export interface IDay {
    name: string;
    number: string;
}

export interface ITime {
    name: string;
    number: string;
}

export class Calendar {
    static readonly etaList: IETA[] = [
        {
            title: 'Select ETA',
            value: '0',
        },
        {
            title: '5 min',
            value: '5 min',
        },
        {
            title: '10 min',
            value: '10 min',
        },
        {
            title: '15 min',
            value: '15 min',
        },
        {
            title: '20 min',
            value: '20 min',
        },
        {
            title: '25 min',
            value: '25 min',
        },
        {
            title: '30 min',
            value: '30 min',
        },
        {
            title: '35 min',
            value: '35 min',
        },
        {
            title: '40 min',
            value: '40 min',
        },
        {
            title: '45 min',
            value: '45 min',
        },
        {
            title: '50 min',
            value: '50 min',
        },
        {
            title: '55 min',
            value: '55 min',
        },
        {
            title: '1 hr',
            value: '1 hr',
        },
        {
            title: '2 hr',
            value: '2 hr',
        },
        {
            title: '3 hr',
            value: '3 hr',
        },
        {
            title: '4 hr',
            value: '4 hr',
        },
        {
            title: '5 hr',
            value: '5 hr',
        },
        {
            title: '6 hr',
            value: '6 hr',
        },
        {
            title: '7 hr',
            value: '7 hr',
        },
        {
            title: '8 hr',
            value: '8 hr',
        },
        {
            title: '9 hr',
            value: '9 hr',
        },
        {
            title: '10 hr',
            value: '10 hr',
        },
        {
            title: '11 hr',
            value: '11 hr',
        }, {
            title: '12 hr',
            value: '12 hr',
        },
        {
            title: '13 hr',
            value: '13 hr',
        },
        {
            title: '14 hr',
            value: '14 hr',
        },
        {
            title: '15 hr',
            value: '15 hr',
        },
        {
            title: '16 hr',
            value: '16 hr',
        },
        {
            title: '17 hr',
            value: '17 hr',
        },
        {
            title: '18 hr',
            value: '18 hr',
        },
        {
            title: '19 hr',
            value: '19 hr',
        },
        {
            title: '20 hr',
            value: '20 hr',
        },
        {
            title: '21 hr',
            value: '21 hr',
        },
        {
            title: '22 hr',
            value: '22 hr',
        },
        {
            title: '23 hr',
            value: '23 hr',
        },
        {
            title: '24 hr',
            value: '24 hr',
        },
    ];

    static readonly months: IMonth[] = [
        {
            name: 'January', number: '1'
        },
        {
            name: 'February', number: '2'
        },
        {
            name: 'March', number: '3'
        },
        {
            name: 'April', number: '4'
        },
        {
            name: 'May', number: '5'
        },
        {
            name: 'June', number: '6'
        },
        {
            name: 'July', number: '7'
        },
        {
            name: 'August', number: '8'
        },
        {
            name: 'September', number: '8'
        },
        {
            name: 'October', number: '10'
        },
        {
            name: 'November', number: '11'
        },
        {
            name: 'December', number: '12'
        }
    ];

    static readonly days: IDay[] = [
        {
            name: 'Saturday', number: '1'
        },
        {
            name: 'Sunday', number: '2'
        },
        {
            name: 'Monday', number: '3'
        },
        {
            name: 'Tuesday', number: '4'
        },
        {
            name: 'Wednesday', number: '5'
        },
        {
            name: 'Thursday', number: '6'
        },
        {
            name: 'Friday', number: '7'
        },
    ];

    static readonly times: ITime[] = [
        {
            name: '5 min', number: '1'
        },
        {
            name: '10 min', number: '2'
        },
        {
            name: '15 min', number: '3'
        },
        {
            name: '20 min', number: '4'
        },
        {
            name: '25 min', number: '5'
        },
        {
            name: '30 min', number: '6'
        },
        {
            name: '35 min', number: '7'
        },
        {
            name: '40 min', number: '8'
        },
        {
            name: '45 min', number: '8'
        },
        {
            name: '50 min', number: '10'
        },
        {
            name: '55 min', number: '11'
        },
        {
            name: '60 min', number: '12'
        },
        {
            name: '5 min', number: '1'
        },
        {
            name: '10 min', number: '2'
        },
        {
            name: '15 min', number: '3'
        },
        {
            name: '20 min', number: '4'
        },
        {
            name: '25 min', number: '5'
        },
        {
            name: '30 min', number: '6'
        },
        {
            name: '35 min', number: '7'
        },
        {
            name: '40 min', number: '8'
        },
        {
            name: '45 min', number: '8'
        },
        {
            name: '50 min', number: '10'
        },
        {
            name: '55 min', number: '11'
        },
        {
            name: '60 min', number: '12'
        }
    ];

    static readonly etaListUnits: IETA[] = [
        {
            title: 'Select ETA',
            value: '0',
        },
        {
            title: '5 min',
            value: '5',
            unit: AlertDuration.Mintes
        },
        {
            title: '10 min',
            value: '10',
            unit: AlertDuration.Mintes
        },
        {
            title: '15 min',
            value: '15',
            unit: AlertDuration.Mintes
        },
        {
            title: '20 min',
            value: '20',
            unit: AlertDuration.Mintes
        },
        {
            title: '25 min',
            value: '25',
            unit: AlertDuration.Mintes
        },
        {
            title: '30 min',
            value: '30',
            unit: AlertDuration.Mintes
        },
        {
            title: '35 min',
            value: '35',
            unit: AlertDuration.Mintes
        },
        {
            title: '40 min',
            value: '40',
            unit: AlertDuration.Mintes
        },
        {
            title: '45 min',
            value: '45',
            unit: AlertDuration.Mintes
        },
        {
            title: '50 min',
            value: '50',
            unit: AlertDuration.Mintes
        },
        {
            title: '55 min',
            value: '55',
            unit: AlertDuration.Mintes
        },
        {
            title: '1 hr',
            value: '1',
            unit: AlertDuration.Houres
        },
        {
            title: '2 hr',
            value: '2',
            unit: AlertDuration.Houres

        },
        {
            title: '3 hr',
            value: '3',
            unit: AlertDuration.Houres

        },
        {
            title: '4 hr',
            value: '4',
            unit: AlertDuration.Houres
        },
        {
            title: '5 hr',
            value: '5',
            unit: AlertDuration.Houres
        },
        {
            title: '6 hr',
            value: '6',
            unit: AlertDuration.Houres
        },
        {
            title: '7 hr',
            value: '7',
            unit: AlertDuration.Houres
        },
        {
            title: '8 hr',
            value: '8',
            unit: AlertDuration.Houres
        },
        {
            title: '9 hr',
            value: '9',
            unit: AlertDuration.Houres
        },
        {
            title: '10 hr',
            value: '10',
            unit: AlertDuration.Houres
        },
        {
            title: '11 hr',
            value: '11',
            unit: AlertDuration.Houres
        }, {
            title: '12 hr',
            value: '12',
            unit: AlertDuration.Houres
        },
        {
            title: '13 hr',
            value: '13',
            unit: AlertDuration.Houres
        },
        {
            title: '14 hr',
            value: '14',
            unit: AlertDuration.Houres
        },
        {
            title: '15 hr',
            value: '15',
            unit: AlertDuration.Houres
        },
        {
            title: '16 hr',
            value: '16',
            unit: AlertDuration.Houres
        },
        {
            title: '17 hr',
            value: '17',
            unit: AlertDuration.Houres
        },
        {
            title: '18 hr',
            value: '18',
            unit: AlertDuration.Houres
        },
        {
            title: '19 hr',
            value: '19',
            unit: AlertDuration.Houres
        },
        {
            title: '20 hr',
            value: '20',
            unit: AlertDuration.Houres
        },
        {
            title: '21 hr',
            value: '21',
            unit: AlertDuration.Houres
        },
        {
            title: '22 hr',
            value: '22',
            unit: AlertDuration.Houres
        },
        {
            title: '23 hr',
            value: '23',
            unit: AlertDuration.Houres
        },
        {
            title: '24 hr',
            value: '24',
            unit: AlertDuration.Houres
        },
    ];
}

