import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireModule } from "@angular/fire";
import { environment } from '@jms/environments/environment';
import { MessagingService } from '@jms/services/messaging.service';

@NgModule({
  imports: [
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    ToastrModule.forRoot(),
  ],
  providers: [
    MessagingService
  ]
})
export class RealtimeNotificationModule { }
