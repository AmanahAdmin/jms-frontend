importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

firebase.initializeApp({
 apiKey: "AIzaSyDq_8Zf2Cruhuj2Zl1JrREVysPVAmLTgXY",
 authDomain: "jmscore-13578.firebaseapp.com",
 databaseURL:  "https://jmscore-13578.firebaseio.com",
 projectId: "jmscore-13578",
 storageBucket: "jmscore-13578.appspot.com",
 messagingSenderId:  "499940210678",
 appId: "1:499940210678:web:35496bd2ecff736988b9d7",
 measurementId: "G-HFNWBHYLSF"
});

const messaging = firebase.messaging();