import { MapSettings } from "@jms/app/core/map-settings";
import { Settings } from "@jms/app/core/settings";

/**
 * Base URL settings (development)
 *
 *
 */
const settings: Settings = {
  apiHost: '37.34.189.202',
  apiProtocol: 'http',
  apiPort: 8030,
  apiSubDomain: '',
  // apiHost: 'localhost',
  // apiProtocol: 'http',
  // apiPort: 5991,
  // apiSubDomain: '',
};

const mapSettings: MapSettings = {
  apiKey: '',
  libraries: ['places']
}

export const environment = {
  production: false,
  settings: settings,
  mapSettings: mapSettings,

  firebase: {
    apiKey: "AIzaSyDq_8Zf2Cruhuj2Zl1JrREVysPVAmLTgXY",
    authDomain: "jmscore-13578.firebaseapp.com",
    databaseURL: "https://jmscore-13578.firebaseio.com",
    projectId: "jmscore-13578",
    storageBucket: "jmscore-13578.appspot.com",
    messagingSenderId: "499940210678",
    appId: "1:499940210678:web:35496bd2ecff736988b9d7",
    measurementId: "G-HFNWBHYLSF"
  }
};
